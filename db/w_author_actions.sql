/*
MySQL Data Transfer
Source Host: localhost
Source Database: reew
Target Host: localhost
Target Database: reew
Date: 2018-10-16 15:03:28
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for w_author_actions
-- ----------------------------
DROP TABLE IF EXISTS `w_author_actions`;
CREATE TABLE `w_author_actions` (
  `aid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'aid',
  `action` varchar(30) NOT NULL DEFAULT '' COMMENT 'action',
  `displayorder` int(11) NOT NULL DEFAULT '50' COMMENT '排序',
  `parentid` int(11) NOT NULL DEFAULT '0' COMMENT '上级id',
  `title` varchar(120) NOT NULL DEFAULT '' COMMENT '标题',
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='�û�������';

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `w_author_actions` VALUES ('1', 'home:index', '1', '0', '用户添加');
INSERT INTO `w_author_actions` VALUES ('2', 'user:edit', '2', '0', '用户修改');
