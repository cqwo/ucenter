/*
MySQL Data Transfer
Source Host: rm-bp16z7ji1v0z288uqo.mysql.rds.aliyuncs.com
Source Database: gateway
Target Host: rm-bp16z7ji1v0z288uqo.mysql.rds.aliyuncs.com
Target Database: gateway
Date: 2018-10-16 15:02:38
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for deviceinfo
-- ----------------------------
DROP TABLE IF EXISTS `deviceinfo`;
CREATE TABLE `deviceinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `deviceid` varchar(255) NOT NULL,
  `encryptkey` varchar(120) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `publicid` varchar(50) NOT NULL,
  `tcpmethod` varchar(20) NOT NULL,
  `token` varchar(120) NOT NULL,
  `type` int(11) NOT NULL,
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_m5njyo51fc3nqiabvkdll8yhg` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for gateway_info
-- ----------------------------
DROP TABLE IF EXISTS `gateway_info`;
CREATE TABLE `gateway_info` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `code` varchar(255) DEFAULT NULL COMMENT '网关编号',
  `key` varchar(255) DEFAULT NULL COMMENT '网关key码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='网关信息表';

-- ----------------------------
-- Table structure for onlinedeviceinfo
-- ----------------------------
DROP TABLE IF EXISTS `onlinedeviceinfo`;
CREATE TABLE `onlinedeviceinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `deviceid` varchar(255) NOT NULL,
  `encryptkey` varchar(120) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `publicid` varchar(50) NOT NULL,
  `tcpmethod` varchar(20) NOT NULL,
  `token` varchar(120) NOT NULL,
  `type` int(11) NOT NULL,
  `version` varchar(255) NOT NULL,
  `channelid` varchar(255) NOT NULL,
  `lasthearttime` int(11) NOT NULL,
  `mark` varchar(255) NOT NULL,
  `onlinestate` int(11) NOT NULL,
  `onlinetime` int(11) NOT NULL,
  `timestamp` varchar(255) NOT NULL,
  `crc` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_paiexu2vejjg3x0u0orrkoq54` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for person
-- ----------------------------
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `uid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_attachment
-- ----------------------------
DROP TABLE IF EXISTS `w_attachment`;
CREATE TABLE `w_attachment` (
  `attachid` int(11) NOT NULL AUTO_INCREMENT,
  `createtime` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `uid` varchar(50) NOT NULL DEFAULT '' COMMENT 'uid',
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`attachid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_author_actions
-- ----------------------------
DROP TABLE IF EXISTS `w_author_actions`;
CREATE TABLE `w_author_actions` (
  `aid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'aid',
  `action` varchar(30) NOT NULL DEFAULT '' COMMENT 'action',
  `displayorder` int(11) NOT NULL DEFAULT '50' COMMENT '排序',
  `parentid` int(11) NOT NULL DEFAULT '0' COMMENT '上级id',
  `title` varchar(120) NOT NULL DEFAULT '' COMMENT '标题',
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='�û�������';

-- ----------------------------
-- Table structure for w_author_adminlogs
-- ----------------------------
DROP TABLE IF EXISTS `w_author_adminlogs`;
CREATE TABLE `w_author_adminlogs` (
  `logid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `roleid` int(11) NOT NULL DEFAULT '0' COMMENT '�ǳ�',
  `roletitle` varchar(50) NOT NULL COMMENT '管理组标题',
  `description` varchar(150) NOT NULL COMMENT '描述',
  `ip` varchar(32) NOT NULL DEFAULT '127.0.0.1' COMMENT 'ip',
  `nickname` varchar(30) NOT NULL COMMENT '昵称',
  `operatetime` int(11) NOT NULL DEFAULT '0' COMMENT '操作时间',
  `operation` varchar(50) NOT NULL COMMENT '操作标题',
  `uid` varchar(50) NOT NULL DEFAULT '' COMMENT '用户Id',
  `admingid` int(11) NOT NULL,
  `admingtitle` varchar(50) NOT NULL,
  PRIMARY KEY (`logid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='������־��';

-- ----------------------------
-- Table structure for w_author_permissions
-- ----------------------------
DROP TABLE IF EXISTS `w_author_permissions`;
CREATE TABLE `w_author_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `roleid` int(11) NOT NULL COMMENT '用户角色id',
  `aid` int(11) NOT NULL COMMENT '动作id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='��ɫ���ɱ�';

-- ----------------------------
-- Table structure for w_author_roles
-- ----------------------------
DROP TABLE IF EXISTS `w_author_roles`;
CREATE TABLE `w_author_roles` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(50) NOT NULL COMMENT '分组名称',
  `code` varchar(20) NOT NULL COMMENT '分组标识',
  `description` varchar(250) NOT NULL COMMENT '分组描述',
  PRIMARY KEY (`roleid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户分组表';

-- ----------------------------
-- Table structure for w_author_sessions
-- ----------------------------
DROP TABLE IF EXISTS `w_author_sessions`;
CREATE TABLE `w_author_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` varchar(50) NOT NULL DEFAULT '' COMMENT '用户uid',
  `roleid` int(11) NOT NULL COMMENT '用户组id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='�û�-�������';

-- ----------------------------
-- Table structure for w_category
-- ----------------------------
DROP TABLE IF EXISTS `w_category`;
CREATE TABLE `w_category` (
  `cateId` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `displayorder` int(11) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `layer` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parentid` int(11) NOT NULL,
  `topid` int(11) NOT NULL,
  `typeid` int(11) NOT NULL,
  `warntype` int(11) NOT NULL,
  PRIMARY KEY (`cateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_commom_bannedips
-- ----------------------------
DROP TABLE IF EXISTS `w_commom_bannedips`;
CREATE TABLE `w_commom_bannedips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(30) NOT NULL,
  `liftbantime` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_commom_regions
-- ----------------------------
DROP TABLE IF EXISTS `w_commom_regions`;
CREATE TABLE `w_commom_regions` (
  `regionid` int(11) NOT NULL AUTO_INCREMENT,
  `cityid` int(11) NOT NULL,
  `cityname` varchar(20) NOT NULL,
  `displayorder` int(11) NOT NULL,
  `layer` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `parentid` int(11) NOT NULL,
  `provinceid` int(11) NOT NULL,
  `provincename` varchar(20) NOT NULL,
  `shortspell` varchar(20) NOT NULL,
  `spell` varchar(30) NOT NULL,
  `engname` varchar(30) NOT NULL,
  PRIMARY KEY (`regionid`)
) ENGINE=InnoDB AUTO_INCREMENT=820102 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for w_device_action
-- ----------------------------
DROP TABLE IF EXISTS `w_device_action`;
CREATE TABLE `w_device_action` (
  `actionid` int(11) NOT NULL AUTO_INCREMENT,
  `groupid` int(11) NOT NULL,
  `deviceid` int(11) NOT NULL,
  `protocolid` int(11) NOT NULL,
  `protocol` varchar(10) NOT NULL,
  `httpurl` varchar(150) NOT NULL DEFAULT 'http://',
  `isenable` int(11) NOT NULL,
  `intervaltime` int(11) NOT NULL,
  `thancount` int(11) NOT NULL,
  PRIMARY KEY (`actionid`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_device_action_params
-- ----------------------------
DROP TABLE IF EXISTS `w_device_action_params`;
CREATE TABLE `w_device_action_params` (
  `paramId` int(11) NOT NULL AUTO_INCREMENT,
  `actionid` int(11) NOT NULL,
  `defaultvalue` varchar(255) NOT NULL,
  `key2` varchar(30) NOT NULL,
  `tokey` varchar(30) NOT NULL,
  PRIMARY KEY (`paramId`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_device_group
-- ----------------------------
DROP TABLE IF EXISTS `w_device_group`;
CREATE TABLE `w_device_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deviceid` int(11) NOT NULL,
  `groupid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=283 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_devices
-- ----------------------------
DROP TABLE IF EXISTS `w_devices`;
CREATE TABLE `w_devices` (
  `deviceid` int(11) NOT NULL AUTO_INCREMENT COMMENT '探测器id',
  `imei` varchar(64) NOT NULL COMMENT '设备的串号',
  `name` varchar(30) NOT NULL COMMENT '探测器名称',
  `devicesn` varchar(50) NOT NULL COMMENT '探测器编号',
  `productid` int(11) NOT NULL COMMENT '产品d',
  `productname` varchar(30) NOT NULL COMMENT '产品名称',
  `token` varchar(120) NOT NULL COMMENT '所属网关设备',
  `litpic` varchar(150) NOT NULL COMMENT '设备缩略图',
  `version` varchar(255) NOT NULL COMMENT '设备版本',
  `description` varchar(255) NOT NULL COMMENT '设备描述',
  `latitude` double NOT NULL COMMENT '纬度',
  `longitude` double NOT NULL COMMENT '经度',
  `addtime` int(11) NOT NULL COMMENT '探测器添加时间',
  `updatetime` int(11) NOT NULL COMMENT '探测器修改时间',
  `devicemodel` int(11) NOT NULL,
  `address` varchar(150) NOT NULL,
  `regionid` int(11) NOT NULL,
  `unitcode` varchar(30) NOT NULL,
  PRIMARY KEY (`deviceid`)
) ENGINE=InnoDB AUTO_INCREMENT=273 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_gateway
-- ----------------------------
DROP TABLE IF EXISTS `w_gateway`;
CREATE TABLE `w_gateway` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  `token` varchar(120) NOT NULL DEFAULT '',
  `sim` varchar(20) NOT NULL,
  `tcpmethod` varchar(20) NOT NULL,
  `encryptkey` varchar(120) NOT NULL DEFAULT '1234',
  `description` varchar(255) NOT NULL DEFAULT '',
  `latitude` double NOT NULL DEFAULT '0',
  `longitude` double NOT NULL DEFAULT '0',
  `version` varchar(255) NOT NULL DEFAULT '',
  `addtime` int(11) NOT NULL,
  `address` varchar(150) NOT NULL,
  `litpic` varchar(150) NOT NULL,
  `regionid` int(11) NOT NULL,
  `updatetime` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_m5njyo51fc3nqiabvkdll8yhg` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_gateway_online
-- ----------------------------
DROP TABLE IF EXISTS `w_gateway_online`;
CREATE TABLE `w_gateway_online` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addtime` int(11) NOT NULL,
  `address` varchar(150) NOT NULL,
  `description` varchar(255) NOT NULL,
  `encryptkey` varchar(120) NOT NULL,
  `latitude` double NOT NULL,
  `litpic` varchar(150) NOT NULL,
  `longitude` double NOT NULL,
  `name` varchar(120) NOT NULL,
  `regionid` int(11) NOT NULL,
  `sim` varchar(20) NOT NULL,
  `tcpmethod` varchar(20) NOT NULL,
  `token` varchar(120) NOT NULL,
  `updatetime` int(11) NOT NULL,
  `version` varchar(255) NOT NULL,
  `channelid` varchar(255) NOT NULL,
  `crc` varchar(255) NOT NULL,
  `lasthearttime` int(11) NOT NULL,
  `mark` varchar(255) NOT NULL,
  `onlinestate` int(11) NOT NULL,
  `onlinetime` int(11) NOT NULL,
  `timestamp` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_unique` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=879 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_gateway_online2
-- ----------------------------
DROP TABLE IF EXISTS `w_gateway_online2`;
CREATE TABLE `w_gateway_online2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  `token` varchar(120) NOT NULL DEFAULT '',
  `sim` varchar(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  `encryptkey` varchar(120) NOT NULL DEFAULT '',
  `latitude` double NOT NULL DEFAULT '0',
  `longitude` double NOT NULL DEFAULT '0',
  `tcpmethod` varchar(20) NOT NULL DEFAULT '',
  `version` varchar(255) NOT NULL DEFAULT '',
  `channelid` varchar(255) NOT NULL DEFAULT '',
  `crc` varchar(255) NOT NULL DEFAULT '',
  `lasthearttime` int(11) NOT NULL DEFAULT '0',
  `mark` varchar(255) NOT NULL DEFAULT '',
  `onlinestate` int(11) NOT NULL DEFAULT '0',
  `onlinetime` int(11) NOT NULL DEFAULT '0',
  `timestamp` varchar(255) NOT NULL DEFAULT '',
  `addtime` int(11) NOT NULL,
  `address` varchar(150) NOT NULL,
  `litpic` varchar(150) NOT NULL,
  `regionid` int(11) NOT NULL,
  `updatetime` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_q05d4js3r5asnjaixv1i5th5k` (`token`),
  UNIQUE KEY `UK_cpy71tgxwafca6jp58yega0oa` (`sim`)
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_groups
-- ----------------------------
DROP TABLE IF EXISTS `w_groups`;
CREATE TABLE `w_groups` (
  `groupid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `productid` int(11) NOT NULL DEFAULT '0',
  `productname` varchar(30) NOT NULL,
  `description` varchar(500) NOT NULL,
  `netprotocol` varchar(20) NOT NULL,
  `network` varchar(20) NOT NULL,
  `addtime` int(11) NOT NULL,
  `updatetime` int(11) NOT NULL,
  `isenable` int(11) NOT NULL,
  PRIMARY KEY (`groupid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_message_record
-- ----------------------------
DROP TABLE IF EXISTS `w_message_record`;
CREATE TABLE `w_message_record` (
  `recordid` int(11) NOT NULL AUTO_INCREMENT,
  `devicesn` varchar(255) NOT NULL,
  `msgid` int(11) NOT NULL,
  `addtime` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `key2` varchar(255) NOT NULL,
  `protocol` varchar(255) NOT NULL,
  PRIMARY KEY (`recordid`)
) ENGINE=InnoDB AUTO_INCREMENT=266104 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for w_messages
-- ----------------------------
DROP TABLE IF EXISTS `w_messages`;
CREATE TABLE `w_messages` (
  `msgid` int(11) NOT NULL AUTO_INCREMENT,
  `channelid` varchar(120) NOT NULL,
  `content` varchar(800) NOT NULL,
  `devicesn` varchar(120) NOT NULL DEFAULT '',
  `protocol` varchar(120) NOT NULL DEFAULT '' COMMENT '议协',
  `ip` varchar(255) NOT NULL,
  `message` varchar(120) NOT NULL,
  `port` int(11) NOT NULL,
  `pluginid` int(11) NOT NULL DEFAULT '0' COMMENT '件插类型',
  `state` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `token` varchar(120) NOT NULL DEFAULT '',
  `type` int(11) NOT NULL,
  `devicecode` varchar(255) NOT NULL,
  `devicename` varchar(255) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `lose` int(11) NOT NULL,
  `productid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`msgid`)
) ENGINE=InnoDB AUTO_INCREMENT=230651 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_messages_heart
-- ----------------------------
DROP TABLE IF EXISTS `w_messages_heart`;
CREATE TABLE `w_messages_heart` (
  `msgid` int(11) NOT NULL AUTO_INCREMENT,
  `channelid` varchar(120) NOT NULL,
  `content` varchar(800) NOT NULL,
  `devicecode` varchar(255) NOT NULL,
  `devicename` varchar(255) NOT NULL,
  `devicesn` varchar(120) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `lose` int(11) NOT NULL,
  `message` varchar(120) NOT NULL,
  `port` int(11) NOT NULL,
  `protocol` varchar(255) NOT NULL,
  `state` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `token` varchar(120) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `pluginid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  PRIMARY KEY (`msgid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_messages_open
-- ----------------------------
DROP TABLE IF EXISTS `w_messages_open`;
CREATE TABLE `w_messages_open` (
  `msgid` int(11) NOT NULL AUTO_INCREMENT,
  `channelid` varchar(120) NOT NULL,
  `content` varchar(800) NOT NULL,
  `devicecode` varchar(255) NOT NULL,
  `devicename` varchar(255) NOT NULL,
  `devicesn` varchar(120) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `lose` int(11) NOT NULL,
  `message` varchar(120) NOT NULL,
  `port` int(11) NOT NULL,
  `protocol` varchar(255) NOT NULL,
  `state` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `token` varchar(120) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `pluginid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  PRIMARY KEY (`msgid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_output
-- ----------------------------
DROP TABLE IF EXISTS `w_output`;
CREATE TABLE `w_output` (
  `logid` int(11) NOT NULL AUTO_INCREMENT,
  `addtime` int(11) NOT NULL,
  `content` varchar(500) NOT NULL,
  `devicesn` varchar(50) NOT NULL,
  `httpurl` varchar(150) NOT NULL,
  `msgid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `result` text NOT NULL,
  `token` varchar(50) NOT NULL,
  `actionid` int(11) NOT NULL,
  `groupid` int(11) NOT NULL,
  PRIMARY KEY (`logid`)
) ENGINE=InnoDB AUTO_INCREMENT=6173 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_product_protocol
-- ----------------------------
DROP TABLE IF EXISTS `w_product_protocol`;
CREATE TABLE `w_product_protocol` (
  `protocolid` int(11) NOT NULL AUTO_INCREMENT,
  `productid` int(11) NOT NULL,
  `productname` varchar(255) NOT NULL,
  `name` varchar(30) NOT NULL,
  `protocol` varchar(30) NOT NULL,
  `description` varchar(500) NOT NULL,
  `isdefault` int(11) NOT NULL,
  `code` varchar(30) NOT NULL,
  PRIMARY KEY (`protocolid`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_product_protocol_param
-- ----------------------------
DROP TABLE IF EXISTS `w_product_protocol_param`;
CREATE TABLE `w_product_protocol_param` (
  `paramid` int(11) NOT NULL AUTO_INCREMENT,
  `key2` varchar(30) NOT NULL,
  `description` varchar(500) NOT NULL,
  `name` varchar(30) NOT NULL,
  `productid` int(11) NOT NULL,
  `productname` varchar(30) NOT NULL,
  `protocolid` int(11) NOT NULL,
  `protocolname` varchar(30) NOT NULL,
  PRIMARY KEY (`paramid`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_products
-- ----------------------------
DROP TABLE IF EXISTS `w_products`;
CREATE TABLE `w_products` (
  `productid` int(11) NOT NULL AUTO_INCREMENT COMMENT '产品的id',
  `prodcutsn` varchar(80) NOT NULL COMMENT '产品编号',
  `name` varchar(30) NOT NULL COMMENT '产品名称',
  `litpic` varchar(150) NOT NULL COMMENT '设备图片',
  `company` varchar(150) NOT NULL COMMENT '生产公司',
  `onlinetime` int(11) NOT NULL COMMENT '上线时间',
  `version` varchar(20) NOT NULL COMMENT '产品的版本',
  `addtime` int(11) NOT NULL COMMENT '新增时间',
  `updatetime` int(11) NOT NULL COMMENT '更新时间',
  `description` varchar(500) NOT NULL COMMENT '分组描述',
  `body` varchar(255) NOT NULL COMMENT '设备详情描述',
  `isenable` int(11) NOT NULL COMMENT '是否启用分组',
  `token` varchar(255) NOT NULL,
  `salt` varchar(16) NOT NULL,
  `secretkey` varchar(255) NOT NULL,
  PRIMARY KEY (`productid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_sms
-- ----------------------------
DROP TABLE IF EXISTS `w_sms`;
CREATE TABLE `w_sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` varchar(80) NOT NULL,
  `code` varchar(10) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `sendtime` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `uid` varchar(50) NOT NULL DEFAULT '' COMMENT 'uid',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_sysconfig
-- ----------------------------
DROP TABLE IF EXISTS `w_sysconfig`;
CREATE TABLE `w_sysconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `vardesc` varchar(255) NOT NULL,
  `varname` varchar(255) NOT NULL,
  `varvalue` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_user_creditlogs
-- ----------------------------
DROP TABLE IF EXISTS `w_user_creditlogs`;
CREATE TABLE `w_user_creditlogs` (
  `logid` int(11) NOT NULL AUTO_INCREMENT,
  `action` int(11) NOT NULL,
  `actioncode` int(11) NOT NULL,
  `actiondes` varchar(150) NOT NULL,
  `actiontime` int(11) NOT NULL,
  `operator` int(11) NOT NULL,
  `paycredits` int(11) NOT NULL,
  `rankcredits` int(11) NOT NULL,
  `uid` varchar(50) NOT NULL DEFAULT '' COMMENT 'uid',
  PRIMARY KEY (`logid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_user_loginfaillogs
-- ----------------------------
DROP TABLE IF EXISTS `w_user_loginfaillogs`;
CREATE TABLE `w_user_loginfaillogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `failtimes` int(11) NOT NULL,
  `lastlogintime` int(11) NOT NULL,
  `loginip` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_user_oauth
-- ----------------------------
DROP TABLE IF EXISTS `w_user_oauth`;
CREATE TABLE `w_user_oauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appid` int(11) NOT NULL,
  `openid` varchar(80) NOT NULL,
  `server` varchar(30) NOT NULL,
  `uid` varchar(50) NOT NULL DEFAULT '' COMMENT 'uid',
  `unionid` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_hfklegvuujxpuhm93wjiaas65` (`openid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_user_onlinetime
-- ----------------------------
DROP TABLE IF EXISTS `w_user_onlinetime`;
CREATE TABLE `w_user_onlinetime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `uid` varchar(50) NOT NULL DEFAULT '' COMMENT 'uid',
  `updatetime` int(11) NOT NULL,
  `week` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_user_onlineusers
-- ----------------------------
DROP TABLE IF EXISTS `w_user_onlineusers`;
CREATE TABLE `w_user_onlineusers` (
  `olid` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(30) NOT NULL,
  `nickname` varchar(30) NOT NULL,
  `regionid` int(11) NOT NULL,
  `sid` varchar(30) NOT NULL,
  `uid` varchar(50) NOT NULL DEFAULT '' COMMENT 'uid',
  `updatetime` int(11) NOT NULL,
  PRIMARY KEY (`olid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_user_userdetails
-- ----------------------------
DROP TABLE IF EXISTS `w_user_userdetails`;
CREATE TABLE `w_user_userdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bio` varchar(500) NOT NULL,
  `birthday` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `idcard` varchar(30) NOT NULL,
  `lastvisitip` varchar(30) NOT NULL,
  `lastvisitrgid` int(11) NOT NULL,
  `lastvisittime` int(11) NOT NULL,
  `registerip` varchar(30) NOT NULL,
  `registerrgid` int(11) NOT NULL,
  `registertime` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_user_userranks
-- ----------------------------
DROP TABLE IF EXISTS `w_user_userranks`;
CREATE TABLE `w_user_userranks` (
  `userrid` int(11) NOT NULL AUTO_INCREMENT,
  `avatar` varchar(150) NOT NULL,
  `creditslower` int(11) NOT NULL,
  `creditsupper` int(11) NOT NULL,
  `limitdays` int(11) NOT NULL,
  `system` int(11) NOT NULL,
  `title` varchar(30) NOT NULL,
  PRIMARY KEY (`userrid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_users
-- ----------------------------
DROP TABLE IF EXISTS `w_users`;
CREATE TABLE `w_users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(30) NOT NULL DEFAULT '',
  `mobile` varchar(12) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `userrid` int(11) NOT NULL DEFAULT '0',
  `parentid` int(11) NOT NULL DEFAULT '0',
  `avatar` varchar(255) NOT NULL DEFAULT '',
  `nickname` varchar(30) NOT NULL DEFAULT '',
  `realname` varchar(10) NOT NULL DEFAULT '',
  `latitude` double NOT NULL DEFAULT '0',
  `longitude` double NOT NULL DEFAULT '0',
  `regionid` int(11) NOT NULL DEFAULT '0',
  `address` varchar(255) NOT NULL DEFAULT '',
  `money` double NOT NULL DEFAULT '0',
  `paycredits` int(11) NOT NULL DEFAULT '122',
  `rankcredits` int(11) NOT NULL DEFAULT '0',
  `rankmoney` double NOT NULL,
  `verifyemail` int(11) NOT NULL DEFAULT '0',
  `verifymobile` int(11) NOT NULL DEFAULT '0',
  `invitcode` varchar(30) NOT NULL DEFAULT '',
  `liftbantime` int(11) NOT NULL DEFAULT '0',
  `salt` varchar(18) NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for w_users_token
-- ----------------------------
DROP TABLE IF EXISTS `w_users_token`;
CREATE TABLE `w_users_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `limittime` int(11) NOT NULL,
  `token` varchar(500) NOT NULL,
  `uid` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_p803ratwyyl3jqdl0befakh1q` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for wqq
-- ----------------------------
DROP TABLE IF EXISTS `wqq`;
CREATE TABLE `wqq` (
  `id` double NOT NULL,
  `ds` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records 
-- ----------------------------
