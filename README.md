版本更新

2.4

UC用户中心版本更新

1.除token登录以外的所有登录,均增加指定token过期时间,默认2小时
2.去除token登录自身刷新功能
3.refreshToken刷新接口增加指定token过期时间
4.普通用户账号和密码登录增加是否强制刷新token,默认强制刷新
5.维护token过期时间为5分钟

微服务接口

```
<dependency>
<groupId>com.cqwo.ucenter</groupId>
<artifactId>UCenter.Client</artifactId>
<version>2.4</version>
</dependency>
```

普通接口
```
<dependency>
<groupId>com.cqwo.ucenter</groupId>
<artifactId>UCenter.HttpClient</artifactId>
<version>2.4</version>
</dependency>
```
