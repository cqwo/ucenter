package com.cqwo.ucenter.web.controller;

import com.cqwo.ucenter.web.framework.controller.BaseWebController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "oauth")
public class BaseOpenAuthController extends BaseWebController {
}
