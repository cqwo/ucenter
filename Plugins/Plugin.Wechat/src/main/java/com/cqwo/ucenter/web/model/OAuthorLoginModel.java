package com.cqwo.ucenter.web.model;

import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import com.cqwo.ucenter.services.Users;
import lombok.Getter;
import lombok.Setter;
import org.apache.catalina.User;


@Getter
@Setter
public class OAuthorLoginModel {

    /**
     * token
     */
    private String token = "";

    /**
     * userInfo
     */
    private PartUserInfo userInfo;

    /**
     * 返回页面
     */
    private String returnUrl = "/";

    public OAuthorLoginModel(String token, PartUserInfo userInfo, String returnUrl) {
        this.token = token;
        this.userInfo = Users.securityUserInfo(userInfo);
        this.returnUrl = returnUrl;
    }
}
