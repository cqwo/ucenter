package com.cqwo.ucenter.plugin.wechat;

import com.cqwo.ucenter.core.plugin.interface2.IOAuthPlugin;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


@Component(value = "PluginOpenService")
public class PluginService implements IOAuthPlugin {


    @Override
    public String name() {
        return "微信";
    }

    @Override
    public String desc() {
        return "微信登录";
    }

    @Override
    public String version() {
        return "1.0";
    }

    /**
     * 初始化插件
     */
    @PostConstruct
    @Override
    public void initPlugin() { //System.out.println("插件初始化成功");
    }

    @Override
    public String getLoginUrl() {
        return "/wechat/login";
    }
}
