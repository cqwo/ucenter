package com.cqwo.ucenter.web.controller;

import com.alibaba.fastjson.JSON;
import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import com.cqwo.ucenter.core.enums.authors.LoginType;
import com.cqwo.ucenter.core.exception.CWMException;
import com.cqwo.ucenter.core.exception.token.NoLoginException;
import com.cqwo.ucenter.core.exception.token.TokenException;
import com.cqwo.ucenter.core.exception.users.FailException;
import com.cqwo.ucenter.core.helper.StringHelper;
import com.cqwo.ucenter.services.WxOpenUtils;
import com.cqwo.ucenter.web.framework.model.UserTokenPasswordToken;
import com.cqwo.ucenter.web.model.OAuthorLoginModel;
import com.cqwo.wechat.open.web.domain.WxOpenOAuth2AccessToken;
import com.cqwo.wechat.open.web.domain.WxOpenOAuth2User;
import com.cqwo.wechat.open.web.exption.WxOpenErrorException;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;


@RestController(value = "WechatController")
public class WechatController extends BaseOpenAuthController {

    @Autowired
    private WxOpenUtils wxOpenUtils;

    private String server = "wechat";


    @GetMapping("goto_auth_url_show")
    public String gotoPreAuthUrlShow() {
        return "<a href='goto_auth_url'>go</a>";
    }

    /**
     * 登录
     *
     * @return
     */
    @RequestMapping(value = "login", method = RequestMethod.GET)
    public void login(@RequestParam(defaultValue = "") String returnUrl) {


        StringBuilder redirectUrl = new StringBuilder();

        redirectUrl.append("http://");
        redirectUrl.append(appConfig.getDomain());
        redirectUrl.append("/oauth/backcall");

        if (StringHelper.isNotNullOrWhiteSpace(returnUrl)) {
            redirectUrl.append("?returnUrl=").append(returnUrl);
        }


        //String url = "http://ucenter.510link.com/oauth/jump?returnUrl=https://iot.510link.com/login";
        try {
            String url = wxOpenUtils.getOauth2CodeUrl(redirectUrl.toString());
            response.sendRedirect(url);
        } catch (WxOpenErrorException | IOException ex) {
            logs.write(ex, "gotoPreAuthUrl");
            throw new RuntimeException(ex);
        }
    }


    /**
     * 登录返回
     *
     * @param authorizationCode 授权码
     * @param returnUrl         返回地址
     * @return
     */
    @RequestMapping(value = "backcall", method = RequestMethod.GET)
    public ModelAndView backCall(@RequestParam(value = "code") String authorizationCode,
                                 @RequestParam(defaultValue = "") String returnUrl) {
        try {

            if (StringHelper.isNullOrWhiteSpace(authorizationCode)) {
                return PromptView("用户登录失败");
            }

            WxOpenOAuth2AccessToken accessToken = wxOpenUtils.oauth2GetAccessToken(authorizationCode);

            if (accessToken == null || accessToken.getAccessToken().isEmpty()) {
                return PromptView("用户登录失败");
            }

            String oauthAppId = wxOpenUtils.getAppId();

            logs.write("WxOpenOAuth2AccessToken:" + JSON.toJSONString(accessToken));

            ;

            PartUserInfo userInfo = null;

            /*
             * 这位仁兄没注册过处理
             */
            if (oauths.existsOauthByOAuthAppId(server, accessToken.getOpenId(), oauthAppId)) {

                userInfo = users.getPartUserByOpenIdAndOAuthAppId(server, accessToken.getOpenId(), oauthAppId);

            } else {

                WxOpenOAuth2User wxOpenOAuth2User = wxOpenUtils.oauth2GetUserinfo(accessToken.getOpenId());


                if (wxOpenOAuth2User == null || wxOpenOAuth2User.getOpenId().isEmpty()) {
                    return PromptView("用户授权失败");
                }

                try {
                    userInfo = users.oauthLogin(server, wxOpenOAuth2User.getOpenId(), wxOpenOAuth2User.getUnionId(), wxOpenOAuth2User.getNickName(), wxOpenOAuth2User.getHeadimgurl(), wxOpenOAuth2User.getSex(), 0, oauthAppId);
                } catch (FailException e) {
                    e.printStackTrace();
                    return PromptView("http://www.510link.com/", "用户登录失败");
                }

                logs.write("wxOpenOAuth2User:" + JSON.toJSONString(wxOpenOAuth2User));
            }

            if (userInfo == null || userInfo.getUid().isEmpty()) {
                return PromptView("http://www.510link.com/", "用户登录失败");
            }

            String userToken = "";

            try {

                userToken = users.updateUserToken(userInfo.getUid()).getToken();

            } catch (TokenException | NoLoginException ex) {

                return PromptView("http://www.510link.com/", "用户信息异常");

            }

            Subject subject = SecurityUtils.getSubject();

            try {
                UserTokenPasswordToken token = new UserTokenPasswordToken(LoginType.TokenLogin, userToken);
                subject.login(token);
            } catch (UnknownAccountException ex) { //未找到用户信息

                //System.out.println("未找到用户信息" + ex.getMessage());

                return PromptView("http://www.510link.com/", "未找到用户信息");

            } catch (IncorrectCredentialsException ex) { //账号密码错误

                //System.out.println("账号密码错误" + ex.getMessage());
                return PromptView("http://www.510link.com/", "账号密码错误");

            } catch (LockedAccountException ex) { //账户锁定


                System.out.println("账号被锁定" + ex.getMessage());
                return PromptView("http://www.510link.com/", "账号被锁定");

            } catch (ExcessiveAttemptsException ex) { //登录次数超出限制

                System.out.println("登录次数超出限制" + ex.getMessage());
                return PromptView("登录次数超出限制");

            } catch (AuthenticationException ex) { //用户异常

                ex.printStackTrace();
                System.out.println("用户异常" + ex.getMessage());

                return PromptView("http://www.510link.com/", "用户异常");
                //unexpected errors?
            } catch (Exception ex) {
                System.out.println("登陆失败");
                System.out.println(ex.getMessage());
                return PromptView("http://www.510link.com/", "用户异常");
            }

            if (StringHelper.isNullOrWhiteSpace(returnUrl)) {

                if (subject.hasRole("admin")) {

                    return PromptView("/admin/index", "用户登录成功");
                }

                return PromptView("/", " 用户登录成功");

            }

            OAuthorLoginModel model = new OAuthorLoginModel(userToken, userInfo, returnUrl);

            logs.write("getQueryAuth" + accessToken);

            return View(model);

        } catch (Exception ex) {

            ex.printStackTrace();
            logs.write(ex, "gotoPreAuthUrl");

            return PromptView("http://www.510link.com/", "登录失败");
        }
    }


    /**
     * 绑定用户
     *
     * @return
     */
    @RequestMapping(value = "bind", method = RequestMethod.GET)
    public void bind(@RequestParam(defaultValue = "") String returnUrl,
                     @RequestParam(defaultValue = "") String uid) {

        if (StringHelper.isNullOrWhiteSpace(uid)) {
            throw new RuntimeException("用户UID错误");
        }

        PartUserInfo userInfo = users.getPartUserByUid(uid);

        if (userInfo == null || StringHelper.isNullOrWhiteSpace(userInfo.getUid())) {
            throw new RuntimeException("用户信息出错");
        }

        StringBuilder redirectUrl = new StringBuilder();

        redirectUrl.append("http://");
        redirectUrl.append(appConfig.getDomain());
        redirectUrl.append("/oauth/bindcall");
        redirectUrl.append("?uid=").append(uid);

        if (StringHelper.isNotNullOrWhiteSpace(returnUrl)) {
            redirectUrl.append("&returnUrl=").append(returnUrl);
        }


        //String url = "http://ucenter.510link.com/oauth/jump?returnUrl=https://iot.510link.com/login";
        try {
            String url = wxOpenUtils.getOauth2CodeUrl(redirectUrl.toString());
            response.sendRedirect(url);
        } catch (WxOpenErrorException | IOException ex) {
            logs.write(ex, "gotoPreAuthUrl");
            throw new RuntimeException(ex);
        }
    }


    /**
     * 绑定返回
     *
     * @param authorizationCode 授权码
     * @param uid               绑定用户信息
     * @param returnUrl         返回地址
     * @return
     */
    @RequestMapping(value = "bindcall", method = RequestMethod.GET)
    public ModelAndView bindCall(@RequestParam(value = "code") String authorizationCode,
                                 @RequestParam(defaultValue = "") String uid,
                                 @RequestParam(defaultValue = "") String returnUrl) {
        try {
            if (StringHelper.isNullOrWhiteSpace(returnUrl)) {
                returnUrl = "http://www.510link.com/";
            }

            if (StringHelper.isNullOrWhiteSpace(uid)) {
                throw new RuntimeException("用户UID错误");
            }

            if (StringHelper.isNullOrWhiteSpace(authorizationCode)) {
                return PromptView("用户登录失败");
            }

            PartUserInfo userInfo = users.getPartUserByUid(uid);

            if (userInfo == null || StringHelper.isNullOrWhiteSpace(userInfo.getUid())) {
                throw new RuntimeException("用户信息出错");
            }

            WxOpenOAuth2AccessToken accessToken = wxOpenUtils.oauth2GetAccessToken(authorizationCode);

            if (accessToken == null || accessToken.getAccessToken().isEmpty()) {
                return PromptView("用户登录失败");
            }

            String oauthAppId = wxOpenUtils.getAppId();

            logs.write("WxOpenOAuth2AccessToken:" + JSON.toJSONString(accessToken));

            if (oauths.existsOauthByOAuthAppId(server, accessToken.getOpenId(), oauthAppId)) {
                return PromptView("请勿重复绑定用户");
            }
            ;

            WxOpenOAuth2User wxOpenOAuth2User = wxOpenUtils.oauth2GetUserinfo(accessToken.getOpenId());

            if (wxOpenOAuth2User == null || wxOpenOAuth2User.getOpenId().isEmpty()) {
                return PromptView("用户授权失败");
            }

            try {

                users.bindUserOAuthor(userInfo, server, wxOpenOAuth2User.getOpenId(), wxOpenOAuth2User.getUnionId(), wxOpenOAuth2User.getNickName(), wxOpenOAuth2User.getHeadimgurl(), wxOpenOAuth2User.getSex(), 0, oauthAppId);

            } catch (FailException e) {

                e.printStackTrace();
                return PromptView(returnUrl, "用户绑定失败");

            }

            logs.write("wxOpenOAuth2User:" + JSON.toJSONString(wxOpenOAuth2User));


            return PromptView(returnUrl, " 用户绑定成功");


        } catch (Exception ex) {

            ex.printStackTrace();
            logs.write(ex, "gotoPreAuthUrl");

            if (StringHelper.isNullOrWhiteSpace(returnUrl)) {
                return PromptView("http://www.510link.com/", "用户绑定失败");
            }

            return PromptView(returnUrl, "用户绑定失败");
        }
    }


}
