package com.cqwo.ucenter.web.model;

import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 绑定用户
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OAuthorBindModel implements Serializable {

    private static final long serialVersionUID = 1312263458897030736L;
    /**
     * 用户信息
     */
    private PartUserInfo userInfo;

    /**
     * 返回地址
     */
    private String returnUrl;
}
