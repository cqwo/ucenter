

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 与String相关的工具方法集
 *
 * @author Jimmy
 */
public class StringUtil {

    public static String getMatcher(String regex, String source) {
        String result = "";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(source);
        while (matcher.find()) {
            result = matcher.group(1);
        }
        return result;
    }

    public static void main(String[] args) {


        String ss = "M=86230200\n" +
                "    S=0001\n" +
                "    CD=B0\n" +
                "    D=0000\n" +
                "    CLASS=ffff\n" +
                "    CR=3";


    }

}