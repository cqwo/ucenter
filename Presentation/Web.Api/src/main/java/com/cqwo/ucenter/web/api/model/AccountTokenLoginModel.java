package com.cqwo.ucenter.web.api.model;


import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountTokenLoginModel implements Serializable {

    private static final long serialVersionUID = 1345277050653050817L;

    @NotNull(message = "token不能为空")
    private String token = "";

}
