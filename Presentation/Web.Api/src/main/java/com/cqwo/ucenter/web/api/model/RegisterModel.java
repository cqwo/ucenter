package com.cqwo.ucenter.web.api.model;

import com.cqwo.ucenter.core.helper.ValidateHelper;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 注册模型
 * @Author: cqnews
 * @Date: 2018-09-29 11:39
 * @Version 1.0
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RegisterModel implements Serializable {

    private static final long serialVersionUID = 552403296763220198L;
    /**
     * 账户名
     */
    @NotBlank(message = "账号名不能为空")
    @Pattern(regexp = ValidateHelper.ACCOUNT_REGEX, message = "账号验证失败")
    private String account = "";

    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空")
    @Pattern(regexp = ValidateHelper.PASSWORD_REGEX, message = "密码验证失败")
    private String password = "";

    /**
     * 昵称
     */
    @NotBlank(message = "昵称不能为空")
    private String nickName = "英卡人";

    /**
     * 头像
     */
    @NotNull(message = "头像不能为空")
    private String avatar = "";

    @NotNull
    @Range(min = 0, max = 2, message = "请选择正确的性别")
    private Integer gender = 0;


    @NotNull(message = "请上报正确的行政编号")
    @Range(min = 0, message = "请上报正确的行政编号")
    private Integer regionId = 500107;

    /**
     * 密码
     */
    @NotNull(message = "超时时间不能为空")
    @Range(min = 0, max = 30 * 24, message = "超时时间不能为空")
    private Integer expireHours = 2;
}
