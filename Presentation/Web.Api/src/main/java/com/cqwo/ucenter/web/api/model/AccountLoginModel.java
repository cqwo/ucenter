package com.cqwo.ucenter.web.api.model;

import com.cqwo.ucenter.core.helper.ValidateHelper;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @Author: cqnews
 * @Date: 2018-09-28 14:51
 * @Version 1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AccountLoginModel implements Serializable {


    private static final long serialVersionUID = -5085583693366986474L;

    /**
     * 账号名称
     */
    @NotNull(message = "用户名不能为空")
    @Length(min = 5, max = 20, message = "用户名长度必须在6-20之间")
    @Pattern(regexp = ValidateHelper.ACCOUNT_REGEX, message = "用户名账号不合法")
    private String account = "";

    /**
     * 密码
     */
    @NotNull(message = "密码不能为空")
    @Length(min = 6, max = 30, message = "密码长度必须在6-20之间")
    private String password = "";


    /**
     * 是否刷新token
     */
    @NotNull(message = "是否刷新不能为空")
    @Range(min = 0, max = 1, message = "不正确的刷新参数")
    private Integer isRefresh = 1;


    /**
     * 超时时间
     */
    @NotNull(message = "超时时间不能为空")
    @Range(min = 0, max = 30 * 24, message = "超时时间不能为空")
    private Integer expireHours = 2;

}
