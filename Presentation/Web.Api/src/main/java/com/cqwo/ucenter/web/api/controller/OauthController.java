package com.cqwo.ucenter.web.api.controller;

import com.alibaba.fastjson.JSON;
import com.cqwo.ucenter.core.domain.users.OauthInfo;
import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import com.cqwo.ucenter.core.domain.users.UserRankInfo;
import com.cqwo.ucenter.core.domain.users.UserTokenInfo;
import com.cqwo.ucenter.core.exception.token.NoLoginException;
import com.cqwo.ucenter.core.exception.token.TokenException;
import com.cqwo.ucenter.core.exception.users.FailException;
import com.cqwo.ucenter.core.exception.users.RepeatException;
import com.cqwo.ucenter.core.helper.StringHelper;
import com.cqwo.ucenter.services.Users;
import com.cqwo.ucenter.web.api.model.LoginMessageModel;
import com.cqwo.ucenter.web.api.model.OauthRegisterModel;
import com.cqwo.ucenter.web.framework.controller.BaseApiController;
import com.cqwo.ucenter.web.framework.validate.ValidateModel;
import com.cqwo.ucenter.web.framework.validate.ValidationResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.cqwo.ucenter.core.errors.CWMConstants.*;


/**
 * 用户中心API开发登录
 */
@RestController(value = "ApiOauthController")
public class OauthController extends BaseApiController {

    /**
     * 用户注册
     *
     * @return 用户登录
     */
    @RequestMapping(value = {"oauth/login"}, method = RequestMethod.POST)
    public String login(OauthRegisterModel model) {

        System.out.println(JSON.toJSONString(model));

        ValidationResult result = ValidateModel.validateEntity(model);

        if (result.isNotErrors()) {

            PartUserInfo userInfo = null;


            try {

                userInfo = users.oauthLogin(model.getServer(),
                        model.getOpenId(),
                        model.getUnionId(),
                        model.getNickName(),
                        model.getAvatar(),
                        model.getGender(),
                        model.getRegionId(),
                        workContext.getAppInfo().getWxAppId());

            } catch (RepeatException ex) {

                userInfo = ex.getUserInfo();
//
//                ex.printStackTrace();
//                return JsonView(AUTHOR_REPEATUSER, "重复的账号信息");

            } catch (FailException ex) {

                ex.printStackTrace();
                return JsonView(AUTHOR_FAILUSER, "账号登录失败");

            }

            if (userInfo == null || userInfo.getUid().isEmpty()) {
                return JsonView(AUTHOR_FAILUSER, "账号注登录败");
            }

            userInfo = Users.securityUserInfo(userInfo);

            String token = "";
            String refreshToken = "";

            try {

                Integer expireHours = model.getExpireHours();

                if (expireHours == null || expireHours <= 0) {
                    expireHours = 2;
                }

                UserTokenInfo userTokenInfo = users.updateUserToken(userInfo.getUid(), workContext.getAppInfo().getAppId(), expireHours);

                token = userTokenInfo.getToken();
                refreshToken = userTokenInfo.getRefreshToken();

            } catch (TokenException | NoLoginException ex) {

                return JsonView(AUTHOR_AUTHENTICATION, "用户授权信息异常");

            }

            UserRankInfo rankInfo = userRanks.getUserRankByCredits(userInfo.getRankCredits());

            LoginMessageModel loginModel = new LoginMessageModel(userInfo.getUid(), token, refreshToken, userInfo, rankInfo);

            return JsonView(SUCCESS, loginModel, "用户登录成功");


        }
        return JsonView(VALIDATION_FAILED, "用户登录失败,code:" + result.toString());

    }


    /**
     * 查找用户授权信息
     *
     * @param uid uid
     * @return
     */
    @RequestMapping(value = "oauth/find")
    public String findOauth(@RequestParam(defaultValue = "") String uid) {


        if (StringHelper.isNullOrWhiteSpace(uid)) {
            return JsonView(VALIDATION_FAILED, "uid不能为空");
        }

        OauthInfo oauthInfo = oauths.getOauthByOAuthAppIdAndUid(workContext.getAppInfo().getWxAppId(), uid);

        if (oauthInfo == null) {
            return JsonView("用户信息未找到");
        }


        return JsonView(SUCCESS, oauthInfo, "用户授权信息加载成功");


    }
}
