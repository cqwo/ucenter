package com.cqwo.ucenter.web.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserUpdateMoneyModel implements Serializable {

    private static final long serialVersionUID = -3452078520806068398L;


    /**
     * Uid
     */
    @NotNull(message = "用户UID不能为空")
    @Length(min = 10, message = "UID不正确")
    private String uid = "";


    /**
     * 金额
     */
    @NotNull(message = "用户更新金额不能为空")
    public double money = 0.00;
}
