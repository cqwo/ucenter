package com.cqwo.ucenter.web.api.model;

import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;


/**
 * 用户修改
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserUpdateModel implements Serializable {

    private static final long serialVersionUID = -6833093345300630122L;

    /**
     * Uid
     */
    @NotNull(message = "用户UID不能为空")
    @Length(min = 10, message = "UID不正确")
    private String uid = "";

    /**
     * 昵称
     **/
    @NotNull(message = "用户昵称不能为空")
    private String nickName = "";
    /**
     * 真实姓名
     **/
    @NotNull(message = "真实姓名不能为空")
    private String realName = "";

    /**
     * 所在区域
     **/
    private int regionId = 0;


    /**
     * 地址
     **/
    @NotNull(message = "地址不能为空")
    private String address = "";

    /**
     * 用户详情
     */
    @NotNull(message = "用户详情不能为空")
    private String bio = "";


}
