package com.cqwo.ucenter.web.api.controller;

import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import com.cqwo.ucenter.core.errors.CWMConstants;
import com.cqwo.ucenter.core.helper.StringHelper;
import com.cqwo.ucenter.services.Users;
import com.cqwo.ucenter.web.api.model.UserListModel;
import com.cqwo.ucenter.web.api.model.UserUpdateModel;
import com.cqwo.ucenter.web.api.model.UserUpdateMoneyModel;
import com.cqwo.ucenter.web.framework.controller.BaseApiController;
import com.cqwo.ucenter.web.framework.validate.ValidateModel;
import com.cqwo.ucenter.web.framework.validate.ValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.cqwo.ucenter.core.errors.CWMConstants.SUCCESS;


/**
 * 用户控制器
 *
 * @author cqnews
 */
@RestController(value = "ApiUserController")
public class UserController extends BaseApiController {

    @Autowired
    private Users users;


    private final Lock lock = new ReentrantLock();


    /**
     * 通过Uid查找用户
     *
     * @param uid uid
     */
    @RequestMapping(value = "user/findbyuid")
    public String findUserByUid(@RequestParam(defaultValue = "") String uid) {

        if (uid.isEmpty()) {

            System.out.println("用户UID不正确" + uid);
            return JsonView("用户UID不正确");
        }

        PartUserInfo userInfo = users.getPartUserByUid(uid);

        if (userInfo == null || userInfo.getUid().isEmpty()) {
            return JsonView("用户不存在");
        }

        return JsonView(SUCCESS, Users.securityUserInfo(userInfo), "用户加载成功");
    }


    /**
     * 通过手机查找用户
     *
     * @param mobile 手机
     */
    @RequestMapping(value = "user/findbymobile")
    public String findUserByMobile(@RequestParam(defaultValue = "") String mobile) {

        if (StringHelper.isNullOrWhiteSpace(mobile)) {
            return JsonView("手机号码不正确");
        }

        PartUserInfo userInfo = users.getPartUserByMobile(mobile);

        if (userInfo == null || userInfo.getUid().isEmpty()) {
            return JsonView("用户不存在");
        }

        return JsonView(SUCCESS, Users.securityUserInfo(userInfo), "用户加载成功");
    }


    /**
     * 通过手机查找用户
     *
     * @param email 邮箱
     */
    @RequestMapping(value = "user/findbyemail")
    public String findUserByEmail(@RequestParam(defaultValue = "") String email) {

        if (StringHelper.isNullOrWhiteSpace(email)) {
            return JsonView("邮箱不正确");
        }

        PartUserInfo userInfo = users.getPartUserByEmail(email);

        if (userInfo == null || userInfo.getUid().isEmpty()) {
            return JsonView("用户不存在");
        }

        return JsonView(SUCCESS, Users.securityUserInfo(userInfo), "用户加载成功");

    }


    /**
     * 通过用户名查找用户
     *
     * @param userName 用户名
     */
    @RequestMapping(value = "user/findbyusername")
    public String findUserByUserName(@RequestParam(defaultValue = "") String userName) {

        if (StringHelper.isNullOrWhiteSpace(userName)) {
            return JsonView("用户名不正确");
        }

        PartUserInfo userInfo = users.getPartUserByUserName(userName);

        if (userInfo == null || userInfo.getUid().isEmpty()) {
            return JsonView("用户不存在");
        }

        return JsonView(SUCCESS, Users.securityUserInfo(userInfo), "用户加载成功");
    }


    /**
     * 查找用户列表
     *
     * @param pageSize   条数
     * @param pageNumber 当前页数
     * @param nickName   昵称
     * @param mobile     手机
     */
    @RequestMapping(value = "user/list")
    public String findUserList(@RequestParam(defaultValue = "10") Integer pageSize,
                               @RequestParam(defaultValue = "1") Integer pageNumber,
                               @RequestParam(defaultValue = "") String uid,
                               @RequestParam(defaultValue = "") String nickName,
                               @RequestParam(defaultValue = "") String mobile) {


        Specification<PartUserInfo> condition = users.getPartUserListCondition(uid, nickName, mobile);
        Sort sort = Sort.by(Sort.Direction.DESC, "createTime");
        Page<PartUserInfo> infoPage = users.getPartUserList(pageSize, pageNumber, condition, sort);

        if (infoPage == null || infoPage.getTotalElements() <= 0) {
            return JsonView("没有更多的数据了");
        }


        UserListModel model = new UserListModel(uid, nickName, mobile, infoPage.getContent(), pageSize, pageNumber, infoPage.getTotalElements());

        return JsonView(SUCCESS, model, "用户列表加载成功");
    }


    /**
     * 用户修改信息
     */
    @RequestMapping(value = "user/update", method = RequestMethod.POST)
    public String updatePost(UserUpdateModel model) {


        lock.lock();
        try {
            ValidationResult result = ValidateModel.validateEntity(model);

            if (result.isNotErrors()) {


                PartUserInfo userInfo = users.getPartUserByUid(model.getUid());

                if (userInfo == null || userInfo.getUid().isEmpty()) {
                    return JsonView("用户信息异常");
                }

                userInfo.setNickName(model.getNickName());
                userInfo.setRealName(model.getRealName());
                userInfo.setRegionId(model.getRegionId());
                userInfo.setAddress(model.getAddress());

                userInfo = users.updateUser(userInfo);

                if (userInfo == null || userInfo.getUid().isEmpty()) {
                    return JsonView("用户信息修改失败");
                }

                return JsonView(SUCCESS, userInfo, "用户信息修改成功");

            }

            return JsonView(CWMConstants.VALIDATION_FAILED, "用户数据校验失败," + result.toString());

        } catch (Exception ex) {

            logs.write(ex, "用户信息发生故障");
        } finally {
            lock.unlock();
        }


        return JsonView("信息校验失败");

    }

    /**
     * 用户修改信息
     */
    @RequestMapping(value = "user/updatemoney", method = RequestMethod.POST)
    public String updateMoney(UserUpdateMoneyModel model) {


        lock.lock();
        try {
            ValidationResult result = ValidateModel.validateEntity(model);

            if (result.isNotErrors()) {


                PartUserInfo userInfo = users.getPartUserByUid(model.getUid());

                if (userInfo == null || userInfo.getUid().isEmpty()) {
                    return JsonView("用户信息异常");
                }

                userInfo.setMoney(userInfo.getMoney() + model.getMoney());

                userInfo = users.updateUser(userInfo);

                if (userInfo == null || userInfo.getUid().isEmpty()) {
                    return JsonView("用户金额修改失败");
                }

                return JsonView(SUCCESS, userInfo, "用户金额修改成功");

            }

            return JsonView(CWMConstants.VALIDATION_FAILED, "用户数据校验失败," + result.toString());

        } catch (Exception ex) {

            logs.write(ex, "用户信息发生故障");
        } finally {
            lock.unlock();
        }


        return JsonView("信息校验失败");

    }

    /**
     * 重置用户密码
     *
     * @param uid         用户uid
     * @param newPassword 新密码
     * @return 返回
     */

    @RequestMapping(value = "user/resetpassword", method = RequestMethod.POST)
    public String resetPassword(@RequestParam(defaultValue = "") String uid,
                                @RequestParam(defaultValue = "") String newPassword) {
        lock.lock();

        try {

            PartUserInfo userInfo = users.getPartUserByUid(uid);

            if (userInfo == null || userInfo.getUid().isEmpty()) {
                return JsonView("用户信息异常");
            }

            String password = users.createUserPassword(newPassword, userInfo.getSalt());

            userInfo.setPassword(password);

            userInfo = users.updateUser(userInfo);

            if (userInfo == null || userInfo.getUid().isEmpty()) {
                return JsonView("用户信息修改失败");
            }

            return JsonView(SUCCESS, userInfo, "用户信息修改成功");

        } catch (Exception ex) {

            logs.write(ex, "用户信息发生故障");
        } finally {
            lock.unlock();
        }


        return JsonView("信息校验失败");

    }

    /**
     * 用户修改密码
     */
    @RequestMapping(value = "user/updatepassword", method = RequestMethod.POST)
    public String updatePassword(@RequestParam(defaultValue = "") String mobile,
                                 @RequestParam(defaultValue = "") String newPassword) {

        lock.lock();
        try {

            PartUserInfo userInfo = users.getPartUserByMobile(mobile);

            if (userInfo == null || userInfo.getUid().isEmpty()) {
                return JsonView("用户信息异常");
            }

            String password = users.createUserPassword(newPassword, userInfo.getSalt());

            userInfo.setPassword(password);

            userInfo = users.updateUser(userInfo);

            if (userInfo == null || userInfo.getUid().isEmpty()) {
                return JsonView("用户信息修改失败");
            }

            return JsonView(SUCCESS, userInfo, "用户信息修改成功");

        } catch (Exception ex) {

            logs.write(ex, "用户信息发生故障");
        } finally {
            lock.unlock();
        }


        return JsonView("信息校验失败");

    }

}
