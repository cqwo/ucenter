package com.cqwo.ucenter.web.api.model;

import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import com.cqwo.ucenter.web.framework.model.PageModel;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserListModel {

    /**
     * uid
     */
    private String uid = "";

    /**
     * 昵称
     */
    private String nickName = "";

    /**
     * 手机
     */
    private String mobile = "";

    /**
     * 用户列表
     */
    private List<PartUserInfo> userInfoList;

    /**
     * 分页模型
     */
    private Integer pageSize = 10;

    /**
     * 分页模型
     */
    private Integer pageNumber = 1;

    /**
     * 分页模型
     */
    private long totalCount = 0;


}
