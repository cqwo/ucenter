/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.web.api.model;

import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import com.cqwo.ucenter.core.domain.users.UserRankInfo;
import lombok.*;

import java.io.Serializable;

/**
 * Created by cqnews on 2017/12/25.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginMessageModel implements Serializable {

    private static final long serialVersionUID = -5745809786819093283L;
    /**
     * 用户登录成功Uid
     */
    @Builder.Default
    private String uid = "";


    /**
     * 用户登录token
     */
    @Builder.Default
    private String token = "";

    /**
     * 刷新密钥
     */
    @Builder.Default
    private String refreshToken = "";


    /**
     * 用户模型
     */
    private PartUserInfo userInfo;

    /**
     * 用户分组
     */
    private UserRankInfo userRankInfo;


}
