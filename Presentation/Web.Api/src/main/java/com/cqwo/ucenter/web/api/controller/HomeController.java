package com.cqwo.ucenter.web.api.controller;

import com.cqwo.ucenter.web.framework.controller.BaseApiController;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;


@RestController(value = "ApiHomeController")
public class HomeController extends BaseApiController {


    @RequestMapping(value = "index")
    public String index() {



        return "Hello,I'm API Controller";


    }


}
