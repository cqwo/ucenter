package com.cqwo.ucenter.web.api.controller;

import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import com.cqwo.ucenter.core.domain.users.UserRankInfo;
import com.cqwo.ucenter.core.domain.users.UserToken;
import com.cqwo.ucenter.core.domain.users.UserTokenInfo;
import com.cqwo.ucenter.core.enums.authors.LoginType;
import com.cqwo.ucenter.core.exception.token.NoLoginException;
import com.cqwo.ucenter.core.exception.token.TokenException;
import com.cqwo.ucenter.core.exception.users.FailException;
import com.cqwo.ucenter.core.exception.users.RepeatException;
import com.cqwo.ucenter.core.helper.DateHelper;
import com.cqwo.ucenter.core.helper.StringHelper;
import com.cqwo.ucenter.services.UserRanks;
import com.cqwo.ucenter.services.Users;
import com.cqwo.ucenter.web.api.model.AccountLoginModel;
import com.cqwo.ucenter.web.api.model.AccountTokenLoginModel;
import com.cqwo.ucenter.web.api.model.LoginMessageModel;
import com.cqwo.ucenter.web.api.model.RegisterModel;
import com.cqwo.ucenter.web.framework.controller.BaseApiController;
import com.cqwo.ucenter.web.framework.model.UserTokenPasswordToken;
import com.cqwo.ucenter.web.framework.validate.ValidateModel;
import com.cqwo.ucenter.web.framework.validate.ValidationResult;
import com.google.common.base.Strings;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.cqwo.ucenter.core.errors.CWMConstants.*;

/**
 * @Author: cqnews
 * @Date: 2018-09-28 14:40
 * @Version 1.0
 */
@RestController(value = "ApiAccountController")
public class AccountController extends BaseApiController {


    @Autowired
    private Users users;

    @Autowired
    private UserRanks userRanks;

    /**
     * 申明锁
     */
    private Lock lock = new ReentrantLock();

    /**
     * 用户登录
     *
     * @return String
     */
    @RequestMapping(value = {"account/login"})
    public String login(AccountLoginModel model) {

        ValidationResult result = ValidateModel.validateEntity(model);

        lock.lock();
        try {

            if (result.isNotErrors()) {

                System.out.println("数据验证成功");

                if (workContext.getAppInfo() == null) {
                    return JsonView(AUTHOR_AUTHENTICATION, "API信息异常");
                }

                // 获取主体
                Subject subject = SecurityUtils.getSubject();

                PartUserInfo userInfo = null;

                try {

                    UserTokenPasswordToken token = new UserTokenPasswordToken(model.getAccount(), model.getPassword());
                    subject.login(token);

                } catch (UnknownAccountException ex) { //未找到用户信息

                    return JsonView(AUTHOR_UNKNOWNACCOUNT, "未找到用户信息");

                } catch (IncorrectCredentialsException ex) { //账号密码错误
                    logs.write(ex);
                    return JsonView(AUTHOR_INCORRECTCREDENTIALS, "账号密码错误");

                } catch (LockedAccountException ex) { //账户锁定

                    logs.write(ex);
                    return JsonView(AUTHOR_LOCKEDACCOUNT, "账号被锁定");

                } catch (ExcessiveAttemptsException ex) { //登录次数超出限制
                    logs.write(ex);
                    return JsonView(AUTHOR_EXCESSIVEATTEMPTS, "登录次数超出限制");

                } catch (AuthenticationException ex) { //用户异常
                    logs.write(ex);
                    return JsonView(AUTHOR_AUTHENTICATION, "用户异常");
                    //unexpected errors?
                } catch (Exception ex) { //其它异常信息
                    logs.write(ex);
                    return JsonView(AUTHOR_ERRLOGIN, ex.getMessage());
                }

                userInfo = users.getPartUserByAccount(model.getAccount());

                if (userInfo == null || userInfo.getUid().isEmpty()) {
                    return JsonView(AUTHOR_AUTHENTICATION, "用户信息异常");
                }

                String token = "";
                String refreshToken = "";
                UserTokenInfo userTokenInfo = null;

                try {

                    if (model.getIsRefresh().equals(0)) {

                        userTokenInfo = users.findLastUserTokenByUid(userInfo.getUid(), workContext.getAppId());

                        if (!(userTokenInfo == null || Strings.isNullOrEmpty(userTokenInfo.getToken()))) {
                            token = userTokenInfo.getToken();
                            refreshToken = userTokenInfo.getRefreshToken();
                        }
                    }

                    if (model.getIsRefresh().equals(1) || Strings.isNullOrEmpty(token)) {

                        Integer expireHours = model.getExpireHours();

                        if (expireHours == null || expireHours <= 0) {
                            expireHours = 2;
                        }

                        userTokenInfo = users.updateUserToken(userInfo.getUid(), workContext.getAppInfo().getAppId(), expireHours);
                        token = userTokenInfo.getToken();
                        refreshToken = userTokenInfo.getRefreshToken();

                    }

                } catch (TokenException | NoLoginException ex) {
                    ex.printStackTrace();
                    return JsonView(AUTHOR_AUTHENTICATION, "用户异常");
                }

                UserRankInfo rankInfo = userRanks.getUserRankByCredits(userInfo.getRankCredits());


                LoginMessageModel loginModel = new LoginMessageModel(userInfo.getUid(), token, refreshToken, userInfo, rankInfo);


                return JsonView(SUCCESS, loginModel, "用户登录成功");
            }

            return JsonView(VALIDATION_FAILED, "数据校验失败:" + result.toString());

        } catch (Exception ignored) {

        } finally {
            lock.unlock();
        }

        return JsonView(AUTHOR_AUTHENTICATION, "用户异常");
    }

    /**
     * 用户登录
     *
     * @return String
     */
    @RequestMapping(value = {"account/tokenlogin"})
    public String tokenLogin(AccountTokenLoginModel model) {

        ValidationResult result = ValidateModel.validateEntity(model);

        if (result.isNotErrors()) {

            System.out.println("数据验证成功");


            String token = model.getToken();

            if (StringHelper.isEmpty(token)) {

                return JsonView(VALIDATION_FAILED, "Token为空");
            }


            UserToken userToken = users.decryptUserToken(token);

            if (userToken == null) {
                return JsonView(TOKEN_ERROR, "token解读失败,token:" + token);
            }

            UserTokenInfo tokenInfo = null;

            try {

                tokenInfo = users.findUserToken(token);

            } catch (TokenException ex) {

                return JsonView(ex.getMessage());

            }

            if (tokenInfo == null || tokenInfo.getId() <= 0) {

                return JsonView(TOKEN_EXPIRE, "Token过期");

            }

            Integer nowTime = DateHelper.getUnixTimeStamp();

            if (tokenInfo.getLimitTime() < nowTime) {

                return JsonView(TOKEN_EXPIRE, "Token过期");

            }

            if (!token.equals(tokenInfo.getToken())) {

                return JsonView(TOKEN_EXPIRE, "校验失败");

            }


            // 获取主体
            Subject subject = SecurityUtils.getSubject();

            PartUserInfo userInfo = null;

            try {

                UserTokenPasswordToken passwordToken = new UserTokenPasswordToken(
                        LoginType.TokenLogin,
                        model.getToken());
                subject.login(passwordToken);

            } catch (UnknownAccountException ex) { //未找到用户信息

                return JsonView(AUTHOR_UNKNOWNACCOUNT, "未找到用户信息");

            } catch (IncorrectCredentialsException ex) { //账号密码错误

                logs.write(ex);
                return JsonView(AUTHOR_INCORRECTCREDENTIALS, "账号或密码错误");

            } catch (LockedAccountException ex) { //账户锁定

                logs.write(ex);
                return JsonView(AUTHOR_LOCKEDACCOUNT, "账号被锁定");

            } catch (ExcessiveAttemptsException ex) { //登录次数超出限制
                logs.write(ex);
                return JsonView(AUTHOR_EXCESSIVEATTEMPTS, "登录次数超出限制");

            } catch (AuthenticationException ex) { //用户异常
                logs.write(ex);
                return JsonView(AUTHOR_AUTHENTICATION, "账号或密码错误");
                //unexpected errors?
            } catch (Exception ex) { //其它异常信息
                logs.write(ex);
                return JsonView(AUTHOR_ERRLOGIN, ex.getMessage());
            }


            userInfo = users.getPartUserByUid(tokenInfo.getUid());

            if (userInfo == null || userInfo.getUid().isEmpty()) {
                return JsonView(AUTHOR_AUTHENTICATION, "账号或密码错误");
            }


            String refreshToken = tokenInfo.getRefreshToken();

            UserRankInfo rankInfo = userRanks.getUserRankByCredits(userInfo.getRankCredits());


            LoginMessageModel loginModel = new LoginMessageModel(userInfo.getUid(), token, refreshToken, userInfo, rankInfo);


            return JsonView(SUCCESS, loginModel, "用户登录成功");
        }

        return JsonView(VALIDATION_FAILED, "数据校验失败:" + result.getFirstMessage());

    }

    /**
     * 用户注册
     *
     * @return 返回注册信息
     */
    @RequestMapping(value = {"account/register"}, method = RequestMethod.POST)
    public String register(RegisterModel model) {


        lock.lock();

        try {

            ValidationResult result = ValidateModel.validateEntity(model);

            if (result.isNotErrors()) {

                PartUserInfo userInfo = null;

                try {
                    userInfo = users.createSimpleUserInfo(model.getAccount(), model.getPassword(), model.getNickName(), model.getAvatar(), model.getGender(), model.getRegionId());
                } catch (RepeatException e) {

                    e.printStackTrace();
                    return JsonView(AUTHOR_REPEATUSER, "重复的账号信息");

                } catch (FailException e) {

                    e.printStackTrace();
                    return JsonView(AUTHOR_FAILUSER, "账号注册失败");

                }

                if (userInfo == null || userInfo.getUid().isEmpty()) {
                    return JsonView(AUTHOR_FAILUSER, "账号注册失败");
                }

                userInfo = Users.securityUserInfo(userInfo);

                String token = "";
                String refreshToken = "";
                try {

                    Integer expireHours = model.getExpireHours();

                    if (expireHours == null || expireHours <= 0) {
                        expireHours = 2;
                    }

                    UserTokenInfo userTokenInfo = users.updateUserToken(userInfo.getUid(), workContext.getAppInfo().getAppId(), expireHours);
                    token = userTokenInfo.getToken();
                    refreshToken = userTokenInfo.getRefreshToken();

                } catch (TokenException | NoLoginException ex) {
                    return JsonView(AUTHOR_AUTHENTICATION, "用户注册信息异常");
                }

                UserRankInfo rankInfo = userRanks.getUserRankByCredits(userInfo.getRankCredits());

                LoginMessageModel loginModel = new LoginMessageModel(userInfo.getUid(), token, refreshToken, userInfo, rankInfo);

                return JsonView(SUCCESS, loginModel, "用户注册成功");

            }
            return JsonView(VALIDATION_FAILED, "用户注册失败,code:" + result.toString());
        } catch (Exception ex) {
            return JsonView(VALIDATION_FAILED, "用户注册失败");
        } finally {
            lock.unlock();
        }

    }


    /**
     * 用户修改密码
     */
    @RequestMapping(value = "account/updatepassword", method = RequestMethod.POST)
    public String updatePassword(@RequestParam(defaultValue = "") String mobile,
                                 @RequestParam(defaultValue = "") String newPassword) {

        lock.lock();
        try {

            PartUserInfo userInfo = users.getPartUserByMobile(mobile);

            if (userInfo == null || userInfo.getUid().isEmpty()) {
                return JsonView("用户信息异常");
            }

            String password = users.createUserPassword(newPassword, userInfo.getSalt());

            userInfo.setPassword(password);

            userInfo = users.updateUser(userInfo);

            if (userInfo == null || userInfo.getUid().isEmpty()) {
                return JsonView("用户信息修改失败");
            }

            return JsonView(SUCCESS, userInfo, "用户信息修改成功");

        } catch (Exception ex) {

            logs.write(ex, "用户信息发生故障");
        } finally {
            lock.unlock();
        }


        return JsonView("信息校验失败");

    }


}
