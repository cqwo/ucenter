package com.cqwo.ucenter.web.api.controller;

import com.cqwo.ucenter.core.domain.users.UserToken;
import com.cqwo.ucenter.core.domain.users.UserTokenInfo;
import com.cqwo.ucenter.core.exception.token.NoLoginException;
import com.cqwo.ucenter.core.exception.token.TokenException;
import com.cqwo.ucenter.core.helper.DateHelper;
import com.cqwo.ucenter.core.helper.StringHelper;
import com.cqwo.ucenter.services.Users;
import com.cqwo.ucenter.web.api.model.RefreshTokenModel;
import com.cqwo.ucenter.web.framework.controller.BaseApiController;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static com.cqwo.ucenter.core.errors.CWMConstants.*;

/**
 * token管理
 *
 * @Author: cqnews
 * @Date: 2018-09-30 11:12
 * @Version 1.0
 */
@RestController(value = "ApiTokenController")
public class TokenController extends BaseApiController {

    @Autowired
    private Users users;

    /**
     * 解读token
     *
     * @param token token
     * @return
     */
    @RequestMapping(value = "token/read")
    public String readToken(@RequestParam(defaultValue = "") String token) {


        UserToken userToken = users.decryptUserToken(token);

        if (userToken == null) {
            return JsonView("token解读失败3");
        }

        return JsonView(SUCCESS, userToken, "token解读成功32");

    }

    /**
     * 更新token
     *
     * @return String
     */
    @RequestMapping(value = {"token/update", "token/refresh"})
    public String updateToken(@RequestParam(defaultValue = "") String uid,
                              @RequestParam(defaultValue = "") String refreshToken,
                              @RequestParam(defaultValue = "2") Integer expireHours) {

        try {

            UserTokenInfo userTokenInfo = users.findUserTokenByUidAndRefreshToken(uid, workContext.getAppInfo().getAppId(), refreshToken);

            if (userTokenInfo == null || Strings.isNullOrEmpty(userTokenInfo.getToken())) {
                return JsonView("用户token更新失败");
            }

            if (userTokenInfo.getRefreshToken().equals(refreshToken)) {

                if (expireHours == null || expireHours <= 0) {
                    expireHours = 2;
                }

                userTokenInfo = users.updateUserToken(uid, workContext.getAppInfo().getAppId(), expireHours);

                if (userTokenInfo == null || userTokenInfo.getId() <= 0) {
                    return JsonView("Token刷新失败");
                }

                String token = userTokenInfo.getToken();

                refreshToken = userTokenInfo.getRefreshToken();

                RefreshTokenModel model = RefreshTokenModel.builder().uid(uid).token(token).refreshToken(refreshToken).build();


                return JsonView(SUCCESS, model, "Token更新成功");
            }


        } catch (TokenException | NoLoginException ex) {
            return JsonView(ex.getMessage());
        }
        return JsonView("用户token更新失败");
    }


    /**
     * 校验token签名
     *
     * @return
     */
    @RequestMapping(value = "token/checksignature")
    public String checkTokenSignature(@RequestParam(defaultValue = "") String token) {

        if (StringHelper.isEmpty(token)) {

            return JsonView(VALIDATION_FAILED, "Token为空");
        }

        UserToken userToken = users.decryptUserToken(token);

        if (userToken == null) {
            return JsonView(TOKEN_ERROR, "token解读失败2");
        }

        UserTokenInfo tokenInfo = null;
        try {
            tokenInfo = users.findUserToken(token);
        } catch (TokenException ex) {
            return JsonView(ex.getMessage());
        }

        if (tokenInfo == null || tokenInfo.getId() <= 0) {
            return JsonView(TOKEN_EXPIRE, "Token过期");
        }

        Integer nowTime = DateHelper.getUnixTimeStamp();

        if (tokenInfo.getLimitTime() < nowTime) {
            return JsonView(TOKEN_EXPIRE, "Token过期");
        }

        if (!token.equals(tokenInfo.getToken())) {
            return JsonView(TOKEN_EXPIRE, "校验失败");
        }


        return JsonView(SUCCESS, userToken, "token校验证成功");
    }

}
