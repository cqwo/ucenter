package com.cqwo.ucenter.web.api.model;

import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OauthRegisterModel implements Serializable {


    private static final long serialVersionUID = 7575999437897343596L;
    /**
     * 服务
     */
    @NotNull(message = "请选择正确的服务")
    @Length(min = 2, max = 10, message = "请选择正确的服务")
    private String server = "wechat";

    /**
     * 开发Id
     */
    @NotNull(message = "请上报正确的openId")
    @Length(min = 2, max = 50, message = "请上报正确的openId")
    private String openId = "";

    /**
     * 联合Id
     */
    @NotNull(message = "请上报正确的unionId")
    private String unionId = "";

    /**
     * 昵称
     */
    @NotNull(message = "请上报正确的昵称")
    @Length(min = 1, max = 30, message = "请上报正确的昵称")
    private String nickName = "";

    /**
     * 头像
     */
    @NotNull(message = "请上报正确的头像")
    @Length(min = 10, max = 150, message = "请上报正确的头像")
    private String avatar = "";

    /**
     * 性别
     */
    @NotNull(message = "请上报正确的性别")
    @Range(min = 0, max = 2, message = "请上报正确的性别")
    private Integer gender = 0;


    /**
     * 行政区域编号
     */
    @NotNull(message = "请上报正确的行政区域编号")
    @Range(min = 0, message = "请上报正确的行政区域编号")
    private Integer regionId = 500107;


    /**
     * 密码
     */
    @NotNull(message = "超时时间不能为空")
    @Range(min = 0, max = 30 * 24, message = "超时时间不能为空")
    private Integer expireHours = 2;
}
