package com.cqwo.ucenter.web.api.controller;

import com.cqwo.ucenter.core.errors.CWMConstants;
import com.cqwo.ucenter.web.framework.controller.BaseApiController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 错误异常
 *
 * @author cqnews
 */
@RestController(value = "ApiErrorController")
public class ErrorController extends BaseApiController {


    /**
     * 未授权
     *
     * @return
     */
    @RequestMapping(value = "error/403")
    public String error403() {

        return JsonView(CWMConstants.AUTHOR_FAILED, "错误的授权");
    }

}
