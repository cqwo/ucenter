/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.web.admin.controller;


import com.cqwo.ucenter.core.domain.base.RegionInfo;
import com.cqwo.ucenter.services.Regions;
import com.cqwo.ucenter.services.Uploads;
import com.cqwo.ucenter.web.framework.controller.BaseAdminController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

import static com.cqwo.ucenter.core.errors.CWMConstants.SUCCESS;

/**
 * Created by cqnews on 2017/12/21.
 */
@Controller
@Component(value = "AdminToolController")
public class ToolController extends BaseAdminController {

    @Autowired
    private Uploads uploads;

    @Autowired
    private Regions regions;

    /**
     * 首级列表
     *
     * @return
     */
    @RequestMapping(value = "tool/getprovincelist")
    @ResponseBody
    public String provinceList() {

        List<RegionInfo> regionList = regions.getRegionProvinceList();

        return JsonView(1, regionList, "列表加载成功");
    }

    /**
     * 市列表
     *
     * @param provinceId 省id
     * @return
     */
    @RequestMapping(value = "tool/getcitylist")
    @ResponseBody
    public String getCityList(@RequestParam(defaultValue = "110000") int provinceId) {

        List<RegionInfo> regionList = regions.getRegionCityList(provinceId);

        return JsonView(1, regionList, "市列表加载成功");
    }

    /**
     * 县或区列表
     *
     * @param cityId 市id
     * @return
     */
    @RequestMapping(value = "tool/getcountylist")
    @ResponseBody
    public String getCountyList(@RequestParam(defaultValue = "110100") int cityId) {

        List<RegionInfo> regionList = regions.getRegionCountyList(cityId);

        return JsonView(1, regionList, "县或区列表加载成功");
    }


    @RequestMapping(value = "tool/upload2")
    @ResponseBody
    public String uploaddemo(MultipartHttpServletRequest request) {

        Map<String, MultipartFile> files = request.getFileMap();

        return "";
    }

    /**
     * 图片上传
     *
     * @param operation
     * @return
     */
    @RequestMapping(value = "tool/upload")
    @ResponseBody
    public String upload(String operation, MultipartHttpServletRequest request) {


        Map<String, MultipartFile> requestFileMap = request.getFileMap();

        if (requestFileMap.size() <= 0) {

            return "-1";
        }

        /**
         * 产品
         */
        if ("uploadproductimage".equals(operation))//上传广告图片
        {
            List<String> litpicList = uploads.fileUpload("product", requestFileMap);

            if (litpicList.size() >= 1) {

                return JsonView(SUCCESS, litpicList.get(0), "图片上传成功");
            }


        }


        /**
         * 产品
         */
        if ("uploadproducteditorimage".equals(operation))//上传广告图片
        {
            List<String> litpic = uploads.fileUpload("product", requestFileMap);

            if (litpic.size() >= 1) {
                return MessageFormat.format("{2}\"url\":\"{0}\",\"state\":\"{1}\",\"originalName\":\"\",'name':\"\",\"size\":\"\",\"loginType\":\"\"{3}", litpic.get(0), getUEState(litpic.get(0)), "{", "}");

            }
        }

        //新闻
        if ("uploadnewsimage".equals(operation))//上传广告图片
        {
            List<String> litpicList = uploads.fileUpload("news", requestFileMap);

            if (litpicList.size() >= 1) {

                return JsonView(SUCCESS, litpicList.get(0), "图片上传成功");
            }

        }

        //新闻
        if ("uploadnewseditorimage".equals(operation))//上传广告图片
        {
            List<String> litpic = uploads.fileUpload("news", requestFileMap);

            if (litpic.size() >= 1) {
                return MessageFormat.format("{2}\"url\":\"{0}\",\"state\":\"{1}\",\"originalName\":\"\",'name':\"\",\"size\":\"\",\"loginType\":\"\"{3}", litpic.get(0), getUEState(litpic.get(0)), "{", "}");

            }
        }

        if ("uploaddeviceimage".equals(operation)) {

            List<String> litpic = uploads.fileUpload("device", requestFileMap);
            if (litpic.size() >= 1) {

                return JsonView(SUCCESS, litpic.get(0), "图片上传成功");
            }

        }

        if ("uploadecologicimage".equals(operation)) {

            List<String> litpic = uploads.fileUpload("news", requestFileMap);

            if (litpic.size() >= 1) {

                return JsonView(SUCCESS, litpic.get(0), "图片上传成功");
            }

        }

        return "-2";
    }


//    /**
//     * 设备分组信息
//     * @param productId
//     * @return
//     */
//    public String getGroupList(Integer productId) {
//
//    }

    /**
     * 获得ueditor状态
     *
     * @param result 上传结果
     * @return
     */
    private String getUEState(String result) {
        if (result == null) {
            return "上传图片失败";
        } else if ("-2".equals(result)) {
            return "不允许的图片类型";
        } else if ("-3".equals(result)) {
            return "图片大小超出网站限制";
        } else {
            return "SUCCESS";
        }
    }

}
