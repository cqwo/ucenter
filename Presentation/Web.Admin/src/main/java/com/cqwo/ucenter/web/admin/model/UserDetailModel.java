package com.cqwo.ucenter.web.admin.model;

import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by 友财
 * 2018/11/15 11:32
 *
 * @author 87242*/
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserDetailModel {

    /**
     * 用户信息
     */
    private PartUserInfo userInfo;
}
