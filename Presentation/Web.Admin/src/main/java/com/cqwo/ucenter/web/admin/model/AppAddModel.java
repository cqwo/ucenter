package com.cqwo.ucenter.web.admin.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by 友财
 * 2018/11/15 16:05
 *
 * @author 87242
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppAddModel implements Serializable {

    private static final long serialVersionUID = -701588685114521876L;

    /**
     * 开发者ID
     */
    @NotNull(message = "开发者ID不能为空")
    @Length(min = 1, max = 200, message = "开发者ID长度必须在1-200之间")
    private String appId = "";

    /**
     * 开发者名称
     */
    @NotNull(message = "开发者名称不能为空")
    @Length(min = 1, max = 200, message = "开发者名称长度必须在1-200之间")
    private String appName = "";


    /**
     * 开发者key
     */
    @NotNull(message = "开发者key不能为空")
    @Length(min = 1, max = 200, message = "开发者key长度必须在1-200之间")
    private String apiKey = "";


    /**
     * 开发者密码
     */
    @NotNull(message = "开发者密码不能为空")
    @Length(min = 1, max = 200, message = "开发者密码长度必须在1-200之间")
    private String apiSecret = "";

    /**
     * ASE私钥
     */
    @NotNull(message = "ASE私钥不能为空")
    @Length(min = 1, max = 200, message = "ASE私钥长度必须在1-200之间")
    private String aesSecret = "";


    /**
     * 项目描述
     */
    @NotNull(message = "项目描述不能为空")
    @Length(min = 0, max = 200, message = "项目描述长度必须在1-200之间")
    private String description = "";

    /**
     * 微信AppId
     */
    @NotNull(message = "微信AppId不能为空")
    @Length(min = 0, max = 200, message = "微信AppId长度必须在1-200之间")
    private String wxAppId = "";

    /**
     * 微信秘钥
     */
    @NotNull(message = "微信秘钥不能为空")
    @Length(min = 0, max = 200, message = "微信秘钥长度必须在1-200之间")
    private String wxSecret = "";

    /**
     * 微信通信息密钥
     */
    @NotNull(message = "通信息密钥不能为空")
    @Length(min = 0, max = 200, message = "通信息密钥长度必须在1-200之间")
    private String wxAesKey = "";
}
