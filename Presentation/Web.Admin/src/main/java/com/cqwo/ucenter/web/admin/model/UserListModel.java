package com.cqwo.ucenter.web.admin.model;

import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import com.cqwo.ucenter.web.framework.model.PageModel;
import lombok.*;

import java.util.List;


@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserListModel {


    /**
     * 用户uid
     */
    private String uid = "";

    /**
     * 用户昵称
     */
    private String nickName = "";

    /**
     * 手机号码
     */
    private String mobile = "";

    /**
     * 用户列表
     */
    private List<PartUserInfo> userInfoList;

    /**
     * 分页模型
     */
    private PageModel pageModel;


    public UserListModel(List<PartUserInfo> userInfoList, PageModel pageModel) {

        this.userInfoList = userInfoList;
        this.pageModel = pageModel;

    }
}
