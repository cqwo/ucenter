package com.cqwo.ucenter.web.admin.controller;

import com.cqwo.ucenter.core.domain.app.AppInfo;
import com.cqwo.ucenter.core.helper.DateHelper;
import com.cqwo.ucenter.services.Apps;
import com.cqwo.ucenter.web.admin.model.AppAddModel;
import com.cqwo.ucenter.web.admin.model.AppEditModel;
import com.cqwo.ucenter.web.admin.model.ApplicationListModel;
import com.cqwo.ucenter.web.framework.controller.BaseAdminController;
import com.cqwo.ucenter.web.framework.model.PageModel;
import com.cqwo.ucenter.web.framework.validate.ValidateModel;
import com.cqwo.ucenter.web.framework.validate.ValidationResult;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.text.MessageFormat;

/**
 * 应用管理
 *
 * @Author: cqnews
 * @Date: 2018-09-29 16:42
 * @Version 1.0
 */
@Controller(value = "AdminAppController")
@RequiresRoles(value = "admin")
public class AppController extends BaseAdminController {


    @Autowired
    private Apps apps;

    /**
     * 首页
     *
     * @return
     */
    @RequestMapping(value = "app/index")
    public ModelAndView index() {
        return View();
    }

    /**
     * 应用列表
     *
     * @param appName    app名称
     * @param pageSize   每页的条数
     * @param pageNumber 当前页数
     * @return
     */
    @RequestMapping(value = "app/list")
    public ModelAndView list(@RequestParam(defaultValue = "") String appName,
                             @RequestParam(defaultValue = "10") Integer pageSize,
                             @RequestParam(defaultValue = "1") Integer pageNumber) {

        Specification<AppInfo> condition = apps.getAppListCondition(appName);

        Sort sort =  Sort.by(Sort.Direction.DESC, "appid");

        Page<AppInfo> infoPage = apps.getAppList(pageSize, pageNumber, condition, sort);

        if (infoPage == null) {
            return PromptView("没有更多的数据了");
        }

        PageModel pageModel = new PageModel(pageSize, pageNumber, infoPage.getTotalElements());


        ApplicationListModel model = new ApplicationListModel(appName, infoPage.getContent(), pageModel);


        cwmUtils.setAdminRefererCookie(response, MessageFormat.format("{0}?pageSize={1}&pageNumber={2}&appName={3}", cwmUtils.getRawUrl(), pageSize, pageNumber, appName));

        return View(model);
    }


    /**
     * 应用添加
     *
     * @return
     */
    @RequestMapping(value = "app/add", method = {RequestMethod.GET})
    public ModelAndView addGet() {
        return View();
    }

    /**
     * 应用添加
     *
     * @param mode
     * @return
     */
    @RequestMapping(value = "app/add", method = {RequestMethod.POST})
    public ModelAndView addPost(AppAddModel mode) {


        ValidationResult result = ValidateModel.validateEntity(mode);

        if (result.isNotErrors()) {

            Integer nowTime = DateHelper.getUnixTimeStamp();


            AppInfo appInfo = apps.getAppByAppId(mode.getAppId());

            if (appInfo != null) {
                return PromptView("已存在相同的AppId");
            }

            appInfo = new AppInfo();

            appInfo.setAppId(mode.getAppId());
            appInfo.setAppName(mode.getAppName());
            appInfo.setApiKey(mode.getApiKey());
            appInfo.setApiSecret(mode.getApiSecret());
            appInfo.setAesSecret(mode.getAesSecret());

            appInfo.setWxAppId(mode.getWxAppId());
            appInfo.setWxAesKey(mode.getWxAesKey());
            appInfo.setWxSecret(mode.getWxSecret());

            appInfo.setDescription(mode.getDescription());
            ;

            appInfo = apps.createApp(appInfo);

            if (appInfo == null || appInfo.getId() <= 0) {
                return PromptView("应用添加失败");
            }

            return PromptView("应用添加成功");
        }
        return PromptView("添加设备成功");
    }


    /**
     * 应用修改
     *
     * @param id id
     * @return
     */
    @RequestMapping(value = "app/edit", method = RequestMethod.GET)
    public ModelAndView editGet(@RequestParam(defaultValue = "0") Integer id) {

        if (id <= 0) {
            return PromptView("应用不存在");
        }

        AppInfo appInfo = apps.getAppById(id);

        if (appInfo == null || appInfo.getId() <= 0) {
            return PromptView("应用信息不存在");
        }
        AppEditModel model = new AppEditModel();

        return View(model);
    }

}
