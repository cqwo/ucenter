package com.cqwo.ucenter.web.admin.model;

import com.cqwo.ucenter.core.domain.app.AppInfo;
import com.cqwo.ucenter.web.framework.model.PageModel;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: cqnews
 * @Date: 2018-09-29 16:57
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationListModel implements Serializable {


    private static final long serialVersionUID = -4814482765582321504L;

    /**
     * App名称
     */
    String appName = "";

    /**
     * 列表
     */
    List<AppInfo> appInfoList;


    /**
     * 分页模型
     */
    PageModel pageModel;
}
