package com.cqwo.ucenter.web.admin.controller;

import com.cqwo.ucenter.core.domain.base.RegionInfo;
import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import com.cqwo.ucenter.core.domain.users.UserRankInfo;
import com.cqwo.ucenter.core.helper.StringHelper;
import com.cqwo.ucenter.core.helper.TypeHelper;
import com.cqwo.ucenter.core.model.SelectListItem;
import com.cqwo.ucenter.services.Regions;
import com.cqwo.ucenter.services.UserRanks;
import com.cqwo.ucenter.services.Users;
import com.cqwo.ucenter.web.admin.model.UserDetailModel;
import com.cqwo.ucenter.web.admin.model.UserEditModel;
import com.cqwo.ucenter.web.admin.model.UserListModel;
import com.cqwo.ucenter.web.framework.controller.BaseAdminController;
import com.cqwo.ucenter.web.framework.model.PageModel;
import com.cqwo.ucenter.web.framework.validate.ValidateModel;
import com.cqwo.ucenter.web.framework.validate.ValidationResult;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * 管理中心用户模块
 */
@Controller(value = "AdminUserController")
@RequiresRoles(value = "admin")
public class UserController extends BaseAdminController {


    @Autowired
    private Users users;

    @Autowired
    private UserRanks userRanks;

    @Autowired
    private Regions regions;

    private Lock lock = new ReentrantLock();

    /**
     * 用户中心首页
     *
     * @return
     */
    @RequestMapping(value = "user/index")
    public ModelAndView index() {
        return View();
    }


    /**
     * 用户记中心列表
     *
     * @param pageSize   每页条数
     * @param pageNumber 当前页数
     * @return
     */
    @RequestMapping(value = "user/list")
    public ModelAndView list(@RequestParam(defaultValue = "10") Integer pageSize,
                             @RequestParam(defaultValue = "1") Integer pageNumber,
                             UserListModel model) {

        Specification<PartUserInfo> condition = users.getPartUserListCondition(model.getUid().trim(), model.getNickName().trim(), model.getMobile().trim());
        Sort sort = Sort.by(Sort.Direction.DESC, "createTime");

        Page<PartUserInfo> infoPage = users.getPartUserList(pageSize, pageNumber, condition, sort);

        if (infoPage == null) {
            return PromptView("没有更多的数据");
        }

        List<PartUserInfo> userInfoList = infoPage.getContent();

        PageModel pageModel = new PageModel(pageSize, pageNumber, infoPage.getTotalElements());

        cwmUtils.setAdminRefererCookie(response, MessageFormat.format("{0}?pageSize={1}&pageNumber={2}", cwmUtils.getRawUrl(), pageSize, pageNumber));


        model = new UserListModel(userInfoList, pageModel);

        return View(model);


    }

    /**
     * 修改用户信息
     *
     * @param uid 用户uid
     * @return
     */
    @RequestMapping(value = "user/edit", method = RequestMethod.GET)
    public ModelAndView editGet(@RequestParam(defaultValue = "0") String uid) {


        if (uid == null || uid.isEmpty()) {
            return PromptView("用户uid不正确");
        }

        PartUserInfo userInfo = users.getPartUserByUid(uid);

        if (userInfo == null) {
            return PromptView("用户不存在");
        }

        UserEditModel model = new UserEditModel();

        model.setUid(userInfo.getUid());
        model.setUserName(userInfo.getUserName());
        model.setEmail(userInfo.getEmail());
        model.setMobile(userInfo.getMobile());
        if (!StringHelper.isEmpty(model.getPassword())) {
            model.setPassword(model.getPassword());
        }
        model.setUserRid(userInfo.getUserRid());
        model.setNickName(userInfo.getNickName());
        model.setRealName(userInfo.getRealName());
        model.setRegionId(userInfo.getRegionId());
        model.setAddress(userInfo.getAddress());
        model.setBio("");

        List<SelectListItem> userRankItmeList = new ArrayList<SelectListItem>();


        List<UserRankInfo> userRankInfoList = userRanks.getAllUserRankList();

        for (UserRankInfo info : userRankInfoList) {

            SelectListItem item = new SelectListItem();

            item.setText(info.getTitle());
            item.setValue(TypeHelper.intToString(info.getUserRid()));


            if (info.getUserRid().equals(userInfo.getUserRid())) {
                item.setSelected(true);
            }

            userRankItmeList.add(item);
        }

        model.setUserRankItemList(userRankItmeList);

        RegionInfo regionInfo = regions.getRegionByRegionid(userInfo.getRegionId());

        model.setRegionInfo(regionInfo);

        return View(model);


    }

    /**
     * 修改用户信息
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "user/edit", method = RequestMethod.POST)
    public ModelAndView editPost(UserEditModel model) {

        lock.lock();
        try {
            ValidationResult result = ValidateModel.validateEntity(model);

            if (result.isNotErrors()) {


                PartUserInfo userInfo = users.getPartUserByUid(model.getUid());

                if (userInfo == null || userInfo.getUid().isEmpty()) {
                    return PromptView("用户信息异常");
                }


                userInfo.setUserName(model.getUserName());


                userInfo.setEmail(model.getEmail());

                userInfo.setMobile(model.getMobile());

                if (StringHelper.isNullOrWhiteSpace(model.getPassword())) {

                    String password = users.createUserPassword(model.getPassword(), userInfo.getSalt());

                    userInfo.setPassword(password);

                }


                userInfo.setUserRid(model.getUserRid());
                userInfo.setNickName(model.getNickName());
                userInfo.setRealName(model.getRealName());
                userInfo.setRegionId(model.getRegionId());
                userInfo.setAddress(model.getAddress());

                PartUserInfo info = users.updateUser(userInfo);

                if (info == null || info.getUid().isEmpty()) {
                    return PromptView("用户信息修改失败");
                }

                return PromptView("用户信息修改成功");

            }

            return PromptView("edit?uid=" + model.getUid(), "用户数据校验失败," + result.toString());

        } catch (Exception ex) {

            logs.write(ex, "用户信息发生故障");
        } finally {
            lock.unlock();
        }


        return PromptView("用户信息修改失败");
    }


    /**
     * 用户详情
     *
     * @param uid 用户uid
     * @return
     */
    @RequestMapping(value = "user/detail")
    public ModelAndView detail(@RequestParam(defaultValue = "") String uid) {
        UserDetailModel model = null;

        if (uid == null) {
            return PromptView("获取uid信息异常");
        }

        PartUserInfo userInfo = users.getUserByUid(uid);

        if (userInfo == null) {
            return PromptView("找不到用户信息");
        }

        model = new UserDetailModel(userInfo);

        return View(model);
    }
}
