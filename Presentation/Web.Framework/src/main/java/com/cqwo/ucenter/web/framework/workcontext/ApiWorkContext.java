/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.web.framework.workcontext;


import com.cqwo.ucenter.core.domain.app.AppInfo;
import lombok.*;

/**
 * Created by cqnews on 2017/4/14.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ApiWorkContext extends BaseWorkContext {


    /**
     * APPId
     */
    private String appId = "";

    /**
     * app信息
     */
    private AppInfo appInfo = new AppInfo();

    /**
     * api账号
     */
    private String apiKey = "1311535288";

    /**
     * 密钥
     */
    private String apiSecret = "7uvF4ZfA7B2JWm0CN8Dm6F7HMIMaYszr";

    /**
     * 用户token 计算出来
     */
    private String accessToken = "";

    /**
     * 接收token
     */
    private String token = "";//Token

    /**
     * 用户openid
     */
    private String openId = "";

    /**
     * 用户sessionId
     */
    private String sessionId = "";


}
