/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.web.framework.controller;

import com.cqwo.ucenter.core.config.info.WechatConfigInfo;
import com.cqwo.ucenter.core.domain.app.AppInfo;
import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import com.cqwo.ucenter.core.domain.users.UserToken;
import com.cqwo.ucenter.core.exception.CWMAPIException;
import com.cqwo.ucenter.core.helper.StringHelper;
import com.cqwo.ucenter.core.helper.WebHelper;
import com.cqwo.ucenter.services.Apps;
import com.cqwo.ucenter.web.framework.workcontext.ApiWorkContext;
import com.google.common.base.Strings;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping(value = "api")
public class BaseApiController extends BaseController {

    protected ApiWorkContext workContext;

//	@Autowired
//	protected WechatUtils wechatUtils;

    protected String accessToken;

    protected WechatConfigInfo wechatConfigInfo;


    public BaseApiController() {

    }

    @Override
    ApiWorkContext getWorkContext() {
        return workContext;
    }

    @ModelAttribute
    public void setInitialize(HttpServletResponse response) throws CWMAPIException {

        this.response = response;
        this.workContext = new ApiWorkContext();
        this.session = request.getSession();

//		this.accessToken = wechatUtils.getAccessToken();
//		this.wechatConfigInfo = cwmConfig.getIconfigstrategy().getWechatConfig();

        /*
         * 获取当前URL
         */
        this.workContext.setUrl(WebHelper.getUrl(request));

        /*
         * 获取当前controller
         */
        this.workContext.setController(this.getClass().getName());

        /*
         * 获取当前action 暂时不能实现
         */
        this.workContext.setAction(WebHelper.getRawUrl(request));

        /*
         * 获取sessionid
         */
        this.workContext.setSid(session.getId());

        /*
         * 判断是否为ajax
         */
        if (WebHelper.isAjax(request)) {
            this.workContext.setHttpAjax(true);
        }

        /*
         * 获取IP
         */
        this.workContext.setIP(WebHelper.getIP(request));

        this.workContext.setUrlReferrer(WebHelper.getUrlReferrer(request));

        this.workContext.setSid(session.getId());


        this.workContext.setImageCDN("/static/admin/images");
        this.workContext.setCssCDN("/static/admin/css");
        this.workContext.setScriptCDN("/static/admin/scripts");
        this.workContext.setFontCDN("/static/admin/fonts");


        try {

            this.workContext.setAppId(WebHelper.getAppIdHeader(request));

            String appId = WebHelper.getAppIdHeader(request);

            if (StringHelper.isNullOrWhiteSpace(appId)) {
                throw new CWMAPIException("APPId异常");
            }


            this.workContext.setApiKey(WebHelper.getApiKeyHeader(request));
            this.workContext.setApiSecret(WebHelper.getApiSecretHeader(request));


            AppInfo appInfo = apps.getAppByApiKeyAndApiSecret(workContext.getApiKey(), workContext.getApiSecret());

            if (appInfo == null || StringHelper.isNullOrWhiteSpace(appInfo.getAppId())) {
                throw new CWMAPIException("APi异常");
            }

            this.workContext.setAppId(appId);
            this.workContext.setAppInfo(appInfo);

        } catch (CWMAPIException ex) {
            throw ex;
        } catch (Exception ex) {

            logs.write(ex, "用户权限处理2");
            throw new CWMAPIException("用户权限异常");
        }

        try {

            String token = cwmUtils.getApiTokenHeader();

            UserToken userToken = users.decryptUserToken(token);

            if (userToken != null) {


                PartUserInfo userInfo = users.getPartUserByUid(userToken.getUid());

                if (userInfo == null || Strings.isNullOrEmpty(userInfo.getUid())) {
                    this.workContext.setPartUserInfo(users.getGuestPartUserInfo());

                } else {
                    this.workContext.setUid(userInfo.getUid());
                    this.workContext.setPartUserInfo(userInfo);
                    this.workContext.setOpenId("");
                    this.workContext.setNickName(userInfo.getNickName());
                    this.workContext.setUserRankInfo(userRanks.getUserRankByCredits(userInfo.getPayCredits()));

                    if (this.workContext.getUserRankInfo() != null) {


                        this.workContext.setUserRid(this.workContext.getUserRankInfo().getUserRid());
                        if (!this.workContext.getUserRankInfo().getUserRid().equals(userInfo.getUserRid())) {

                            userInfo.setUserRid(this.workContext.getUserRankInfo().getUserRid());

                            users.updateUserRankByUid(userInfo.getUid(), this.workContext.getUserRid());
                        }


                    }
                }
            }


        } catch (Exception ex) {
            throw new CWMAPIException("用户处理异常");
            //ex.printStackTrace();
        }

        if (this.workContext.getPartUserInfo() == null || Strings.isNullOrEmpty(this.workContext.getPartUserInfo().getUid())) {

            this.workContext.setPartUserInfo(users.getGuestPartUserInfo());

        }


        //Log.e("URL",request.getRequestURI());
    }


    @ModelAttribute
    public void inspectInitialize() {

        System.out.println("inspectInitialize被执行了");
    }

    @Override
    public void sendLoginRedirect() {
        sendRedirect("/api/error/403");
    }
}
