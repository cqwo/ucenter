//package com.cqwo.ucenter.web.framework.model;
//
//public enum LoginType {
//
//    /**
//     * 用户登录
//     */
//    AdminLogin(0, "AdminLogin"),
//    /**
//     * Token登录
//     */
//    TokenLogin(1, "TokenLogin"),
//
//    /**
//     * ApiLogin
//     */
//    ApiLogin(1, "ApiLogin");
//
//    private Integer index;
//
//    private String name;
//
//    LoginType(Integer index, String name) {
//        this.index = index;
//        this.name = name;
//    }
//
//    public Integer getIndex() {
//        return index;
//    }
//
//    public void setIndex(Integer index) {
//        this.index = index;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//}
