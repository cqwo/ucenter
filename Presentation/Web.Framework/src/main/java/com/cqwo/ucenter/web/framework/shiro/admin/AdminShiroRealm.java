package com.cqwo.ucenter.web.framework.shiro.admin;

import com.cqwo.ucenter.core.domain.authors.AuthUserInfo;
import com.cqwo.ucenter.core.domain.authors.AuthorActionInfo;
import com.cqwo.ucenter.core.domain.authors.AuthorRoleInfo;
import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import com.cqwo.ucenter.core.domain.users.UserRankInfo;
import com.cqwo.ucenter.core.enums.authors.LoginType;
import com.cqwo.ucenter.core.helper.DateHelper;
import com.cqwo.ucenter.services.*;
import com.cqwo.ucenter.web.framework.model.UserTokenPasswordToken;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

/**
 * @author cqnews
 */
public class AdminShiroRealm extends AuthorizingRealm {

    private Logger logger = LoggerFactory.getLogger(AdminShiroRealm.class);

    @Autowired
    private Users users;

    @Autowired
    private Authors authors;

    @Autowired
    private LoginFailLogs loginFailLogs;

    @Autowired
    private UserRanks userRanks;

    @Autowired
    private AppConfig appConfig;

    @Override
    public String getName() {
        return LoginType.AdminLogin.getName();
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        logger.info("doGetAuthorizationInfo+" + principals.toString());


        SimpleAuthorizationInfo auth = new SimpleAuthorizationInfo();


        if (!(principals.getPrimaryPrincipal() instanceof AuthUserInfo)) {
            return auth;
        }

        AuthUserInfo authUserInfo = (AuthUserInfo) principals.getPrimaryPrincipal();

        if (authUserInfo == null) {
            return auth;
        }

        PartUserInfo userInfo = authUserInfo.getUserInfo();

        if (userInfo == null) {
            return auth;
        }

        String uid = userInfo.getUid();

        //String shiroLoginUser = (String) principals.fromRealm(getName()).iterator().next();


        System.out.println("principals.getRealmNames():" + getName());
        System.out.println("account:" + userInfo.toString());


        List<AuthorRoleInfo> authorRoleList = authors.getUserAuthorRoleList(uid);

//        //赋予管理员角色
//        for (AuthorRoleInfo roleInfo : authorRoleList) {
//            auth.addRole(roleInfo.getCode());
//        }

        if (Arrays.asList(appConfig.getAdmin()).contains(userInfo.getUserName())) {
            auth.addRole("admin");
        }

        List<AuthorActionInfo> authorActionList = authors.getUserAuthorActionList(uid);

        for (AuthorActionInfo actionInfo : authorActionList) {
            System.out.println(actionInfo.toString());
        auth.addStringPermission(actionInfo.getAction());
    }

        // System.out.println(auth.getStringPermissions().toString());

        return auth;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

        System.out.println("我应在江湖悠悠,饮一壶浊酒...");
        System.out.println(authenticationToken.toString());

        if (!(authenticationToken instanceof UserTokenPasswordToken)) {
            throw new UnknownAccountException("未找到用户信息");
        }

        UserTokenPasswordToken token = (UserTokenPasswordToken) authenticationToken;

        PartUserInfo userInfo = null;

        if (!token.getLoginType().equals(LoginType.AdminLogin)) {
            throw new DisabledAccountException("禁止的登录方式");
        }


        String account = token.getUsername();


        userInfo = users.getPartUserByAccount(account);


        //检测模型是否存在
        if (userInfo == null || userInfo.getUid().isEmpty()) {
            throw new UnknownAccountException("未找到用户信息");
        }


        String sourcePassword = new String(token.getPassword());


        String password = users.createUserPassword(sourcePassword, userInfo.getSalt());

        System.out.println("sourcePassword" + sourcePassword);
        logger.error("password:" + password);
        logger.error("userInfo.getPassword():" + userInfo.getPassword());

        //检测密码是否正确
        if (!userInfo.getPassword().equals(password)) {
            loginFailLogs.addLoginFailTimes("127.0.0.1", DateHelper.getTimestamp());//增加登陆失败次数 要完善

            throw new IncorrectCredentialsException("用户账号名或密码错误..");

        }


        if (userInfo.getLiftBanTime() >= DateHelper.getUnixTimeStamp())//达到解禁时间
        {
            throw new LockedAccountException("您的账号当前被锁定,不能访问");
        }

        UserRankInfo userRankInfo = userRanks.getUserRankByCredits(userInfo.getPayCredits());

        if (userRankInfo != null && !userRankInfo.getUserRid().equals(userInfo.getUserRid())) {
            users.updateUserRankByUid(userInfo.getUid(), userRankInfo.getUserRid());
            userInfo.setUserRid(userRankInfo.getUserRid());
        }


        loginFailLogs.deleteLoginFailLogByIP("127.0.0.1");


        Session session = SecurityUtils.getSubject().getSession();



        AuthUserInfo authUserInfo = new AuthUserInfo(LoginType.AdminLogin, token.getUsername(), userInfo.getUid(), userInfo);


        session.setAttribute("userinfo", userInfo);
        session.setAttribute("authUserInfo", userInfo);

        System.out.println("处理数据,...sawq");


        return new SimpleAuthenticationInfo(authUserInfo, token.getPassword(), getName());
    }
}
