package com.cqwo.ucenter.web.framework.shiro;


import com.cqwo.ucenter.services.Apps;
import com.cqwo.ucenter.web.framework.filter.ApiAccessControlFilter;
import com.cqwo.ucenter.web.framework.shiro.admin.AdminShiroRealm;
import com.cqwo.ucenter.web.framework.shiro.api.ApiShiroRealm;
import com.cqwo.ucenter.web.framework.shiro.token.TokenShiroRealm;
import com.google.common.collect.Maps;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authc.pam.AtLeastOneSuccessfulStrategy;
import org.apache.shiro.authc.pam.ModularRealmAuthenticator;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.mgt.SessionsSecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class ShiroConfig {

    @Autowired
    private Apps apps;

    private static final Logger logger = LoggerFactory.getLogger(ShiroConfig.class);


    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {
        System.out.println("ShiroConfiguration.shirFilter()");
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();

        // 必须设置 SecurityManager
        shiroFilterFactoryBean.setSecurityManager(securityManager);

        // 如果不设置默认会自动寻找Web工程根目录下的"/login.jsp"页面
        shiroFilterFactoryBean.setLoginUrl("/login");
        // 登录成功后要跳转的链接
        shiroFilterFactoryBean.setSuccessUrl("/index");
        //未授权界面;
        shiroFilterFactoryBean.setUnauthorizedUrl("/403");


        //自定义拦截器
        Map<String, Filter> filtersMap = Maps.newLinkedHashMap();
        filtersMap.put("apiAccessControlFilter", new ApiAccessControlFilter());
        shiroFilterFactoryBean.setFilters(filtersMap);

        //拦截器.
        Map<String, String> filterChainDefinitionMap = Maps.newLinkedHashMap();

        filterChainDefinitionMap.put("/api/author/**", "anon");

        //配置退出过滤器,其中的具体的退出代码Shiro已经替我们实现了
        filterChainDefinitionMap.put("/logout", "logout");
        filterChainDefinitionMap.put("/test", "anon");
        filterChainDefinitionMap.put("/test2", "anon");

        filterChainDefinitionMap.put("/oauth/**", "anon");

        filterChainDefinitionMap.put("/api/error/*", "anon");
        filterChainDefinitionMap.put("/api/index", "anon");
        filterChainDefinitionMap.put("/api/login", "anon");
        filterChainDefinitionMap.put("/api/account/login", "anon");
        filterChainDefinitionMap.put("/api/register", "anon");
        filterChainDefinitionMap.put("/api/account/register", "anon");

        filterChainDefinitionMap.put("/api/**", "apiAccessControlFilter");
//        filterChainDefinitionMap.put("/api/**", "authc");


//        // logged in users with the 'admin' role
//       filterChainDefinitionMap.put("/admin/**", "authc, roles[admin]"); //, roles[admin]

        // logged in users with the 'document:read' permission
        filterChainDefinitionMap.put("/docs/**", "authc, perms[document:read]");

        filterChainDefinitionMap.put("/static/**", "anon"); //匿名访问静态资源

        filterChainDefinitionMap.put("/hello", "anon"); //匿名访问静态资源

        filterChainDefinitionMap.put("/components/**", "anon"); //匿名访问静态资源


        filterChainDefinitionMap.put("/tool/**", "anon"); //匿名访问静态资源


        // all other paths require a logged in user
        filterChainDefinitionMap.put("/**", "authc");//authc


        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return shiroFilterFactoryBean;
    }


//    @Bean
//    public Realm realm() {
//
//        UserRealm userRealm = new UserRealm();
//        userRealm.setCachingEnabled(true);
//
//        return userRealm;
//
//    }

    /**
     * 系统自带的Realm管理，主要针对多realm
     */
    @Bean
    public ModularRealmAuthenticator modularRealmAuthenticator() {
        //自己重写的ModularRealmAuthenticator
        UserModularRealmAuthenticator modularRealmAuthenticator = new UserModularRealmAuthenticator();
        modularRealmAuthenticator.setAuthenticationStrategy(new AtLeastOneSuccessfulStrategy());
        return modularRealmAuthenticator;
    }

    @Bean
    protected CacheManager cacheManager() {
        return new MemoryConstrainedCacheManager();
    }

    @ModelAttribute(name = "subject")
    public Subject subject() {
        return SecurityUtils.getSubject();
    }

    /**
     * Shiro生命周期处理器 * @return
     */
    @Bean
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    /**
     * 开启Shiro的注解(如@RequiresRoles,@RequiresPermissions),需借助SpringAOP扫描使用Shiro注解的类,并在必要时进行安全逻辑验证 * 配置以下两个bean(DefaultAdvisorAutoProxyCreator(可选)和AuthorizationAttributeSourceAdvisor)即可实现此功能 * @return
     */
    @Bean
    @DependsOn({"lifecycleBeanPostProcessor"})
    public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        advisorAutoProxyCreator.setProxyTargetClass(true);
        return advisorAutoProxyCreator;
    }

    @Bean
    protected SessionsSecurityManager securityManager() {

        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();

        securityManager.setAuthenticator(modularRealmAuthenticator());
        List<Realm> realms = new ArrayList<>();
        realms.add(apiShiroRealm());
        realms.add(adminShiroRealm());
        realms.add(tokenShiroRealm());

        securityManager.setRealms(realms);

        return securityManager;
    }

    @Bean
    public Realm adminShiroRealm() {
        //adminShiroRealm.setCredentialsMatcher(hashedCredentialsMatcher());//设置解密规则
        return new AdminShiroRealm();
    }

    @Bean
    public Realm tokenShiroRealm() {
        //tokenShiroRealm.setCredentialsMatcher(hashedCredentialsMatcher());//设置解密规则
        return new TokenShiroRealm();
    }

    @Bean
    public Realm apiShiroRealm() {
        //apiShiroRealm.setCredentialsMatcher(hashedCredentialsMatcher());//设置解密规则
        return new ApiShiroRealm();
    }

    //因为我们的密码是加过密的，所以，如果要Shiro验证用户身份的话，需要告诉它我们用的是md5加密的，并且是加密了两次。同时我们在自己的Realm中也通过SimpleAuthenticationInfo返回了加密时使用的盐。这样Shiro就能顺利的解密密码并验证用户名和密码是否正确了。
    @Bean
    public HashedCredentialsMatcher hashedCredentialsMatcher() {
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        hashedCredentialsMatcher.setHashAlgorithmName("md5");//散列算法:这里使用MD5算法;
        hashedCredentialsMatcher.setHashIterations(2);//散列的次数，比如散列两次，相当于 md5(md5(""));
        return hashedCredentialsMatcher;
    }

//    /**
//     * 开启shiro aop注解支持.
//     * 使用代理方式;所以需要开启代码支持;
//     * 开启 权限注解
//     * Controller才能使用@RequiresPermissions
//     *
//     * @param securityManager
//     * @return
//     */
//    @Bean
//    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
//        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
//        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
//        return authorizationAttributeSourceAdvisor;
//    }

}