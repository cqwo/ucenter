package com.cqwo.ucenter.web.framework.filter;

import com.cqwo.ucenter.core.enums.authors.LoginType;
import com.cqwo.ucenter.core.errors.CWMConstants;
import com.cqwo.ucenter.core.helper.StringHelper;
import com.cqwo.ucenter.core.helper.WebHelper;
import com.cqwo.ucenter.core.message.MessageInfo;
import com.cqwo.ucenter.web.framework.model.UserTokenPasswordToken;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Created by pangkunkun on 2017/11/18.
 */
public class ApiAccessControlFilter extends FormAuthenticationFilter {


    private static final Logger log = LoggerFactory.getLogger(ApiAccessControlFilter.class);


    @Override
    public String getLoginUrl() {
        return "/api/error/403";
    }

//    /**
//     * 表示是否允许访问；mappedValue就是[urls]配置中拦截器参数部分，如果允许访问返回true，否则false；
//     * (感觉这里应该是对白名单（不需要登录的接口）放行的)
//     * 如果isAccessAllowed返回true则onAccessDenied方法不会继续执行
//     * 这里可以用来判断一些不被通过的链接（个人备注）
//     * * 表示是否允许访问 ，如果允许访问返回true，否则false；
//     *
//     * @param request
//     * @param response
//     * @param mappedValue就是   表示写在拦截器中括号里面的字符串 mappedValue 就是 [urls] 配置中拦截器参数部分
//     * @return
//     */
//    @Override
//    public boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue就是) {
//        Subject subject = getSubject(request, response);
//        String url = getPathWithinApplication(request);
//
//        HttpServletRequest hsr = (HttpServletRequest) request;
//
//        log.info("当前用户正在访问的 url => " + hsr.getHeader("user-agent"));
//
//        log.info("当前用户正在访问的 url => " + url);
//        System.out.println("当前用户正在访问的 url => " + url);
//        log.info("subject.isPermitted(url);" + subject.isPermitted(url));
//        System.out.println("subject.isPermitted(url);" + subject.isPermitted(url));
//        return false;
//    }

    /**
     * 表示当访问拒绝时是否已经处理了；如果返回true表示需要继续处理；如果返回false表示该拦截器实例已经处理了，将直接返回即可。
     * onAccessDenied是否执行取决于isAccessAllowed的值，如果返回true则onAccessDenied不会执行；如果返回false，执行onAccessDenied
     * 如果onAccessDenied也返回false，则直接返回，不会进入请求的方法（只有isAccessAllowed和onAccessDenied的情况下）
     */
    @Override
    public boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {

        if (request == null || response == null) {
            return false;
        }

        System.out.println("onAccessDenied");


        //WebHelper.getApiKeyHeader(request,"uid");

        HttpServletRequest hsr = (HttpServletRequest) request;

        String apiKey = WebHelper.getApiKeyHeader(hsr);
        String apiSecret = WebHelper.getApiSecretHeader(hsr);
        String accessToken = WebHelper.getApiTokenHeader(hsr);

        ///apps.GET_APP_BY_API_KEY_AND_API_SECRET(apiKey, apiSecret);

        System.out.println("accessToken:" + accessToken);


//        String loginType = request.getParameter("loginType");

        System.out.println("apiKey:" + apiKey);
        System.out.println("apiSecret:" + apiSecret);


//
//        String apiSecret = cwmUtils.getApiSecretHeader();

        //TODO 通过其它参数验证signature的正确性
//        String digestValue = MD5Util.md5(signature);

//        MyUsernamePasswordToken token = new MyUsernamePasswordToken(username, loginType, digestValue);

//        System.out.println("token.getLoginType():" + token.getLoginType());


//        MyUsernamePasswordToken token=new MyUsernamePasswordToken(username, signature);
//        Subject subject = SecurityUtils.getSubject();
//        try {
//            subject.login(token);
//        } catch (Exception e) {
//            log.info("登陆失败");
//            log.info(e.getMessage());
//            onLoginFail(response);
//            return false;
//        }
//        log.info("登陆成功");


        UserTokenPasswordToken token = null;

        if (StringHelper.isNullOrWhiteSpace(accessToken)) {
            token = new UserTokenPasswordToken(LoginType.ApiLogin, apiKey, apiSecret);
        } else {
            token = new UserTokenPasswordToken(LoginType.TokenLogin, accessToken);
        }


        //UsernamePasswordToken token = new UsernamePasswordToken("yjwwtu", "123456");

        Subject subject = SecurityUtils.getSubject();

        try {
            subject.login(token);
        } catch (UnknownAccountException ex) { //未找到用户信息

            System.out.println("未找到用户信息" + ex.getMessage());

            return false;

        } catch (IncorrectCredentialsException ex) { //账号密码错误

            System.out.println("账号密码错误" + ex.getMessage());
            return false;

        } catch (LockedAccountException ex) { //账户锁定


            System.out.println("账号被锁定" + ex.getMessage());
            return false;

        } catch (ExcessiveAttemptsException ex) { //登录次数超出限制

            System.out.println("登录次数超出限制" + ex.getMessage());
            return false;

        } catch (AuthenticationException ex) { //用户异常

            ex.printStackTrace();
            System.out.println("用户异常" + ex.getMessage());
            return false;
            //unexpected errors?
        } catch (Exception ex) {
            System.out.println("登陆失败");
            System.out.println(ex.getMessage());
            onLoginFail(response);
            return false;
        }

        System.out.println("登录成功了的哟");
        return true;
    }

    /**
     * 登录失败
     */
    private void onLoginFail(ServletResponse response) throws IOException {
        log.info("设置返回");

        HttpServletResponse httpResponse = (HttpServletResponse) response;

        httpResponse.setStatus(HttpServletResponse.SC_OK);
        httpResponse.setHeader("Access-Control-Allow-Origin", "*");
        httpResponse.setHeader("Cache-Control", "no-cache");
        httpResponse.setCharacterEncoding("utf8");
        httpResponse.getWriter().write(MessageInfo.of(CWMConstants.AUTHOR_FAILED, "no author2").toJson());

//        httpResponse.getWriter().write("login error");
    }

    /**
     * TODO 跨域请求
     */
    private void dealCrossDomain() {

    }
}
