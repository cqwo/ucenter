package com.cqwo.ucenter.web.framework.model;

import com.cqwo.ucenter.core.enums.authors.LoginType;
import com.cqwo.wechat.open.web.domain.WxOpenOAuth2AccessToken;
import lombok.*;
import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * 用户token
 *
 * @author cqnews
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserTokenPasswordToken extends UsernamePasswordToken {


    private static final long serialVersionUID = -7508311662351630955L;

    /**
     * 认证类型
     */
    private LoginType loginType = LoginType.AdminLogin;

    /**
     * 用户token
     */
    private String token = "";

    public UserTokenPasswordToken(String username, String password) {
        super(username, password);
    }

    public UserTokenPasswordToken(LoginType loginType, String token) {
        super("", "");
        this.loginType = loginType;
        this.token = token;
    }



    public UserTokenPasswordToken(LoginType loginType, String username, String password) {
        super(username, password);
        this.loginType = loginType;
    }


}
