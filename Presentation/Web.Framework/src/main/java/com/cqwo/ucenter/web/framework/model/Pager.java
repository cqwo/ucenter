/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.web.framework.model;

/**
 * Created by cqnews on 2017/12/14.
 */
public class Pager {


    /**
     * 是否显示汇总
     */
    boolean showSummary = true;

    /**
     * 是否显示页项
     */
    boolean showItems = true;

    /**
     * 项数量
     */
    int itemCount = 7;

    /**
     * 是否显示首页
     */
    boolean showFirst = true;

    /**
     * 是否显示上一页
     */
    boolean showPre = true;

    /**
     * 是否显示下一页
     */
    private boolean showNext = true;

    /**
     * 是否显示末页
     */
    boolean showLast = true;

    /**
     * 是否显示每页数
     */
    private boolean showPageSize = true;

    /**
     * 是否显示页数输入框
     */
    boolean showGoPage = true;

    public boolean isShowSummary() {
        return showSummary;
    }

    public void setShowSummary(boolean showSummary) {
        this.showSummary = showSummary;
    }

    public boolean isShowItems() {
        return showItems;
    }

    public void setShowItems(boolean showItems) {
        this.showItems = showItems;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public boolean isShowFirst() {
        return showFirst;
    }

    public void setShowFirst(boolean showFirst) {
        this.showFirst = showFirst;
    }

    public boolean isShowPre() {
        return showPre;
    }

    public void setShowPre(boolean showPre) {
        this.showPre = showPre;
    }

    public boolean isShowNext() {
        return showNext;
    }

    public void setShowNext(boolean showNext) {
        this.showNext = showNext;
    }

    public boolean isShowLast() {
        return showLast;
    }

    public void setShowLast(boolean showLast) {
        this.showLast = showLast;
    }

    public boolean isShowPageSize() {
        return showPageSize;
    }

    public void setShowPageSize(boolean showPageSize) {
        this.showPageSize = showPageSize;
    }

    public boolean isShowGoPage() {
        return showGoPage;
    }

    public void setShowGoPage(boolean showGoPage) {
        this.showGoPage = showGoPage;
    }


}
