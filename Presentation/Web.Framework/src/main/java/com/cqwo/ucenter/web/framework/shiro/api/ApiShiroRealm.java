package com.cqwo.ucenter.web.framework.shiro.api;

import com.cqwo.ucenter.core.domain.app.AppInfo;
import com.cqwo.ucenter.core.domain.authors.AuthUserInfo;
import com.cqwo.ucenter.core.enums.authors.LoginType;
import com.cqwo.ucenter.core.helper.StringHelper;
import com.cqwo.ucenter.services.Apps;
import com.cqwo.ucenter.web.framework.model.UserTokenPasswordToken;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author cqnews
 */
public class ApiShiroRealm extends AuthorizingRealm {

    @Autowired
    private Apps apps;

    @Override
    public String getName() {
        return LoginType.ApiLogin.getName();
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        SimpleAuthorizationInfo auth = new SimpleAuthorizationInfo();

        if (!(principals.getPrimaryPrincipal() instanceof AuthUserInfo)) {

            return auth;

        }

        AuthUserInfo authUserInfo = (AuthUserInfo) principals.getPrimaryPrincipal();

        if (authUserInfo == null) {
            return auth;
        }


        AppInfo appInfo = authUserInfo.getAppInfo();


        //String shiroLoginUser = (String) principals.fromRealm(getName()).iterator().next();

        if (appInfo == null || StringHelper.isNullOrWhiteSpace(appInfo.getAppId())) {
            return auth;
        }


        System.out.println("principals.getRealmNames():" + getName());
        System.out.println("account:" + appInfo.toString());


        auth.addRole("api");


        return auth;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

        //System.out.println("我应在江湖悠悠,饮一壶浊酒...");
        //System.out.println(authenticationToken.toString());

        if (!(authenticationToken instanceof UserTokenPasswordToken)) {
            throw new UnknownAccountException("未找到用户信息");
        }

        UserTokenPasswordToken token = (UserTokenPasswordToken) authenticationToken;

        if (!token.getLoginType().equals(LoginType.ApiLogin)) {
            throw new DisabledAccountException("禁止的登录方式");
        }

        AppInfo appInfo = null;

        String password = new String(token.getPassword());


        appInfo = apps.getAppByApiKeyAndApiSecret(token.getUsername(), password);

        if (appInfo == null || StringHelper.isNullOrWhiteSpace(appInfo.getAppId())) {
            throw new UnknownAccountException("未找到应用信息");
        }


        Session session = SecurityUtils.getSubject().getSession();

        AuthUserInfo authUserInfo = new AuthUserInfo(LoginType.AdminLogin, token.getUsername(), appInfo.getAppId(), appInfo);

        session.setAttribute("appInfo", appInfo);

        System.out.println("处理数据,...sawq");


        return new SimpleAuthenticationInfo(authUserInfo, token.getPassword(), getName());
    }
}
