//package com.cqwo.ucenter.web.framework.shiro;
//
//import com.cqwo.ucenter.core.domain.authors.AuthorActionInfo;
//import com.cqwo.ucenter.core.domain.authors.AuthorRoleInfo;
//import com.cqwo.ucenter.core.domain.users.PartUserInfo;
//import com.cqwo.ucenter.core.domain.users.UserRankInfo;
//import com.cqwo.ucenter.core.domain.users.UserToken;
//import com.cqwo.ucenter.core.helper.DateHelper;
//import com.cqwo.ucenter.core.helper.TypeHelper;
//import com.cqwo.ucenter.core.log.Logs;
//import com.cqwo.ucenter.services.Authors;
//import com.cqwo.ucenter.services.LoginFailLogs;
//import com.cqwo.ucenter.services.UserRanks;
//import com.cqwo.ucenter.services.Users;
//import com.cqwo.ucenter.web.framework.model.LoginType;
//import com.cqwo.ucenter.web.framework.model.UserTokenPasswordToken;
//import org.apache.shiro.SecurityUtils;
//import org.apache.shiro.authc.*;
//import org.apache.shiro.authz.AuthorizationInfo;
//import org.apache.shiro.authz.SimpleAuthorizationInfo;
//import org.apache.shiro.realm.AuthorizingRealm;
//import org.apache.shiro.session.Session;
//import org.apache.shiro.subject.PrincipalCollection;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import java.util.List;
//
///**
// * Created by cdyoue on 2016/10/21.
// */
//
//
//
//public class UserRealm extends AuthorizingRealm {
//    private Logger logger = LoggerFactory.getLogger(this.getClass());
//
//    @Autowired
//    private Users users;
//
//
//    @Autowired
//    private Logs logs;
//
//
//    @Autowired
//    private LoginFailLogs loginFailLogs;
//
//    @Autowired
//    private UserRanks userRanks;
//
//    @Autowired
//    private Authors authors;
//
//
//    @Override
//    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
//        logger.info("doGetAuthorizationInfo+" + principals.toString());
//
//
//        String uid = principals.getPrimaryPrincipal().toString();
//
//        //String shiroLoginUser = (String) principals.fromRealm(getName()).iterator().next();
//
//
//        System.out.println("principals.getRealmNames():" + getName());
//        System.out.println("account:" + uid);
//
//
//        PartUserInfo userInfo = users.getPartUserByUid(uid);
//
////        //检测账号类型
////        if (ValidateHelper.isValidEmail(account)) {
////            userInfo = users.getPartUserByEmail(account);
////            logs.write("邮箱登录");
////        } else if (ValidateHelper.isValidMobile(account)) {
////            userInfo = users.getPartUserByMobile(account);
////            logs.write("手机登录");
////        } else {
////            userInfo = users.getPartUserByUserName(account);
////            logs.write("账号登录");
////        }
//
//
//        SimpleAuthorizationInfo auth = new SimpleAuthorizationInfo();
//
//        if (userInfo == null) {
//            return auth;
//        }
//
//        List<AuthorRoleInfo> authorRoleList = authors.getUserAuthorRoleList(uid);
//
//        //赋予管理员角色
//        for (AuthorRoleInfo roleInfo : authorRoleList) {
//            auth.addRole(roleInfo.getCode());
//        }
//
//        List<AuthorActionInfo> authorActionList = authors.getUserAuthorActionList(uid);
//
//        for (AuthorActionInfo actionInfo : authorActionList) {
//            System.out.println(actionInfo.toString());
//            auth.addStringPermission(actionInfo.getAction());
//        }
//
//        // System.out.println(auth.getStringPermissions().toString());
//
//        return auth;
//
//
//    }
//
//    @Override
//    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
//        //logger.info("doGetAuthenticationInfo +" + authenticationToken.toString());
//
//
//        System.out.println("我应在江湖悠悠,饮一壶浊酒...");
//        System.out.println(authenticationToken.toString());
//
//        if (!(authenticationToken instanceof UserTokenPasswordToken)) {
//            throw new UnknownAccountException("未找到用户信息");
//        }
//
//        UserTokenPasswordToken token = (UserTokenPasswordToken) authenticationToken;
//
//
//        PartUserInfo userInfo = null;
//
//        if (token.getLoginType().equals(LoginType.TokenLogin)) {
//
//
//            UserToken userToken = users.decryptUserToken(token.getToken());
//
//            if (userToken == null || userToken.getUid().isEmpty()) {
//                throw new UnknownAccountException("未找到用户信息");
//            }
//
//            userInfo = users.getPartUserByUid(userToken.getUid());
//
//            if (userInfo == null || userInfo.getUid().isEmpty()) {
//                throw new UnknownAccountException("未找到用户信息");
//            }
//
//
//        } else {
//
//            String account = token.getUsername();
//
//
//            userInfo = users.getPartUserByAccount(account);
//
//
//            //检测模型是否存在
//            if (userInfo == null || userInfo.getUid().isEmpty()) {
//                throw new UnknownAccountException("未找到用户信息");
//            }
//
//
//            String sourcePassword = new String(token.getPassword());
//
//
//            String password = Users.createUserPassword(sourcePassword, userInfo.getSalt());
//
//            System.out.println("sourcePassword" + sourcePassword);
//            logger.error("password:" + password);
//            logger.error("userInfo.getPassword():" + userInfo.getPassword());
//
//            //检测密码是否正确
//            if (!userInfo.getPassword().equals(password)) {
//                loginFailLogs.addLoginFailTimes("127.0.0.1", DateHelper.getTimestamp());//增加登陆失败次数 要完善
//
//                throw new IncorrectCredentialsException("用户账号名或密码错误..");
//
//
//            }
//
//        }
//
//        if (userInfo.getLiftBanTime() >= DateHelper.getUnixTimeStamp())//达到解禁时间
//        {
//            throw new LockedAccountException("您的账号当前被锁定,不能访问");
//        }
//
//        UserRankInfo userRankInfo = userRanks.getUserRankByCredits(userInfo.getPayCredits());
//
//        if (userRankInfo == null || userRankInfo.getUserRid().equals(userInfo.getUserRid())) {
//        } else {
//            users.updateUserRankByUid(userInfo.getUid(), userRankInfo.getUserRid());
//            userInfo.setUserRid(userRankInfo.getUserRid());
//        }
//
//
//        loginFailLogs.deleteLoginFailLogByIP("127.0.0.1");
//
//
//        Session session = SecurityUtils.getSubject().getSession();
//
//
//        session.setAttribute("userinfo", userInfo);
//
//        System.out.println("处理数据,...sawq");
//
//        return new SimpleAuthenticationInfo(userInfo.getUid(), token.getPassword(), getName());
//    }
//
//    @Override
//    public String getName() {
//        return "UserRealm";
//    }
//}
//
