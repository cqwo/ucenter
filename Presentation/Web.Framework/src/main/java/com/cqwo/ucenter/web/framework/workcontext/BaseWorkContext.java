/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.web.framework.workcontext;

import com.cqwo.ucenter.core.CWMVersion;
import com.cqwo.ucenter.core.config.info.BaseConfigInfo;
import com.cqwo.ucenter.core.domain.authors.AuthorSessionInfo;
import com.cqwo.ucenter.core.domain.base.RegionInfo;
import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import com.cqwo.ucenter.core.domain.users.UserRankInfo;
import lombok.*;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BaseWorkContext {

    /**
     * 项目基本配置
     */
    private BaseConfigInfo baseConfigInfo = new BaseConfigInfo();

    /**
     * 标题
     */
    private String title = "英卡电子";

    /**
     * 关键词
     */
    private String keywords = "英卡电子";

    /**
     * 描述
     */
    private String description = "英卡电子";


    /**
     * 当前请求是否为ajax请求
     */
    private boolean isHttpAjax = false;
    /**
     * 用户ip
     */
    private String IP = "127.0.0.1";//用户ip

    /**
     * 区域信息
     */
    private RegionInfo regionInfo = new RegionInfo();//区域信息

    /**
     * 区域id
     */
    private int regionId = 0;//区域id

    /**
     * 当前url
     */
    private String url = "";//当前url

    /**
     * 上一次访问的url
     */
    private String urlReferrer = "";//上一次访问的url

    /**
     * 用户sid
     */
    private String sid = "";//用户sid

    /**
     * 用户id
     */
    private String uid = "";//用户id

    /**
     * 用户名
     */
    private String userName = "agent";//用户名

    /**
     * 用户邮箱
     */
    private String userEmail = "123@163.com";//用户邮箱

    /**
     * 用户手机号
     */
    private String userMobile = "138";//用户手机号

    /**
     * 用户昵称
     */
    private String nickName = "游客";//用户昵称

    /**
     * 用户头像
     */
    private String avatar = "";//用户头像

    /**
     * 用户密码
     */
    private String password = "";//用户密码

    /**
     * 加密密码
     */
    private String encryptPwd = "";//加密密码

    /**
     * 支付积分名称
     */
    private String payCreditName = "";//支付积分名称

    /**
     * 支付积分数量
     */
    private int payCreditCount = 0;//支付积分数量

    /**
     * 等级积分名称
     */
    private String rankCreditName = "金币";//等级积分名称

    /**
     * 等级积分数量
     */
    private int rankCreditCount = 0;//等级积分数量

    /**
     * 用户信息
     */
    private PartUserInfo partUserInfo;//用户信息

    /**
     * 用户等级id
     */
    private int userRid = -1;//用户等级id

    /**
     * 用户等级信息
     */
    private UserRankInfo userRankInfo;//用户等级信息

    /**
     * 用户等级标题
     */
    private String userRTitle = "游客";//用户等级标题


    /**
     * 用户软件管理员组信息
     */
    private List<AuthorSessionInfo> authorSessionList;//用户软件管理员组信息

    /**
     * 控制器
     */
    private String controller = "home";//控制器

    /**
     * 动作方法
     */
    private String action = "action";//动作方法

    /**
     * 页面标示符
     */
    private String pageKey = "homeaction";//页面标示符

    /**
     * 图片cdn
     */
    private String imageCDN = "/static/admin/images";//图片cdn

    /**
     * csscdn
     */
    private String cssCDN = "/static/admin/css";//csscdn

    /**
     * 脚本cdn
     */
    private String scriptCDN = "/static/admin/scripts";//脚本cdn

    /**
     * 字体cdn
     */
    private String fontCDN = "/static/admin/fonts";//字体cdn

    /**
     * 插件路径
     */
    private String pluginCDN = "/components";//插件路径

    /**
     * 在线总人数
     */
    private int onlineUserCount = 0;//在线总人数

    /**
     * 在线会员数
     */
    private int onlineMemberCount = 0;//在线会员数

    /**
     * 在线游客数
     */
    private int onlineGuestCount = 0;//在线游客数


    /**
     * 页面执行时间
     */
    private double executeTime = 0;//页面执行时间

    /**
     * 执行的sql语句数目
     */
    private int executeCount = 0;//执行的sql语句数目

    /**
     * 执行的sql语句细节
     */
    private String executeDetail = "";//执行的sql语句细节

    /**
     * 软件版本
     */
    private String version = CWMVersion.VERSION;//软件版本

    /**
     * 软件版权
     */
    private String copyright = CWMVersion.COPYRIGHT;//软件版权


}
