package com.cqwo.ucenter.web.framework.controller;


import com.cqwo.ucenter.core.exception.CWMAPIException;
import com.cqwo.ucenter.core.message.MessageInfo;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局自定义异常处理
 *
 * @author cqnews
 */
@ControllerAdvice
public class BaseAdviceController {


    /**
     * 拦截捕捉自定义异常 MyException.class
     *
     * @param ex CWMAPIException
     * @return CWMAPIException
     */
    @ResponseBody
    @ExceptionHandler(value = CWMAPIException.class)
    public String apiErrorHandler(CWMAPIException ex) {

        return MessageInfo.of(0, "异常").toJson();
    }


}
