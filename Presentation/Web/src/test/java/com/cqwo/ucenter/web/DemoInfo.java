package com.cqwo.ucenter.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DemoInfo implements Serializable {


    private static final long serialVersionUID = -3146441510335247120L;
    /**
     * Id
     */
    @Builder.Default
    private Integer id = 0;

    /**
     * name
     */
    @Builder.Default
    private String name = "liu";


    @Builder.Default
    private Integer isShow = 0;
}
