package com.cqwo.ucenter.web.controller;


import com.cqwo.ucenter.core.cache.CWMCache;
import com.cqwo.ucenter.core.data.rdbs.repository.UserRepository;
import com.cqwo.ucenter.services.Authors;
import com.cqwo.ucenter.services.SMSes;
import com.cqwo.ucenter.services.Users;
import com.cqwo.ucenter.web.framework.controller.BaseWebController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

@Controller(value = "HomeController")
public class HomeController extends BaseWebController {

    @Autowired
    SMSes smSes;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CWMCache cwmCache;

    @Autowired
    Users users;

    @Autowired
    Authors authors;

    @RequestMapping(value = {"/", "/hello"})
    public ModelAndView index(Model model) throws IOException {
//
//        List<AuthorRoleInfo> authorRoleList = authors.getUserAuthorRoleList(1);
//
//        System.out.println("authorRoleList:" + authorRoleList.toString());
//
//
//        List<AuthorActionInfo> ationList = authors.getUserAuthorActionList(1);
//
//        System.out.println("ationList:" + ationList.toString());
//
//        Sort sort = new Sort(Sort.Direction.DESC, "createTime");
//
//        Page<PartUserInfo> userInfoPage = users.getPartUserList(10, 1, new Specification<PartUserInfo>() {
//            @Override
//            public Predicate toPredicate(Root<PartUserInfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
//
//                List<Predicate> list = new ArrayList<Predicate>();
//
//                Predicate[] p = new Predicate[list.size()];
//
//                query.where(cb.and(list.toArray(p)));
//
//                model.addAttribute("paymentInfo", 1);
//                model.addAttribute("re", 2);
//
//                return query.getGroupRestriction();
//            }
//        }, sort);
//
//        for (PartUserInfo info : userInfoPage.getContent()) {
//
//            System.out.println("Success List :" + info.toString());
//
//        }


        return PromptView("/login", "正在跳转中...");
    }


}
