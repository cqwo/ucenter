package com.cqwo.ucenter.core.domain.authors;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

//用户-组关联表
@Entity
@Table(name = "w_author_sessions")
@Data
@AllArgsConstructor
@DynamicUpdate
@NoArgsConstructor
@ToString
public class AuthorSessionInfo implements Serializable {


    private static final long serialVersionUID = -2426142183869450857L;
    /**
     * Id
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id = 0;

    /**
     * appid
     */
    @Column(name = "appid", nullable = false)
    private Integer appId = 0;

    /**
     * 用户uid
     **/
    @Column(name = "uid", length = 32, nullable = false)
    private String uid = "";

    /**
     * 用户组id
     **/
    @Column(name = "roleid", nullable = false)
    private Integer roleId = 0;

}