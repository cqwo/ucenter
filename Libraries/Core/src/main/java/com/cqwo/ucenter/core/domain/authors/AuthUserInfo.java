package com.cqwo.ucenter.core.domain.authors;

import com.cqwo.ucenter.core.domain.app.AppInfo;
import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import com.cqwo.ucenter.core.enums.authors.LoginType;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import java.io.Serializable;

/**
 * 用户权限模型
 *
 * @author cqnews
 */
@Getter
@Setter
@AllArgsConstructor
@DynamicUpdate
@NoArgsConstructor
public class AuthUserInfo implements Serializable {


    private static final long serialVersionUID = -8409589443373643119L;
    /**
     * 认证类型
     */
    private LoginType loginType = LoginType.AdminLogin;

    /**
     * 用户token
     */
    private String token = "";

    /**
     * 用户名
     */
    private String username = "";

    /**
     * uid
     */
    private String uid = "";


    /**
     * 用户
     */
    private PartUserInfo userInfo = new PartUserInfo();


    /**
     * appid API时生效
     */
    private String appId = "";

    /**
     * appInfo API时生效
     */
    private AppInfo appInfo = new AppInfo();

    public AuthUserInfo(LoginType loginType, String username, String uid, PartUserInfo userInfo) {
        this.loginType = loginType;
        this.username = username;
        this.uid = uid;
        this.userInfo = userInfo;
    }

    public AuthUserInfo(LoginType loginType, String token, PartUserInfo userInfo) {
        this.loginType = loginType;
        this.token = token;
    }


    public AuthUserInfo(LoginType loginType, String username) {
        this.loginType = loginType;
        this.username = username;
    }

    public AuthUserInfo(LoginType loginType, String username, char[] password) {
        this.loginType = loginType;
        this.username = username;
    }

    public AuthUserInfo(LoginType loginType, String username, String appId, AppInfo appInfo) {
        this.loginType = loginType;
        this.username = username;
        this.appId = appId;
        this.appInfo = appInfo;
    }
}
