/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.core.data.rdbs;

import com.cqwo.ucenter.core.domain.app.AppInfo;
import com.cqwo.ucenter.core.domain.base.BannedIPInfo;
import com.cqwo.ucenter.core.domain.base.RegionInfo;
import com.cqwo.ucenter.core.domain.sms.SMSInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.io.IOException;
import java.util.List;

/**
 * Created by cqnews on 2017/4/19.
 */
public interface IBaseStrategy {

    //region 区域信息

    /**
     * 获得区域信息数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getRegionCount(Specification<RegionInfo> condition) throws IOException;


    /**
     * 创建一条区域信息数据
     *
     * @param regionInfo 区域信息模型
     * @return 返回创建信息
     **/
    RegionInfo createRegion(RegionInfo regionInfo) throws IOException;


    /**
     * 更新一条区域信息数据
     *
     * @param regionInfo 区域信息模型
     **/
    RegionInfo updateRegion(RegionInfo regionInfo) throws IOException;

    /**
     * 删除一条区域信息数据
     *
     * @param regionid 区域信息模型
     **/
    void deleteRegionByRegionid(Integer regionid) throws IOException;

    /**
     * 批量删除一批区域信息数据
     **/
    void deleteRegionByRegionidList(String regionidList) throws IOException;


    /**
     * 获得区域信息一条记录
     *
     * @param regionid regionid
     * @return 返回一条RegionInfo
     **/
    RegionInfo getRegionByRegionid(Integer regionid) throws IOException;

    /**
     * 获得区域信息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回RegionInfo
     **/
    List<RegionInfo> getRegionList(Specification<RegionInfo> condition, Sort sort) throws IOException;


    /**
     * 获得区域信息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回RegionInfo
     **/
    Page<RegionInfo> getRegionList(Integer pageSize, Integer pageNumber, Specification<RegionInfo> condition, Sort sort) throws IOException;


    /**
     * 获取行政省列表
     *
     * @return
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<RegionInfo> getRegionProvinceList() throws IOException;

    /**
     * 获取行政城市(市)列表
     *
     * @return
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<RegionInfo> getRegionCityList(Integer provinceId) throws IOException;


    /**
     * 获取行政县列表
     *
     * @return
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<RegionInfo> getRegionCountyList(Integer cityId) throws IOException;


    //endregion 区域信息结束

    //region 禁用IP

    /**
     * 获得禁用IP数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getBannedIPCount(Specification<BannedIPInfo> condition) throws IOException;


    /**
     * 创建一条禁用IP数据
     *
     * @param bannedipInfo 禁用IP模型
     * @return 返回创建信息
     **/
    BannedIPInfo createBannedIP(BannedIPInfo bannedipInfo) throws IOException;


    /**
     * 更新一条禁用IP数据
     *
     * @param bannedipInfo 禁用IP模型
     **/
    BannedIPInfo updateBannedIP(BannedIPInfo bannedipInfo) throws IOException;

    /**
     * 删除一条禁用IP数据
     *
     * @param id 禁用IP模型
     **/
    void deleteBannedIPById(Integer id) throws IOException;

    /**
     * 批量删除一批禁用IP数据
     **/
    void deleteBannedIPByIdList(String idList) throws IOException;


    /**
     * 获得禁用IP一条记录
     *
     * @param id id
     * @return 返回一条BannedIPInfo
     **/
    BannedIPInfo getBannedIPById(Integer id) throws IOException;

    /**
     * 获得禁用IP数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回BannedIPInfo
     **/
    List<BannedIPInfo> getBannedIPList(Specification<BannedIPInfo> condition, Sort sort) throws IOException;


    /**
     * 获得禁用IP数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回BannedIPInfo
     **/
    Page<BannedIPInfo> getBannedIPList(Integer pageSize, Integer pageNumber, Specification<BannedIPInfo> condition, Sort sort) throws IOException;


    //endregion 禁用IP结束

    //region 短信

    /**
     * 获得短信数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getSMSCount(Specification<SMSInfo> condition) throws IOException;


    /**
     * 创建一条短信数据
     *
     * @param smsInfo 短信模型
     * @return 返回创建信息
     **/
    SMSInfo createSMS(SMSInfo smsInfo) throws IOException;


    /**
     * 更新一条短信数据
     *
     * @param smsInfo 短信模型
     **/
    SMSInfo updateSMS(SMSInfo smsInfo) throws IOException;

    /**
     * 删除一条短信数据
     *
     * @param id 短信模型
     **/
    void deleteSMSById(Integer id) throws IOException;

    /**
     * 批量删除一批短信数据
     **/
    void deleteSMSByIdList(String idList) throws IOException;


    /**
     * 获得短信一条记录
     *
     * @param id id
     * @return 返回一条SMSInfo
     **/
    SMSInfo getSMSById(Integer id) throws IOException;

    /**
     * 获得短信数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回SMSInfo
     **/
    List<SMSInfo> getSMSList(Specification<SMSInfo> condition, Sort sort) throws IOException;


    /**
     * 获得短信数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回SMSInfo
     **/
    Page<SMSInfo> getSMSList(Integer pageSize, Integer pageNumber, Specification<SMSInfo> condition, Sort sort) throws IOException;


    //endregion 短信结束

    //region 应用管理

    /**
     * 获得应用管理数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getAppCount(Specification<AppInfo> condition) throws IOException;


    /**
     * 创建一条应用管理数据
     *
     * @param appInfo 应用管理模型
     * @return 返回创建信息
     **/
    AppInfo createApp(AppInfo appInfo) throws IOException;


    /**
     * 更新一条应用管理数据
     *
     * @param appInfo 应用管理模型
     **/
    AppInfo updateApp(AppInfo appInfo) throws IOException;

    /**
     * 删除一条应用管理数据
     *
     * @param appId 应用管理模型
     **/
    void deleteAppByAppId(Integer appId) throws IOException;

    /**
     * 批量删除一批应用管理数据
     **/
    void deleteAppByAppIdList(String appIdList) throws IOException;


    /**
     * 获得应用管理一条记录
     *
     * @param appId appid
     * @return 返回一条ApplicationInfo
     **/
    AppInfo getAppById(Integer appId) throws IOException;

    /**
     * 获取一条应用管理数据
     *
     * @param appId 应用管理模型
     **/
     AppInfo getAppByAppId(String appId) throws IOException;

    /**
     * 通过密钥和吸获取APP信息
     *
     * @param apiKey    key
     * @param apiSecret 密钥
     * @return
     */
    AppInfo getAppByApiKeyAndApiSecret(String apiKey, String apiSecret) throws IOException;

    /**
     * 获得应用管理数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回ApplicationInfo
     **/
    List<AppInfo> getAppList(Specification<AppInfo> condition, Sort sort) throws IOException;


    /**
     * 获得应用管理数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回ApplicationInfo
     **/
    Page<AppInfo> getAppList(Integer pageSize, Integer pageNumber, Specification<AppInfo> condition, Sort sort) throws IOException;


    //endregion 应用管理结束

}
