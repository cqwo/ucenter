package com.cqwo.ucenter.core.data.rdbs.repository;

import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserRepository extends BaseRepository<PartUserInfo, Integer> {


    /**
     * 判断邮箱是否存在
     *
     * @param mobile   手机
     * @param email    邮箱
     * @param userName 用户名
     * @return
     */
    boolean existsByMobileOrEmailOrUserName(String mobile, String email, String userName);

    /**
     * 判断邮箱是否存在
     *
     * @param email 邮箱
     * @return
     */
    boolean existsByEmail(String email);


    /**
     * 判断用户名是否已经存在
     *
     * @param userName 用户名
     * @return
     */
    boolean existsByUserName(String userName);


    /**
     * 判断用户手机是否存在
     *
     * @param mobile
     * @return
     */
    boolean existsByMobile(String mobile);

    /**
     * 通过手机号码查找用户信息
     *
     * @param mobile
     * @return
     */
    PartUserInfo findFirstByMobile(String mobile);


    /**
     * 通过用户名找用户信息
     *
     * @param userName
     * @return
     */
    PartUserInfo findFirstByUserName(String userName);


    /**
     * 通过邮箱查找用户信息
     *
     * @param email
     * @return
     */
    PartUserInfo findFirstByEmail(String email);


    /**
     * 通过token获取用户信息
     *
     * @param unionId
     * @return
     */
    @Transactional
    @Query("SELECT user FROM PartUserInfo user where exists (SELECT 1 FROM OauthInfo oauth where oauth.uid = user.uid and oauth.server = ?1 and oauth.unionId = ?2)")
    List<PartUserInfo> findByUnionId(String server, String unionId);


    /**
     * 通过token获取用户信息
     *
     * @param server 服务
     * @param openId openid
     * @param oauthAppId  开放appid
     */
    @Transactional
    @Query("select user from PartUserInfo user where exists (select 1 from OauthInfo oauth where oauth.uid = user.uid and oauth.server = ?1 and oauth.openId = ?2 and oauth.oauthAppId = ?3)")
    List<PartUserInfo> findByOpenIdAndOAuthAppId(String server, String openId, String oauthAppId);

    /**
     * 判断验证码是否存在
     *
     * @param invitCode
     * @return
     */
    boolean existsByInvitCode(String invitCode);

    /**
     * 更新用户分组
     *
     * @param uid
     * @param userRid
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    @Query("update PartUserInfo info set info.userRid = ?2 where info.uid = ?1")
    @Modifying
    @Transactional
    int updateUserRankByUid(String uid, Integer userRid);

    /**
     * 查询用户头像
     *
     * @param uid
     * @return
     */
    @Query("select info.avatar  from PartUserInfo info where uid = ?1")
    String getUserAvatar(String uid);


    /**
     * 更新用户手机号码
     *
     * @param uid
     * @param mobile
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    @Modifying
    @Query("update PartUserInfo info set info.mobile = ?2,info.verifyMobile=1 where info.uid=?1")
    @Transactional
    int updateUserMobileByUid(String uid, String mobile);


    /**
     * 更新用户余额
     *
     * @param uid   用户uid
     * @param money 更新的金额
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    @Transactional
    @Modifying
    @Query("update PartUserInfo info set info.money = info.money + ?2 where info.uid = ?1")
    int updateUserMoneyByUid(String uid, double money);

    /**
     * 通过uid获取用户信息
     *
     * @param uid uid
     * @return
     */
    PartUserInfo findFirstByUid(String uid);

    /**
     * 通过用户uid删除
     *
     * @param uid
     */
    void deleteAllByUid(String uid);

}
