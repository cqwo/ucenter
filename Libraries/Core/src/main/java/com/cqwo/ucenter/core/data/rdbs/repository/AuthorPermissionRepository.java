
package com.cqwo.ucenter.core.data.rdbs.repository;

import com.cqwo.ucenter.core.domain.authors.AuthorPermissionInfo;

public interface AuthorPermissionRepository extends BaseRepository<AuthorPermissionInfo, Integer> {
}