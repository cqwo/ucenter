package com.cqwo.ucenter.core.data.rdbs.repository;

import com.cqwo.ucenter.core.domain.base.RegionInfo;

import java.util.List;

public interface RegionRepository extends BaseRepository<RegionInfo, Integer> {

    int countByRegionId(int regionId);

    /**
     * 通过上级id获取列表
     *
     * @param parentId
     * @return
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<RegionInfo> findByParentIdAndLayer(Integer parentId, Integer layer);
}
