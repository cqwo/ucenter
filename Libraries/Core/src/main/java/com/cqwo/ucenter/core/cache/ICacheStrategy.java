package com.cqwo.ucenter.core.cache;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by cqnews on 2017/4/10.
 */
public interface ICacheStrategy {

    /**
     * 测试打印
     */
    String print();


    Object getValue(String key);


    <T> T getValue(String key, Class<T> clz);

    /**
     * 设置
     *
     * @param key
     * @param object
     */
    void setValue(String key, Object object);

    /**
     * 设置
     *
     * @param key
     * @param object
     */
    void setValue(String key, Object object, int timeout, TimeUnit timeUnit);


    <T> List<T> getListValue(String key, Class<T> clz);

    /**
     * 设置列表
     *
     * @param key
     * @param list
     */
    void setListValue(String key, List<?> list);

    /**
     * 设置列表
     *
     * @param key
     * @param list
     */
    void setListValue(String key, List<?> list, int timeout, TimeUnit timeUnit);

}
