package com.cqwo.ucenter.core.data.rdbs.repository;

import com.cqwo.ucenter.core.domain.sms.SMSInfo;

public interface SMSRepository extends BaseRepository<SMSInfo, Integer> {
}
