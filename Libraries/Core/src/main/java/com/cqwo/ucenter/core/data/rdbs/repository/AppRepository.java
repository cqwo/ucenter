
package com.cqwo.ucenter.core.data.rdbs.repository;

import com.cqwo.ucenter.core.domain.app.AppInfo;

public interface AppRepository extends BaseRepository<AppInfo, Integer> {

    /**
     * 通过密钥和吸获取APP信息
     *
     * @param apiKey    key
     * @param apiSecret 密钥
     * @return
     */
    AppInfo findFirstByApiKeyAndApiSecret(String apiKey, String apiSecret);

    /**
     * 获取一条应用管理数据
     *
     * @param appId 应用管理模型
     **/
    AppInfo findFirstByAppId(String appId);
}