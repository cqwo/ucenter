package com.cqwo.ucenter.core.data.rdbs.repository;

import com.cqwo.ucenter.core.domain.base.BannedIPInfo;

public interface BannedIPRepository extends BaseRepository<BannedIPInfo, Integer> {
}
