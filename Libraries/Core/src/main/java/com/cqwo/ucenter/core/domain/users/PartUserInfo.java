/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.core.domain.users;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;


/**
 * 用户
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@DynamicUpdate
@Table(name = "w_users")
public class PartUserInfo implements Serializable {


    private static final long serialVersionUID = -812194532736924613L;
    /**
     * 用户Uid
     **/
    @Id
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @GeneratedValue(generator = "system-uuid")
    @Column(name = "uid", length = 32)
    private String uid = "";


    /**
     * 用户名
     **/
    @Column(name = "username", nullable = false, length = 20)
    private String userName = "";
    /**
     * 邮箱
     **/
    @Column(name = "email", nullable = false, length = 30)
    private String email = "";
    /**
     * 手机
     **/
    @Column(name = "mobile", nullable = false, length = 12)
    private String mobile = "";
    /**
     * 密码
     **/
    @Column(name = "password", nullable = false, length = 32)
    private String password = "";
    /**
     * 用户等级
     **/
    @Column(name = "userrid", nullable = false)
    private Integer userRid = 0;

//    /**
//     * 管理组
//     **/
//    @Column(name = "admingid", nullable = false)
//    private Integer adminGid = 0;
    /**
     * 昵称
     **/
    @Column(name = "nickname", nullable = false, length = 30)
    private String nickName = "";

    /**
     * 推荐人
     */
    @Column(name = "parentid", nullable = false)
    private Integer parentId = 0;


    /**
     * 用户性别(0代表未知，1代表男，2代表女)
     **/
    @Column(name = "gender", nullable = false)
    private Integer gender = 0;
    /**
     * 生日
     **/
    @Column(name = "birthday", nullable = false)
    private Integer birthday = 0;

    /**
     * 真实姓名
     **/
    @Column(name = "realname", nullable = false, length = 10)
    private String realName = "";
    /**
     * 身份证号
     **/
    @Column(name = "idcard", nullable = false, length = 30)
    private String idCard = "";
    /**
     * 头像
     **/
    @Column(name = "avatar", nullable = false, length = 255)
    private String avatar = "";
    /**
     * 所在区域
     **/
    @Column(name = "regionid", nullable = false)
    private Integer regionId = 0;
    /**
     * 地址
     **/
    @Column(name = "address", nullable = false, length = 255)
    private String address = "";
    /**
     * 经度
     **/
    @Column(name = "longitude", nullable = false)
    private double longitude = 0.00;
    /**
     * 纬度
     **/
    @Column(name = "latitude", nullable = false)
    private double latitude = 0.00;
    /**
     * 可支付积分
     **/
    @Column(name = "paycredits", nullable = false)
    private Integer payCredits = 0;
    /**
     * 积分等级
     **/
    @Column(name = "rankcredits", nullable = false)
    private Integer rankCredits = 0;
    /**
     * 用户余额
     **/
    @Column(name = "money", nullable = false)
    private double money = 0.00;
    /**
     * 消费等级
     **/
    @Column(name = "rankmoney", nullable = false)
    private double rankMoney = 0.00;

    /**
     * 邀请码
     */
    @Column(name = "invitcode", nullable = false, length = 30)
    private String invitCode = "";

    /**
     * 是否验证邮箱
     **/
    @Column(name = "verifyemail", nullable = false)
    private Integer verifyEmail = 0;
    /**
     * 是否验证手机
     **/
    @Column(name = "verifymobile", nullable = false)
    private Integer verifyMobile = 0;
    /**
     * 解禁时间
     **/
    @Column(name = "liftbantime", nullable = false)
    private Integer liftBanTime = 0;
    /**
     * 盐值
     **/
    @Column(name = "salt", nullable = false, length = 18)
    private String salt = "";

    /**
     * 创建时间
     */
    @Column(name = "createtime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer createTime = 0;


}