package com.cqwo.ucenter.core.exception.users;

import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import com.cqwo.ucenter.core.exception.CWMException;

/**
 * @Author: cqnews
 * @Date: 2018-09-29 15:12
 * @Version 1.0
 */
public class RepeatException extends CWMException {

    private static final long serialVersionUID = 4518525997134877503L;

    private PartUserInfo userInfo;

    public RepeatException() {
        super();
    }

    public RepeatException(String message) {
        super(message);
    }

    public RepeatException(String message, Throwable cause) {
        super(message, cause);
    }

    public RepeatException(String message, PartUserInfo userInfo) {
        super(message);
        this.userInfo = userInfo;
    }

    public RepeatException(Throwable cause) {
        super(cause);
    }

    public RepeatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public PartUserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(PartUserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
