package com.cqwo.ucenter.core.domain.authors;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

//用户分组表

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@ToString
@Entity
@Table(name = "w_author_roles")
public class AuthorRoleInfo implements Serializable {


    private static final long serialVersionUID = -7169807466024299037L;
    /**
     * deviceId
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer roleId = 0;
    
    /**
     * appid
     */
    @Column(name = "appid", nullable = false)
    private Integer appId = 0;

    /**
     * 分组名称
     **/
    @Column(name = "title", nullable = false)
    private String title = "";

    /**
     * 分组标识
     */
    @Column(name = "code", nullable = false, length = 20)
    private String code = "";

    /**
     * 分组描述
     **/
    @Column(name = "description", nullable = false)
    private String description = "";

}