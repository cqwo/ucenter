package com.cqwo.ucenter.core.data.rdbs.repository;

import com.cqwo.ucenter.core.domain.users.UserDetailInfo;

public interface UserDetailRepository extends BaseRepository<UserDetailInfo, Integer> {

}