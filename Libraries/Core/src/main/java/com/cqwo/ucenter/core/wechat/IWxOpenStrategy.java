package com.cqwo.ucenter.core.wechat;


import com.cqwo.wechat.open.web.domain.WxOpenOAuth2AccessToken;
import com.cqwo.wechat.open.web.domain.WxOpenOAuth2User;
import com.cqwo.wechat.open.web.exption.WxOpenErrorException;

public interface IWxOpenStrategy {


    /**
     * 获取appId
     *
     * @return oauthAppId
     * @throws WxOpenErrorException w异常
     */
    String getAppId() throws WxOpenErrorException;

    /**
     * 获取accesstoken
     *
     * @return accesstoken
     */
    String getAccessToken() throws WxOpenErrorException;

    /**
     * 获取accesstoken
     *
     * @param appId oauthAppId
     * @return accesstoken
     */
    String getAccessToken(String appId) throws WxOpenErrorException;

    /**
     * 获取用户code
     * https://open.weixin.qq.com/connect/qrconnect?appid=%s&redirect_uri=%s&response_type=code&scope=%s&state=%s#wechat_redirect
     */
    String oauth2GetCodeUrl(String redirectUrl) throws WxOpenErrorException;

    /**
     * 获取用户code
     *
     * @param appId       oauthAppId
     * @param redirectUrl 返回地址
     *                    https://open.weixin.qq.com/connect/qrconnect?appid=%s&redirect_uri=%s&response_type=code&scope=%s&state=%s#wechat_redirect
     */
    String oauth2GetCodeUrl(String appId, String redirectUrl) throws WxOpenErrorException;

    /**
     * 获取授权accesstoken
     * <p>
     * https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code
     *
     * @param code code
     */
    WxOpenOAuth2AccessToken oauth2GetAccessToken(String code) throws WxOpenErrorException;

    /**
     * 获取授权accesstoken
     * <p>
     * https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code
     *
     * @param code  code
     * @param appId oauthAppId
     */
    WxOpenOAuth2AccessToken oauth2GetAccessToken(String appId, String code) throws WxOpenErrorException;

    /**
     * 刷新或续期access_token使用
     * https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=%s&grant_type=refresh_token&refresh_token=%s
     *
     * @param refreshToken refresh_token
     */
    WxOpenOAuth2AccessToken oauth2RefreshAccessToken(String refreshToken) throws WxOpenErrorException;

    /**
     * 刷新或续期access_token使用
     * https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=%s&grant_type=refresh_token&refresh_token=%s
     *
     * @param appId        oauthAppId
     * @param refreshToken refresh_token
     */
    WxOpenOAuth2AccessToken oauth2RefreshAccessToken(String appId, String refreshToken) throws WxOpenErrorException;


    /**
     * 检验授权凭证（access_token）是否有效
     * https://api.weixin.qq.com/sns/auth?access_token=%s&openid=%s
     *
     * @param appId accessToken
     */
    boolean oauth2CheckAccessToken(String appId, String openId) throws WxOpenErrorException;


    /**
     * 获取用户信息
     *
     * @param openId openid
     */
    WxOpenOAuth2User oauth2GetUserinfo(String openId) throws WxOpenErrorException;


    /**
     * 获取用户信息
     *
     * @param appId  oauthAppId
     * @param openId openid
     */
    WxOpenOAuth2User oauth2GetUserinfo(String appId, String openId) throws WxOpenErrorException;

}
