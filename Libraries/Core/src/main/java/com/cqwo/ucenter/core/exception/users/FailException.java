package com.cqwo.ucenter.core.exception.users;

import com.cqwo.ucenter.core.exception.CWMException;

/**
 * @Author: cqnews
 * @Date: 2018-09-29 15:18
 * @Version 1.0
 */
public class FailException extends CWMException {


    private static final long serialVersionUID = 5811216172961680307L;

    public FailException() {
        super();
    }

    public FailException(String message) {
        super(message);
    }

    public FailException(String message, Throwable cause) {
        super(message, cause);
    }

    public FailException(Throwable cause) {
        super(cause);
    }

    public FailException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
