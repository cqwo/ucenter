
package com.cqwo.ucenter.core.data.rdbs.repository;

import com.cqwo.ucenter.core.domain.users.CreditLogInfo;

public interface CreditLogRepository extends BaseRepository<CreditLogInfo, Integer> {
}