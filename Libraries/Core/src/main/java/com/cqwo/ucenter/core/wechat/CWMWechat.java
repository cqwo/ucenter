package com.cqwo.ucenter.core.wechat;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component(value = "CWMWechat")
@Getter
@Setter
public class CWMWechat {

    @Autowired(required = false)
    private IWxOpenStrategy iWxOpenStrategy;

}
