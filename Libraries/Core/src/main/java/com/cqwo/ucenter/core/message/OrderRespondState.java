package com.cqwo.ucenter.core.message;

/**
 * Created by mayn on 2018/1/26.
 */

public enum OrderRespondState {

    /**
     * 消息状态
     */
    Complete(1, "成功"), Error(0, "失败");


    private int index;

    private String name;

    OrderRespondState(int index, String name) {
        this.index = index;
        this.name = name;
    }


    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取枚举名称
     *
     * @param index
     * @return
     */
    public String getName(int index) {
        for (OrderRespondState orderRespondState : OrderRespondState.values()) {

            if (index == orderRespondState.getIndex()) {
                return orderRespondState.getName();
            }
        }

        return null;
    }

    /**
     * 获取枚举名称
     *
     * @param index
     * @return
     */
    public OrderRespondState getOrderRespondState(int index) {
        for (OrderRespondState orderRespondState : OrderRespondState.values()) {

            if (index == orderRespondState.getIndex()) {
                return orderRespondState;
            }
        }

        return null;
    }


    @Override
    public String toString() {
        return "OrderRespondState{" +
                "index=" + index +
                ", name='" + name + '\'' +
                '}';
    }
}
