
package com.cqwo.ucenter.core.data.rdbs.repository;

import com.cqwo.ucenter.core.domain.users.UserTokenInfo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface UserTokenRepository extends BaseRepository<UserTokenInfo, Integer> {


    /**
     * 删除过期的token
     *
     * @param timestamp
     */
    @Modifying
    @Transactional
    @Query("delete from UserTokenInfo where limitTime <= ?1")
    void deleteLitmitToken(Integer timestamp);

    /**
     * 通过用户获取token
     *
     * @param token token
     * @return
     */
    UserTokenInfo findFirstByUidAndToken(String uid, String token);

    /**
     * 通过用户uid获取token
     *
     * @param uid   uid
     * @param appId oauthAppId
     * @return UserTokenInfo
     */
    @Query(value = "select info from UserTokenInfo info where info.id = (select max(info2.id) from UserTokenInfo info2 where info2.uid = :uid and info2.appId = :appId)")
    UserTokenInfo findFirstByUidAndAppId(@Param(value = "uid") String uid,
                                         @Param(value = "appId") String appId);
    /**
     * 通过用户uid获取token
     *
     * @param uid   uid
     * @param appId oauthAppId
     * @return UserTokenInfo
     */
    @Query(value = "select info from UserTokenInfo info where info.id = (select max(info2.id) from UserTokenInfo info2 where info2.appId = :uid and info2.appId = :appId and info2.refreshToken = :refreshToken)")
    UserTokenInfo findFirstByUidAndRefreshToken(@Param(value = "uid") String uid,
                                                @Param(value = "appId") String appId,
                                                @Param(value = "refreshToken") String refreshToken);
}