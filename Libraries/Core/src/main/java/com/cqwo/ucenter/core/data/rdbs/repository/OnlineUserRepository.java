package com.cqwo.ucenter.core.data.rdbs.repository;

import com.cqwo.ucenter.core.domain.users.OnlineUserInfo;

public interface OnlineUserRepository extends BaseRepository<OnlineUserInfo, Integer> {
}
