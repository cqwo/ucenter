/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.core.config;

import com.cqwo.ucenter.core.config.info.*;


/**
 * 配置策略接口
 * Created by cqnews on 2017/3/24.
 */
public interface IConfigStrategy {


    /**
     * 保存关系型数据库
     *
     * @param configInfo 关系型数据库
     * @return
     */
    boolean saveRDBSConfig(RDBSConfigInfo configInfo);

    /**
     * 获得关系数据库配置
     *
     * @return 关系数据库配置
     */
    RDBSConfigInfo getRDBSConfig();

    /**
     * 保存项目基本配置
     *
     * @param configInfo 项目基本配置
     * @return 是否保存成功
     */
    boolean saveBaseConfig(BaseConfigInfo configInfo);

    /**
     * 获得项目基本配置
     *
     * @return 项目基本配置
     */
    BaseConfigInfo getBaseConfig();

    /**
     * 保存邮件配置
     *
     * @param configInfo 邮件配置信息
     * @return 是否保存成功
     */
    boolean saveEmailConfig(EmailConfigInfo configInfo);

    /**
     * 获得邮件配置
     *
     * @return 邮件配置
     */
    EmailConfigInfo getEmailConfig();

    /**
     * 保存短信配置
     *
     * @param configInfo 短信配置信息
     * @return 是否保存成功
     */
    boolean saveSMSConfig(SMSConfigInfo configInfo);

    /**
     * 获得短信配置
     *
     * @return 短信配置
     */
    SMSConfigInfo getSMSConfig();

    /**
     * 保存微信配置
     *
     * @param configInfo 微信配置信息
     * @return 是否保存成功
     */
    boolean saveWechatConfig(WechatConfigInfo configInfo);

    /**
     * 获得微信配置
     *
     * @return 微信配置
     */
    WechatConfigInfo getWechatConfig();

}
