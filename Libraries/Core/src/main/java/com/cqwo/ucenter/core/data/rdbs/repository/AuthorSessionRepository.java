
package com.cqwo.ucenter.core.data.rdbs.repository;

import com.cqwo.ucenter.core.domain.authors.AuthorSessionInfo;

public interface AuthorSessionRepository extends BaseRepository<AuthorSessionInfo, Integer> {
}