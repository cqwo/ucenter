/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.core.upload;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * Created by cqnews on 2017/12/25.
 */
@SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
public interface IUploadStrategy {


    /**
     * 文件上传
     * @param fload 路径
     * @param multipartFileMap
     * @return
     * @throws Exception
     */
    List<String> fileUpload(String fload, Map<String, MultipartFile> multipartFileMap) throws Exception;

    /**
     * 获取客户端token
     * @return
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    String getUpToken() throws Exception;

    /**
     * 图片上传
     * @param imgName
     * @param b
     * @return
     */
    String imgUpload(String imgName, byte[] b);
}
