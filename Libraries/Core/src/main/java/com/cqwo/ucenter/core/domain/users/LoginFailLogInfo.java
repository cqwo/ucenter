/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.core.domain.users;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 日志
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_user_loginfaillogs")
public class LoginFailLogInfo implements Serializable {

    private static final long serialVersionUID = 5100659918606336335L;
    /**
     * 日志Id
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id = 0;
    /**
     * 登录IP
     **/
    @Column(name = "loginip", nullable = false)
    private long loginIP = 0;
    /**
     * 失败的次数
     **/
    @Column(name = "failtimes", nullable = false)
    private Integer failTimes = 0;
    /**
     * 最后登陆时间
     **/
    @Column(name = "lastlogintime", nullable = false)
    private Integer lastLoginTime = 0;

}