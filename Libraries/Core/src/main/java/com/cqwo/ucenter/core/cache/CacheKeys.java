/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.core.cache;

/**
 * Created by cqnews on 2017/12/14.
 * 缓存键
 */
public class CacheKeys {


    public static final char SPLIT_CHAR = '_';

    /**
     * 获通过IP取地址区域信息缓存键
     */
    public static final String BASE_REGIONS_GETREGIONIDBYIP = "baseregionsgetregionidbyip";

    /**
     * 新闻列表缓存
     */
    public static final String NEWS_CATEGORY_LIST = "newscategorylist";


    /**
     * 插件
     */
    public static final String PLUGIN_LIST = "pluginlist";

    /**
     * 微信token
     */
    public static final String WECHAT_ACCESS_TOKEN = "wechataccesstoken";


    /**
     * 通过用户名称获取用户信息
     */
    public static final String GET_PARTUSER_USERNAME = "partuserbyusername";

    /**
     * 用户分组权限id
     */
    public static final String GET_USER_AUTHOR_ROLE_LIST = "userauthorrolelist";

    /**
     * 角色权限列表
     */
    public static final String GET_ROLE_AUTHOR_ACTION_LIST = "roleauthoractionlist";


    /**
     * 省列表
     */
    public static final String REGION_PROVINCE_LIST = "regionprovincelist";

    /**
     * 城市列表
     */
    public static final String REGION_CITY_LIST = "regioncitylist";
    /**
     * 县级列表
     */
    public static final String REGION_COUNTY_LIST = "regioncountylist";

    public static final String GET_APP_BY_API_KEY_AND_API_SECRET = "getappbyapikeyandapisecret";
}
