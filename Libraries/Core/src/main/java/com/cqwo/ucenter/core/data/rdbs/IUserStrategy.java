/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.core.data.rdbs;

import com.cqwo.ucenter.core.domain.users.*;
import com.cqwo.ucenter.core.exception.token.TokenException;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.io.IOException;
import java.util.List;

/**
 * Created by cqnews on 2017/12/6.
 */
public interface IUserStrategy {

    //region 用户

    /**
     * 获得用户数量
     *
     * @param condition
     * @return 返回数量
     **/
    long getPartUserCount(Specification<PartUserInfo> condition) throws IOException;

    /**
     * 创建一条用户数据
     *
     * @param userInfo 用户模型
     * @return 返回创建信息
     **/
    PartUserInfo createPartUser(PartUserInfo userInfo) throws IOException;


    /**
     * 更新一条用户数据
     *
     * @param userInfo 用户模型
     **/
    PartUserInfo updatePartUser(PartUserInfo userInfo) throws IOException;

    /**
     * 删除一条用户数据
     *
     * @param uid 用户模型
     **/
    void deleteUserByUid(String uid) throws IOException;

    /**
     * 批量删除一批用户数据
     **/
    void deleteUserByUidList(String uidList) throws IOException;


    /**
     * 获得用户一条记录
     *
     * @param uid uid
     * @return 返回一条UserInfo
     **/
    PartUserInfo getPartUserByUid(String uid) throws IOException;

    /**
     * 通过手机号码获取用户
     *
     * @return
     */
    PartUserInfo getPartUserByMobile(String mobile) throws IOException;

    /**
     * 通过用户名获取用户
     *
     * @return
     */
    PartUserInfo getPartUserByUserName(String userName) throws IOException;


    /**
     * 通过OpenId获取用户信息
     *
     * @param server     服务
     * @param openId     openid
     * @param oauthAppId 开放appid
     */
    PartUserInfo getPartUserByOpenIdAndOAuthAppId(String server, String openId, String oauthAppId) throws IOException;

    /**
     * 通过UnionId获取用户信息
     *
     * @param server  服务
     * @param unionId unionId
     */
    PartUserInfo getPartUserByUnionId(String server, String unionId) throws IOException;

    /**
     * 通过邮箱获取用户
     *
     * @return
     */
    PartUserInfo getPartUserByEmail(String email) throws IOException;


    /**
     * 获得用户数据列表
     *
     * @param specification 条件
     * @return 返回UserInfo
     **/
    List<PartUserInfo> getPartUserList(Specification<PartUserInfo> specification, Sort sort) throws IOException;


    /**
     * 获得用户数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @return 返回UserInfo
     **/
    Page<PartUserInfo> getPartUserList(int pageSize, int pageNumber, Specification<PartUserInfo> specification, Sort sort) throws IOException;


    /**
     * 判断邀请码是否存在
     *
     * @return
     */
    boolean isExitsInvitCode(String invitCode) throws IOException;


    /**
     * 更新用户组
     *
     * @param uid     用户uid
     * @param userRid 用户分组
     */
    void updateUserRankByUid(String uid, Integer userRid) throws IOException;

    /**
     * 检测用户账号是否已经存在
     *
     * @param account 用户账号
     * @return
     */
    boolean checkedUserByAccount(String account) throws IOException;

    /**
     * 检测邮箱是否存在
     *
     * @param email 邮箱
     * @return
     * @throws IOException
     */
    boolean checkedUserByEmail(String email) throws IOException;

    /**
     * 检测用户名是否存在
     *
     * @param userName 用户名
     * @return
     * @throws IOException
     */
    boolean checkedUserByUserName(String userName) throws IOException;

    /**
     * 检测用户手机是否存在
     *
     * @param mobile mobile
     * @return
     * @throws IOException
     */
    boolean checkedUserByMobile(String mobile) throws IOException;

    //endregion 用户结束

    //region 用户详情

    /**
     * 获得用户详情数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getUserDetailCount(Specification<UserDetailInfo> condition) throws IOException;


    /**
     * 创建一条用户详情数据
     *
     * @param userdetailInfo 用户详情模型
     * @return 返回创建信息
     **/
    UserDetailInfo createUserDetail(UserDetailInfo userdetailInfo) throws IOException;


    /**
     * 更新一条用户详情数据
     *
     * @param userdetailInfo 用户详情模型
     **/
    UserDetailInfo updateUserDetail(UserDetailInfo userdetailInfo) throws IOException;

    /**
     * 删除一条用户详情数据
     *
     * @param id 用户详情模型
     **/
    void deleteUserDetailById(int id) throws IOException;

    /**
     * 批量删除一批用户详情数据
     **/
    void deleteUserDetailByIdList(String idList) throws IOException;


    /**
     * 获得用户详情一条记录
     *
     * @param id id
     * @return 返回一条UserDetailInfo
     **/
    UserDetailInfo getUserDetailById(int id) throws IOException;

    /**
     * 获得用户详情数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回UserDetailInfo
     **/
    List<UserDetailInfo> getUserDetailList(Specification<UserDetailInfo> condition, Sort sort) throws IOException;


    /**
     * 获得用户详情数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回UserDetailInfo
     **/
    Page<UserDetailInfo> getUserDetailList(Integer pageSize, Integer pageNumber, Specification<UserDetailInfo> condition, Sort sort) throws IOException;


    //endregion 用户详情结束

    //region 用户等级

    /**
     * 获得用户等级数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getUserRankCount(Specification<UserRankInfo> condition) throws IOException;


    /**
     * 创建一条用户等级数据
     *
     * @param userrankInfo 用户等级模型
     * @return 返回创建信息
     **/
    UserRankInfo createUserRank(UserRankInfo userrankInfo) throws IOException;


    /**
     * 更新一条用户等级数据
     *
     * @param userrankInfo 用户等级模型
     **/
    UserRankInfo updateUserRank(UserRankInfo userrankInfo) throws IOException;

    /**
     * 删除一条用户等级数据
     *
     * @param userrid 用户等级模型
     **/
    void deleteUserRankByUserrid(int userrid) throws IOException;

    /**
     * 批量删除一批用户等级数据
     **/
    void deleteUserRankByUserridList(String userridList) throws IOException;


    /**
     * 获得用户等级一条记录
     *
     * @param userrid userrid
     * @return 返回一条UserRankInfo
     **/
    UserRankInfo getUserRankByUserrid(int userrid) throws IOException;

    /**
     * 获得用户等级数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回UserRankInfo
     **/
    List<UserRankInfo> getUserRankList(Specification<UserRankInfo> condition, Sort sort) throws IOException;


    /**
     * 获得用户等级数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回UserRankInfo
     **/
    Page<UserRankInfo> getUserRankList(Integer pageSize, Integer pageNumber, Specification<UserRankInfo> condition, Sort sort) throws IOException;


    /**
     * 获取全部用户组
     *
     * @return
     */
    List<UserRankInfo> getAllUserRankList() throws IOException;
    //endregion 用户等级结束

    //region 第三方登录

    /**
     * 获得第三方登录数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getOauthCount(Specification<OauthInfo> condition) throws IOException;


    /**
     * 创建一条第三方登录数据
     *
     * @param oauthInfo 第三方登录模型
     * @return 返回创建信息
     **/
    OauthInfo createOauth(OauthInfo oauthInfo) throws IOException;


    /**
     * 更新一条第三方登录数据
     *
     * @param oauthInfo 第三方登录模型
     **/
    OauthInfo updateOauth(OauthInfo oauthInfo) throws IOException;

    /**
     * 删除一条第三方登录数据
     *
     * @param id 第三方登录模型
     **/
    void deleteOauthById(int id) throws IOException;

    /**
     * 批量删除一批第三方登录数据
     **/
    void deleteOauthByIdList(String idList) throws IOException;


    /**
     * 获得第三方登录一条记录
     *
     * @param id id
     * @return 返回一条OauthInfo
     **/
    OauthInfo getOauthById(int id) throws IOException;

    /**
     * 获得第三方登录数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回OauthInfo
     **/
    List<OauthInfo> getOauthList(Specification<OauthInfo> condition, Sort sort) throws IOException;


    /**
     * 获得第三方登录数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回OauthInfo
     **/
    Page<OauthInfo> getOauthList(Integer pageSize, Integer pageNumber, Specification<OauthInfo> condition, Sort sort) throws IOException;

    /**
     * 检测OpendId是否重复
     *
     * @param server     服务
     * @param openId     opendId
     * @param oauthAppId
     * @return
     */
    OauthInfo getOauthByOpenId(String server, String openId, String oauthAppId) throws IOException;


    /**
     * @param server
     * @param openId
     * @param unionId
     * @param oauthAppId
     * @return
     * @throws IOException
     */
    OauthInfo getOauthByOpenIdAndUnionId(String server, String openId, String unionId, String oauthAppId) throws IOException;


    /**
     * 判断服务的OpenId是否存在
     *
     * @param server     服务
     * @param openId     openId
     * @param oauthAppId
     * @return
     */
    boolean existsOauthByOAuthAppId(String server, String openId, String oauthAppId) throws IOException;

    /**
     * 查找用户是否存在
     *
     * @param server  服务
     * @param unionId unionId
     * @return
     */
    boolean existsOauthByUnionId(String server, String unionId) throws IOException;

    /**
     * 通过appid和uid查找用户信息
     *
     * @param oauthAppId oauthAppId
     * @param uid        uid
     * @return
     */
    OauthInfo getOauthByOAuthAppIdAndUid(String oauthAppId, String uid) throws IOException;

    //endregion 第三方登录结束

    //region 在线时间统计

    /**
     * 获得在线时间统计数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getOnlineTimeCount(Specification<OnlineTimeInfo> condition) throws IOException;


    /**
     * 创建一条在线时间统计数据
     *
     * @param onlinetimeInfo 在线时间统计模型
     * @return 返回创建信息
     **/
    OnlineTimeInfo createOnlineTime(OnlineTimeInfo onlinetimeInfo) throws IOException;


    /**
     * 更新一条在线时间统计数据
     *
     * @param onlinetimeInfo 在线时间统计模型
     **/
    OnlineTimeInfo updateOnlineTime(OnlineTimeInfo onlinetimeInfo) throws IOException;

    /**
     * 删除一条在线时间统计数据
     *
     * @param id 在线时间统计模型
     **/
    void deleteOnlineTimeById(int id) throws IOException;

    /**
     * 批量删除一批在线时间统计数据
     **/
    void deleteOnlineTimeByIdList(String idList) throws IOException;


    /**
     * 获得在线时间统计一条记录
     *
     * @param id id
     * @return 返回一条OnlineTimeInfo
     **/
    OnlineTimeInfo getOnlineTimeById(int id) throws IOException;

    /**
     * 获得在线时间统计数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回OnlineTimeInfo
     **/
    List<OnlineTimeInfo> getOnlineTimeList(Specification<OnlineTimeInfo> condition, Sort sort) throws IOException;


    /**
     * 获得在线时间统计数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回OnlineTimeInfo
     **/
    Page<OnlineTimeInfo> getOnlineTimeList(Integer pageSize, Integer pageNumber, Specification<OnlineTimeInfo> condition, Sort sort) throws IOException;


    //endregion 在线时间统计结束

    //region 在线用户

    /**
     * 获得在线用户数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getOnlineUserCount(Specification<OnlineUserInfo> condition) throws IOException;


    /**
     * 创建一条在线用户数据
     *
     * @param onlineuserInfo 在线用户模型
     * @return 返回创建信息
     **/
    OnlineUserInfo createOnlineUser(OnlineUserInfo onlineuserInfo) throws IOException;


    /**
     * 更新一条在线用户数据
     *
     * @param onlineuserInfo 在线用户模型
     **/
    OnlineUserInfo updateOnlineUser(OnlineUserInfo onlineuserInfo) throws IOException;

    /**
     * 删除一条在线用户数据
     *
     * @param olid 在线用户模型
     **/
    void deleteOnlineUserByOlid(int olid) throws IOException;

    /**
     * 批量删除一批在线用户数据
     **/
    void deleteOnlineUserByOlidList(String olidList) throws IOException;


    /**
     * 获得在线用户一条记录
     *
     * @param olid olid
     * @return 返回一条OnlineUserInfo
     **/
    OnlineUserInfo getOnlineUserByOlid(int olid) throws IOException;

    /**
     * 获得在线用户数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回OnlineUserInfo
     **/
    List<OnlineUserInfo> getOnlineUserList(Specification<OnlineUserInfo> condition, Sort sort) throws IOException;


    /**
     * 获得在线用户数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回OnlineUserInfo
     **/
    Page<OnlineUserInfo> getOnlineUserList(Integer pageSize, Integer pageNumber, Specification<OnlineUserInfo> condition, Sort sort) throws IOException;


    //endregion 在线用户结束

    //region token管理

    /**
     * 更新用户的token信息
     *
     * @param userTokenInfo 用户模型
     * @return 返回创建信息
     **/
    UserTokenInfo updateUserToken(UserTokenInfo userTokenInfo) throws IOException;


    /**
     * 删除过期token
     */
    void deleteLitmitToken() throws IOException;


    /**
     * 获取token
     *
     * @param uid
     * @param token token
     * @return
     */
    UserTokenInfo findUserToken(String uid, String token) throws IOException;

    /**
     * 获取token
     *
     * @param uid   uid
     * @param appId oauthAppId
     * @return
     */
    UserTokenInfo findLastUserTokenByUid(String uid, String appId) throws IOException;


    /**
     * 获取token
     *
     * @param uid   uid
     * @param appId oauthAppId
     * @return UserTokenInfo
     */
    UserTokenInfo findUserTokenByUidAndRefreshToken(String uid, String appId, String refreshToken) throws TokenException;

    //endregion


}
