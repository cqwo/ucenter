package com.cqwo.ucenter.core.domain.app;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@ToString
@Entity
@Table(name = "w_applications")
public class AppInfo implements Serializable {


    private static final long serialVersionUID = -6442896812698555554L;

    /**
     * 开发者ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id = 0;


    /**
     * AppId,不允许重复
     */
    @Column(name = "appid", unique = true, nullable = false, length = 50)
    @ColumnDefault("''")
    private String appId = "";

    /**
     * 开发者名称
     */
    @Column(name = "appname", unique = true, nullable = false, length = 30)
    @ColumnDefault("''")
    private String appName = "";


    /**
     * 开发者key
     */
    @Column(name = "apikey", nullable = false, length = 30)
    @ColumnDefault("''")
    private String apiKey = "";


    /**
     * 开发者密码
     */
    @Column(name = "apisecret", nullable = false, length = 256)
    @ColumnDefault("''")
    private String apiSecret = "";

    /**
     * ASE私钥
     */
    @Column(name = "aessecret", nullable = false, length = 32)
    @ColumnDefault("''")
    private String aesSecret = "";


    /**
     * 项目描述
     */
    @Column(name = "description", nullable = false, length = 150)
    @ColumnDefault("''")
    private String description = "";

    /**
     * 微信AppId
     */
    @Column(name = "wxappid", nullable = false, length = 50)
    @ColumnDefault("''")
    private String wxAppId = "";

    /**
     * 微信AppId
     */
    @Column(name = "wxsecret", nullable = false, length = 50)
    @ColumnDefault("''")
    private String wxSecret = "";

    /**
     * 微信通信息密钥
     */
    @Column(name = "wxaeskey", nullable = false, length = 50)
    @ColumnDefault("''")
    private String wxAesKey = "";


}



