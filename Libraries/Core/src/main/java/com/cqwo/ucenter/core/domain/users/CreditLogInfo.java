/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.core.domain.users;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

//用户日志
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_user_creditlogs")
public class CreditLogInfo implements Serializable {


    private static final long serialVersionUID = 5540483259055734690L;
    /**
     * 日志Id
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "logid")
    private Integer logId;

    /**
     * Uid
     **/
    @Column(name = "uid", length = 32, nullable = false)
    private String uid = "";

    /**
     * 支付积分
     **/
    @Column(name = "paycredits", nullable = false)
    private Integer payCredits = 0;

    /**
     * 积分等级
     **/
    @Column(name = "rankcredits", nullable = false)
    private Integer rankCredits = 0;

    /**
     * 动作类型
     **/
    @Column(name = "action", nullable = false)
    private Integer action = 0;

    /**
     * 动作代码
     **/
    @Column(name = "actioncode", nullable = false)
    private Integer actionCode = 0;

    /**
     * 动作时间
     **/
    @Column(name = "actiontime", nullable = false)
    private Integer actionTime = 0;

    /**
     * 动作描述
     **/
    @Column(name = "actiondes", nullable = false, length = 150)
    private String actionDes = "";

    /**
     * 操作人
     **/
    @Column(name = "operator", nullable = false)
    private Integer operator = -1;


}