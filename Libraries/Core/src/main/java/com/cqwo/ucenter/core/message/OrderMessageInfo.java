package com.cqwo.ucenter.core.message;

import lombok.*;

import java.math.BigDecimal;

/**
 * Created by mayn on 2018/1/26.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderMessageInfo {


    /**
     * 订单号
     **/
    private int oid = 0;

    /**
     * 订单编号
     */
    private String osn = "";


    /**
     * 用户
     **/
    private String uid = "";

    /**
     * 机器唯一Id
     **/
    private int machId = 0;
    /**
     * 机器唯一编码
     **/
    private String serialNumber = "";

    /**
     * 订单状态
     */
    private int orderState = 1;


    /**
     * 机器支付模式
     **/
    private int payMode;

    /**
     * 订单合计
     **/
    private double recordAmount = 0;

    /**
     * 订单合计
     **/
    private double orderAmount = 0;

    /**
     * 支付金额
     **/
    private double surplusMoney = 0;




}
