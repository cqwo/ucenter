package com.cqwo.ucenter.core.domain.users;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Table(name = "w_users_token")
@Entity
public class UserTokenInfo implements Serializable {


    private static final long serialVersionUID = 7223137506659169031L;

    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id = 0;

    /**
     * 要求唯一
     * uid
     */
    @Column(name = "uid", nullable = false)
    @ColumnDefault(value = "''")
    private String uid = "";

    /**
     * openid
     */
    @Column(name = "salt", nullable = false, length = 10)
    @ColumnDefault(value = "''")
    private String salt = "";

    /**
     * apikey
     */
    @Column(name = "appid", nullable = false, length = 32)
    @ColumnDefault(value = "''")
    private String appId = "";


    /**
     * token
     */
    @Column(name = "token", nullable = false, length = 500)
    @ColumnDefault(value = "''")
    private String token = "";

    /**
     * token
     */
    @Column(name = "refreshtoken", nullable = false, length = 500)
    @ColumnDefault(value = "''")
    private String refreshToken = "";

    /**
     * 过期时间
     */
    @Column(name = "limittime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer limitTime = 0;


    public UserTokenInfo(String uid, String appId) {
        this.uid = uid;
        this.appId = appId;
    }


}
