/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.core.domain.users;

import lombok.*;

/**
 * Created by cqnews on 2017/12/22.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SmipleUserInfo {

    /**
     * 用户姓名
     */
    private String realName="";

    /**
     * 手机号码
     */
    private String mobile="127.0.0.1";

    /**
     * ip
     */
    private String ip="";


}
