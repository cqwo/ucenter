/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.core.domain.users;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 用户等级
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_user_userranks")
public class UserRankInfo implements Serializable {

    private static final long serialVersionUID = -5299034745544946079L;

    /**
     * 用户等级id
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userrid")
    private Integer userRid = 6;


    /**
     * 是否是系统等级
     **/
    @Column(name = "system", nullable = false)
    private Integer system = 1;
    /**
     * 用户等级标题
     **/
    @Column(name = "title", nullable = false, length = 30)
    private String title = "6";
    /**
     * 用户等级头像
     **/
    @Column(name = "avatar", nullable = false, length = 150)
    private String avatar = "";
    /**
     * 用户等级积分上限
     **/
    @Column(name = "creditslower", nullable = false)
    private Integer creditsLower = 0;
    /**
     * 用户等级积分下限
     **/
    @Column(name = "creditsupper", nullable = false)
    private Integer creditsUpper = 0;
    /**
     * 限制天数
     **/
    @Column(name = "limitdays", nullable = false)
    private Integer limitDays = 0;


}