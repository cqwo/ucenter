/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.core.domain.users;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

//在线时间统计

@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_user_onlinetime")
public class OnlineTimeInfo implements Serializable {

    private static final long serialVersionUID = 8712096278634674842L;
    /**
     * 在线时间统计
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    /**
     * Uid
     **/
    @Column(name = "uid")
    private String uid = "";
    /**
     * 总计在线
     **/
    @Column(name = "total")
    private Integer total = 0;
    /**
     * 年
     **/
    @Column(name = "year")
    private Integer year = 0;
    /**
     * 月
     **/
    @Column(name = "month")
    private Integer month = 0;
    /**
     * 周
     **/
    @Column(name = "week")
    private Integer week = 0;
    /**
     * 日
     **/
    @Column(name = "day")
    private Integer day = 0;
    /**
     * 更新时间
     **/
    @Column(name = "updatetime")
    private Integer updateTime = 0;


}