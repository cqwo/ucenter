package com.cqwo.ucenter.core.message;

/**
 * Created by mayn on 2018/1/26.
 */

/**
 * 订单消息回调
 */
public class OrderRespondMessageInfo {

    /**
     * 订单id
     */
    int oid = -1;

    /**
     * 机器id
     */
    int machId = 0;

    /**
     * 订单状态
     */
    int state = 0;

    public OrderRespondMessageInfo() {
    }

    public OrderRespondMessageInfo(int oid, int machId, int state) {
        this.oid = oid;
        this.machId = machId;
        this.state = state;
    }

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public int getMachId() {
        return machId;
    }

    public void setMachId(int machId) {
        this.machId = machId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "OrderRespondMessageInfo{" +
                "oid=" + oid +
                ", machId=" + machId +
                ", state=" + state +
                '}';
    }
}
