/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.core.domain.base;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

//禁用Ip
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@ToString
@Entity
@Table(name = "w_commom_bannedips")
public class BannedIPInfo implements Serializable {

    private static final long serialVersionUID = -7924205615914753847L;

    /**
     * 禁用Id
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id = 0;

    /**
     * IP
     **/
    @Column(name = "ip", nullable = false, length = 30)
    private String ip = "";

    /**
     * 禁止过期时间
     **/
    @Column(name = "liftbantime", nullable = false)
    private Integer liftBanTime = 0;


}