package com.cqwo.ucenter.core.data.rdbs.repository;

import com.cqwo.ucenter.core.domain.users.UserRankInfo;

public interface UserRankRepository extends BaseRepository<UserRankInfo, Integer> {
}

