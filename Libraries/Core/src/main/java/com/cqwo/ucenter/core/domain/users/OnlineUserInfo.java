/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.core.domain.users;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;


/**
 * 在线用户
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_user_onlineusers")
public class OnlineUserInfo implements Serializable {

    private static final long serialVersionUID = 512440548844532098L;
    /**
     * 在线用户编号
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "olid")
    private Integer olId = 0;
    /**
     * 在线用户id
     **/
    @Column(name = "uid", length = 32, nullable = false)
    private String uid = "";
    /**
     * 在线用户sid
     **/
    @Column(name = "sid", nullable = false, length = 30)
    private String sid = "";
    /**
     * 用户昵称
     **/
    @Column(name = "nickname", nullable = false, length = 30)
    private String nickName = "";
    /**
     * 在线用户ip
     **/
    @Column(name = "ip", nullable = false, length = 30)
    private String ip = "";

    /**
     * 注册区域
     **/
    @Column(name = "regionid", nullable = false)
    private Integer regionId = 0;
    /**
     * 更新时间
     **/
    @Column(name = "updatetime", nullable = false)
    private Integer updateTime = 0;

}