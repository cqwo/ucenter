package com.cqwo.ucenter.core.data.rdbs.repository;

import com.cqwo.ucenter.core.domain.users.OauthInfo;

import java.io.IOException;

public interface OauthRepository extends BaseRepository<OauthInfo, Integer> {

    /**
     * 检测openid是否存在
     *
     * @param openId opendId
     * @param server 服务
     * @param oauthAppId  oauthAppId
     * @return
     */
    boolean existsByOpenIdAndServerAndOauthAppId(String openId, String server, String oauthAppId);


    /**
     * 查询openId和服务
     *
     * @param openId openid
     * @param server 服务
     * @param oauthAppId  oauthAppId
     * @return
     */
    OauthInfo findFirstByServerAndOpenIdAndOauthAppId(String server, String openId, String oauthAppId);

    /**
     * @param server  服务
     * @param openId  openid
     * @param unionId unionId
     * @param oauthAppId   oauthAppId
     * @return
     * @throws IOException
     */
    OauthInfo findFirstByServerAndOpenIdAndUnionIdAndOauthAppId(String server, String openId, String unionId, String oauthAppId);

    /**
     * 判断服务的OpenId是否存在
     *
     * @param server 服务
     * @param openId openId
     * @param oauthAppId  oauthAppId
     * @return
     */
    boolean existsByServerAndOpenIdAndOauthAppId(String server, String openId, String oauthAppId);

    /**
     * 判断unionId是否存在
     *
     * @param server  服务
     * @param unionId unionId
     * @return
     */
    boolean existsByServerAndUid(String server, String unionId);

    /**
     * 判断服务的unionId是否存在
     *
     * @param server  服务
     * @param unionId unionId
     * @return
     */
    boolean existsByServerAndUnionId(String server, String unionId);

    /**
     * 通过appid和uid查找用户信息
     *
     * @param oauthAppId oauthAppId
     * @param uid   uid
     * @return
     */
    OauthInfo findFirstByOauthAppIdAndUid(String oauthAppId, String uid);
}
