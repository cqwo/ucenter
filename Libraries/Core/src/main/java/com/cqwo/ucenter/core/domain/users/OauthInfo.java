/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.core.domain.users;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

//开放授权

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_user_oauth")
public class OauthInfo implements Serializable {

    private static final long serialVersionUID = -6406139675956385119L;

    /**
     * 开放授权Id
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id = 0;


    /**
     * Uid
     **/
    @Column(name = "uid", length = 32, nullable = false)
    @ColumnDefault(value = "''")
    private String uid = "";


    /**
     * openid
     **/
    @Column(name = "openid", nullable = false, unique = true, length = 80)
    @ColumnDefault(value = "''")
    private String openId = "";

    /**
     * unionid
     **/
    @Column(name = "unionid", nullable = false, length = 80)
    @ColumnDefault(value = "''")
    private String unionId = "";

    /**
     * 服务商
     **/
    @Column(name = "server", nullable = false, length = 30)
    @ColumnDefault(value = "wechat")
    private String server = "wechat";

    /**
     * appid
     **/
    @Column(name = "oauthappid", nullable = false)
    @ColumnDefault(value = "''")
    private String oauthAppId = "";


    public OauthInfo(String uid, String openId, String unionId, String server, String oauthAppId) {
        this.uid = uid;
        this.openId = openId;
        this.unionId = unionId;
        this.server = server;
        this.oauthAppId = oauthAppId;
    }

    public OauthInfo(int id, String uid, String openId, String unionId, String server, String oauthAppId) {
        this.id = id;
        this.uid = uid;
        this.openId = openId;
        this.unionId = unionId;
        this.server = server;
        this.oauthAppId = oauthAppId;

    }
}