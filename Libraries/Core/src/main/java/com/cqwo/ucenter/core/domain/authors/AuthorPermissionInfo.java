package com.cqwo.ucenter.core.domain.authors;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

//角色许可表

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@ToString
@Entity
@Table(name = "w_author_permissions")
public class AuthorPermissionInfo implements Serializable {


    private static final long serialVersionUID = 3164302230408551310L;

    /**
     * Id
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;


    /**
     * appid
     */
    @Column(name = "appid", nullable = false)
    private Integer appId = 0;


    /**
     * 动作id
     **/
    @Column(name = "aid", nullable = false)
    private Integer aid = 0;

    /**
     * 用户角色id
     **/
    @Column(name = "roleid", nullable = false)
    private Integer roleId = 0;

}