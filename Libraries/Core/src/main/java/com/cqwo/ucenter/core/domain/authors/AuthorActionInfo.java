package com.cqwo.ucenter.core.domain.authors;


import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

//用户动作表

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@ToString
@Entity
@Table(name = "w_author_actions")
public class AuthorActionInfo implements Serializable {


    private static final long serialVersionUID = -2227297665686562449L;

    /**
     * aid
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer aid = 0;

    /**
     * appid
     */
    @Column(name = "appid", nullable = false)
    private Integer appId = 0;

    /**
     * action
     **/
    @Column(name = "action", nullable = false)
    private String action = "";

    /**
     * 排序
     **/
    @Column(name = "displayorder", nullable = false)
    private Integer displayOrder = 50;

    /**
     * 上级id
     **/
    @Column(name = "parentid", nullable = false)
    private Integer parentId = 0;

    /**
     * 标题
     **/
    @Column(name = "title", nullable = false)
    private String title = "";


}