package com.cqwo.ucenter.core.data.rdbs.repository;

import com.cqwo.ucenter.core.domain.users.OnlineTimeInfo;

public interface OnlineTimeRepository extends BaseRepository<OnlineTimeInfo, Integer> {
}
