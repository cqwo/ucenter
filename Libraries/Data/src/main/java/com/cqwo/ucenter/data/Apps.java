package com.cqwo.ucenter.data;

import com.cqwo.ucenter.core.cache.CacheKeys;
import com.cqwo.ucenter.core.domain.app.AppInfo;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by cqnews on 2017/4/11.
 */


//应用管理
@Service(value = "AppDatas")
public class Apps extends DataService {


    //region  应用管理方法

    /**
     * 获得应用管理数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getAppCount(Specification<AppInfo> condition) throws IOException {
        return getCwmData().getIBaseStrategy().getAppCount(condition);
    }

    /**
     * 创建一条应用管理数据
     *
     * @param appInfo 应用管理模型
     * @return 返回创建信息
     **/
    public AppInfo createApp(AppInfo appInfo) throws IOException {
        return getCwmData().getIBaseStrategy().createApp(appInfo);
    }

    /**
     * 更新一条应用管理数据
     *
     * @param appInfo 应用管理模型
     **/
    public AppInfo updateApp(AppInfo appInfo) throws IOException {
        return getCwmData().getIBaseStrategy().updateApp(appInfo);
    }

    /**
     * 删除一条应用管理数据
     *
     * @param appId 应用管理模型
     **/
    public void deleteAppByAppId(Integer appId) throws IOException {
        getCwmData().getIBaseStrategy().deleteAppByAppId(appId);
    }

    /**
     * 批量删除一批应用管理数据
     **/
    public void deleteAppByAppIdList(String appIdList) throws IOException {
        getCwmData().getIBaseStrategy().deleteAppByAppIdList(appIdList);
    }

    /**
     * 获取一条应用管理数据
     *
     * @param id 应用管理模型
     **/
    public AppInfo getAppById(Integer id) throws IOException {
        return getCwmData().getIBaseStrategy().getAppById(id);
    }

    /**
     * 获取一条应用管理数据
     *
     * @param appId 应用管理模型
     **/
    public AppInfo getAppByAppId(String appId) throws IOException {
        return getCwmData().getIBaseStrategy().getAppByAppId(appId);
    }

    /**
     * 通过密钥和吸获取APP信息
     *
     * @param apiKey    key
     * @param apiSecret 密钥
     * @return
     */
    public AppInfo getAppByApiKeyAndApiSecret(String apiKey, String apiSecret) throws IOException {

        String cahceKey = CacheKeys.GET_APP_BY_API_KEY_AND_API_SECRET + CacheKeys.SPLIT_CHAR + apiKey + CacheKeys.SPLIT_CHAR + apiSecret;

        AppInfo appInfo = getCwmCache().getIcachestrategy().getValue(cahceKey, AppInfo.class);

        if (appInfo == null) {

            appInfo = getCwmData().getIBaseStrategy().getAppByApiKeyAndApiSecret(apiKey, apiSecret);
            getCwmCache().getIcachestrategy().setValue(cahceKey, appInfo, 6, TimeUnit.MINUTES);
        }


        return appInfo;
    }


    /**
     * 获得应用管理数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回ApplicationInfo
     **/
    public List<AppInfo> getAppList(Specification<AppInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIBaseStrategy().getAppList(condition, sort);
    }


    /**
     * 获得应用管理数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回ApplicationInfo
     **/
    public Page<AppInfo> getAppList(Integer pageSize, Integer pageNumber, Specification<AppInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIBaseStrategy().getAppList(pageSize, pageNumber, condition, sort);
    }


    //endregion

}
