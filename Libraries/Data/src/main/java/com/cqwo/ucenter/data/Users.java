package com.cqwo.ucenter.data;

import com.cqwo.ucenter.core.cache.CacheKeys;
import com.cqwo.ucenter.core.domain.users.PartUserInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * Created by cqnews on 2017/4/11.
 */


//用户信息
@Service(value = "UsersData")
public class Users extends DataService {


    //region  用户信息方法

    /**
     * 获得用户信息数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getPartUserCount(Specification<PartUserInfo> condition) throws IOException {
        return getCwmData().getIUserStrategy().getPartUserCount(condition);
    }

    /**
     * 创建一条用户信息数据
     *
     * @param userInfo 用户信息模型
     * @return 返回创建信息
     **/
    public PartUserInfo createPartUser(PartUserInfo userInfo) throws IOException {
        return getCwmData().getIUserStrategy().createPartUser(userInfo);
    }

    /**
     * 更新一条用户信息数据
     *
     * @param userInfo 用户信息模型
     **/
    public PartUserInfo updateUser(PartUserInfo userInfo) throws IOException {
        return getCwmData().getIUserStrategy().updatePartUser(userInfo);
    }

    /**
     * 删除一条用户信息数据
     *
     * @param uid 用户信息模型
     **/
    public void deleteUserByUid(String uid) throws IOException {
        getCwmData().getIUserStrategy().deleteUserByUid(uid);
    }

    /**
     * 批量删除一批用户信息数据
     **/
    public void deleteUserByUidList(String uidList) throws IOException {
        getCwmData().getIUserStrategy().deleteUserByUidList(uidList);
    }

    /**
     * 获取一条用户信息数据
     *
     * @param uid 用户信息模型
     **/
    public PartUserInfo getPartUserByUid(String uid) throws IOException {
        return getCwmData().getIUserStrategy().getPartUserByUid(uid);
    }


    /**
     * 获得用户信息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回UserInfo
     **/
    public List<PartUserInfo> getPartUserList(Specification<PartUserInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIUserStrategy().getPartUserList(condition, sort);
    }


    /**
     * 获得用户信息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回UserInfo
     **/
    public Page<PartUserInfo> getPartUserList(Integer pageSize, Integer pageNumber, Specification<PartUserInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIUserStrategy().getPartUserList(pageSize, pageNumber, condition, sort);
    }


    /**
     * 通过用户名获取用户信息
     *
     * @param userName 用户名
     */
    public PartUserInfo getPartUserByUserName(String userName) throws IOException {

        PartUserInfo userInfo = getCwmCache().getIcachestrategy().getValue(CacheKeys.GET_PARTUSER_USERNAME + userName, PartUserInfo.class);


        if (userInfo == null) {

            userInfo = getCwmData().getIUserStrategy().getPartUserByUserName(userName);

            getCwmCache().getIcachestrategy().setValue(CacheKeys.GET_PARTUSER_USERNAME + userName, userInfo);

        }

        return userInfo;
    }

    /**
     * 通过用户名获取用户信息
     *
     * @param email 邮箱
     */
    public PartUserInfo getPartUserByEmail(String email) throws IOException {

        return getCwmData().getIUserStrategy().getPartUserByEmail(email);
    }

    /**
     * 通过用户手机获取用户信息
     *
     * @param mobile 手机号码
     */
    public PartUserInfo getPartUserByMobile(String mobile) throws IOException {
        return getCwmData().getIUserStrategy().getPartUserByMobile(mobile);
    }


    /**
     * 通过OpenId获取用户信息
     * @param server 服务
     * @param openId openid
     * @param oauthAppId  开放appid
     */
    public PartUserInfo getPartUserByOpenIdAndOAuthAppId(String server, String openId, String oauthAppId) throws IOException {
        return getCwmData().getIUserStrategy().getPartUserByOpenIdAndOAuthAppId(server, openId, oauthAppId);
    }


    /**
     * 通过UnionId获取用户信息
     *
     * @param server  服务
     * @param unionId unionId
     */
    public PartUserInfo getPartUserByUnionId(String server, String unionId) throws IOException {
        return getCwmData().getIUserStrategy().getPartUserByUnionId(server, unionId);
    }



    /**
     * 更新用户组
     *
     * @param uid     用户uid
     * @param userRid 用户分组
     */
    public void updateUserRankByUid(String uid, Integer userRid) throws IOException {
        getCwmData().getIUserStrategy().updateUserRankByUid(uid, userRid);
    }

    /**
     * 检测用户账号是否已经存在
     *
     * @param account 用户账号
     */
    public boolean checkedUserByAccount(String account) throws IOException {
        return getCwmData().getIUserStrategy().checkedUserByAccount(account);
    }


    /**
     * 检测用户邮箱是否存在
     *
     * @param email 邮箱地址
     */
    public boolean checkedUserByEmail(String email) throws IOException {
        return getCwmData().getIUserStrategy().checkedUserByEmail(email);
    }


    /**
     * 检测用户用户名是否存在
     *
     * @param userName 用户名
     * @return
     */
    public boolean checkedUserByUserName(String userName) throws IOException {
        return getCwmData().getIUserStrategy().checkedUserByUserName(userName);
    }


    /**
     * 检测用户手机是否存在
     *
     * @param mobile 手机
     * @return
     */
    public boolean checkedUserByMobile(String mobile) throws IOException {
        return getCwmData().getIUserStrategy().checkedUserByMobile(mobile);
    }



    //endregion

}
