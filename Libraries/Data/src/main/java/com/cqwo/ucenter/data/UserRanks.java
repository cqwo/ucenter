package com.cqwo.ucenter.data;

import com.cqwo.ucenter.core.domain.users.UserRankInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * Created by cqnews on 2017/4/11.
 */


//用户等级
@Service(value = "UserRanksData")
public class UserRanks extends DataService {


    //region  用户等级方法

    /**
     * 获得用户等级数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getUserRankCount(Specification<UserRankInfo> condition) throws IOException {
        return getCwmData().getIUserStrategy().getUserRankCount(condition);
    }

    /**
     * 创建一条用户等级数据
     *
     * @param userrankInfo 用户等级模型
     * @return 返回创建信息
     **/
    public UserRankInfo createUserRank(UserRankInfo userrankInfo) throws IOException {
        return getCwmData().getIUserStrategy().createUserRank(userrankInfo);
    }

    /**
     * 更新一条用户等级数据
     *
     * @param userrankInfo 用户等级模型
     **/
    public UserRankInfo updateUserRank(UserRankInfo userrankInfo) throws IOException {
        return getCwmData().getIUserStrategy().updateUserRank(userrankInfo);
    }

    /**
     * 删除一条用户等级数据
     *
     * @param userrid 用户等级模型
     **/
    public void deleteUserRankByUserrid(int userrid) throws IOException {
        getCwmData().getIUserStrategy().deleteUserRankByUserrid(userrid);
    }

    /**
     * 批量删除一批用户等级数据
     **/
    public void deleteUserRankByUserridList(String userridList) throws IOException {
        getCwmData().getIUserStrategy().deleteUserRankByUserridList(userridList);
    }

    /**
     * 获取一条用户等级数据
     *
     * @param userrid 用户等级模型
     **/
    public UserRankInfo getUserRankByUserrid(int userrid) throws IOException {
        return getCwmData().getIUserStrategy().getUserRankByUserrid(userrid);
    }


    /**
     * 获得用户等级数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回UserRankInfo
     **/
    public List<UserRankInfo> getUserRankList(Specification<UserRankInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIUserStrategy().getUserRankList(condition, sort);
    }


    /**
     * 获得用户等级数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回UserRankInfo
     **/
    public Page<UserRankInfo> getUserRankList(Integer pageSize, Integer pageNumber, Specification<UserRankInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIUserStrategy().getUserRankList(pageSize, pageNumber, condition, sort);
    }

    /**
     * 获取全部用户组
     *
     * @return
     */
    public List<UserRankInfo> getAllUserRankList() throws IOException {
        return getCwmData().getIUserStrategy().getAllUserRankList();
    }


    //endregion

}
