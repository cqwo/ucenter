package com.cqwo.ucenter.data;

import com.cqwo.ucenter.core.cache.CWMCache;
import com.cqwo.ucenter.core.config.CWMConfig;
import com.cqwo.ucenter.core.data.CWMData;
import com.cqwo.ucenter.core.sms.CWMSMS;
import org.springframework.beans.factory.annotation.Autowired;



public class DataService {

    @Autowired
    private CWMCache cwmCache;

    @Autowired
    private CWMData cwmData;

    @Autowired
    private CWMSMS cwmSMS;

    @Autowired
    private CWMConfig cwmConfig;


    protected CWMCache getCwmCache() {
        return cwmCache;
    }

    protected void setCwmCache(CWMCache cwmCache) {
        this.cwmCache = cwmCache;
    }

    protected CWMData getCwmData() {
        return cwmData;
    }

    protected void setCwmData(CWMData cwmData) {
        this.cwmData = cwmData;
    }

    protected CWMSMS getCwmSMS() {
        return cwmSMS;
    }

    protected void setCwmSMS(CWMSMS cwmSMS) {
        this.cwmSMS = cwmSMS;
    }

    protected CWMConfig getCwmConfig() {
        return cwmConfig;
    }

    protected void setCwmConfig(CWMConfig cwmConfig) {
        this.cwmConfig = cwmConfig;
    }
}
