package com.cqwo.ucenter.data;

import com.cqwo.ucenter.core.domain.users.UserTokenInfo;
import com.cqwo.ucenter.core.exception.token.TokenException;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @Author: cqnews
 * @Date: 2018-09-29 17:43
 * @Version 1.0
 */
@Component(value = "UserTokenDatas")
public class UserTokens extends DataService {


    //region token管理

    /**
     * 更新用户的token信息
     *
     * @param userTokenInfo 用户模型
     * @return 返回创建信息
     **/
    @CachePut(value = "userToken", key = "#userTokenInfo.uid")
    public UserTokenInfo updateUserToken(UserTokenInfo userTokenInfo) throws IOException {
        return getCwmData().getIUserStrategy().updateUserToken(userTokenInfo);
    }


    /**
     * 删除过期token
     */
    public void deleteLitmitToken() throws IOException {
        getCwmData().getIUserStrategy().deleteLitmitToken();
    }


    /**
     * 获取token
     *
     * @param uid
     * @param token token
     * @return
     */
    @Cacheable(value = "userToken", key = "#uid")
    public UserTokenInfo findUserToken(String uid, String token) throws IOException {
        return getCwmData().getIUserStrategy().findUserToken(uid, token);
    }

    /**
     * 获取token
     *
     * @param uid   uid
     * @param appId oauthAppId
     * @return
     */
    @Cacheable(value = "userToken", key = "#uid")
    public UserTokenInfo findLastUserTokenByUid(String uid, String appId) throws IOException {
        return getCwmData().getIUserStrategy().findLastUserTokenByUid(uid, appId);
    }

    /**
     * 获取token
     *
     * @param uid   uid
     * @param appId oauthAppId
     * @return UserTokenInfo
     */
    @Cacheable(value = "userToken", key = "#uid+#refreshToken")
    public UserTokenInfo findUserTokenByUidAndRefreshToken(String uid, String appId, String refreshToken) throws TokenException {
        return getCwmData().getIUserStrategy().findUserTokenByUidAndRefreshToken(uid, appId, refreshToken);
    }
    //endregion

}
