package com.cqwo.ucenter.data;

import com.cqwo.ucenter.core.domain.users.OauthInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * Created by cqnews on 2017/4/11.
 */


//第三方登录
@Service(value = "OauthsData")
public class Oauths extends DataService {


    //region  第三方登录方法

    /**
     * 获得第三方登录数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getOauthCount(Specification<OauthInfo> condition) throws IOException {
        return getCwmData().getIUserStrategy().getOauthCount(condition);
    }

    /**
     * 创建一条第三方登录数据
     *
     * @param oauthInfo 第三方登录模型
     * @return 返回创建信息
     **/
    public OauthInfo createOauth(OauthInfo oauthInfo) throws IOException {
        return getCwmData().getIUserStrategy().createOauth(oauthInfo);
    }

    /**
     * 更新一条第三方登录数据
     *
     * @param oauthInfo 第三方登录模型
     **/
    public OauthInfo updateOauth(OauthInfo oauthInfo) throws IOException {
        return getCwmData().getIUserStrategy().updateOauth(oauthInfo);
    }

    /**
     * 删除一条第三方登录数据
     *
     * @param id 第三方登录模型
     **/
    public void deleteOauthById(int id) throws IOException {
        getCwmData().getIUserStrategy().deleteOauthById(id);
    }

    /**
     * 批量删除一批第三方登录数据
     **/
    public void deleteOauthByIdList(String idList) throws IOException {
        getCwmData().getIUserStrategy().deleteOauthByIdList(idList);
    }

    /**
     * 获取一条第三方登录数据
     *
     * @param id 第三方登录模型
     **/
    public OauthInfo getOauthById(int id) throws IOException {
        return getCwmData().getIUserStrategy().getOauthById(id);
    }


    /**
     * 获得第三方登录数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回OauthInfo
     **/
    public List<OauthInfo> getOauthList(Specification<OauthInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIUserStrategy().getOauthList(condition, sort);
    }


    /**
     * 获得第三方登录数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回OauthInfo
     **/
    public Page<OauthInfo> getOauthList(Integer pageSize, Integer pageNumber, Specification<OauthInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIUserStrategy().getOauthList(pageSize, pageNumber, condition, sort);
    }

    /**
     * 检测OpendId是否重复
     *
     * @param server 服务
     * @param openId opendId
     * @param oauthAppId  oauthAppId
     * @return
     */
    public OauthInfo getOauthByOpenId(String server, String openId, String oauthAppId) throws IOException {
        return getCwmData().getIUserStrategy().getOauthByOpenId(server, openId, oauthAppId);
    }

    /**
     * @param server
     * @param openId
     * @param unionId
     * @param oauthAppId
     * @return
     * @throws IOException
     */
    public OauthInfo getOauthByOpenIdAndUnionId(String server, String openId, String unionId, String oauthAppId) throws IOException {
        return getCwmData().getIUserStrategy().getOauthByOpenIdAndUnionId(server, openId, unionId, oauthAppId);
    }

    /**
     * 判断服务的OpenId是否存在
     *
     * @param server 服务
     * @param openId openId
     * @param oauthAppId  oauthAppId
     * @return
     */
    public boolean existsOauthByOAuthAppId(String server, String openId, String oauthAppId) throws IOException {
        return getCwmData().getIUserStrategy().existsOauthByOAuthAppId(server, openId, oauthAppId);
    }

    /**
     * 查找用户是否存在
     *
     * @param server  服务
     * @param unionId unionId
     * @return
     */
    public boolean existsOauthByUnionId(String server, String unionId) throws IOException {
        return getCwmData().getIUserStrategy().existsOauthByUnionId(server, unionId);
    }

    /**
     * 通过appid和uid查找用户信息
     *
     * @param oauthAppId oauthAppId
     * @param uid   uid
     * @return
     */
    public OauthInfo getOauthByOAuthAppIdAndUid(String oauthAppId, String uid) throws IOException {
        return getCwmData().getIUserStrategy().getOauthByOAuthAppIdAndUid(oauthAppId, uid);
    }


    //endregion

}
