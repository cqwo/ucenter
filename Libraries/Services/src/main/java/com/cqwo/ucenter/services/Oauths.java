package com.cqwo.ucenter.services;

import com.cqwo.ucenter.core.domain.users.OauthInfo;
import com.cqwo.ucenter.core.helper.StringHelper;
import com.cqwo.ucenter.core.log.Logs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cqnews on 2017/4/11.
 */

//第三方登录
@Service(value = "Oauths")
public class Oauths {
    @Resource(name = "OauthsData")
    com.cqwo.ucenter.data.Oauths oauths;

    @Autowired
    private Logs logs;


    //region  第三方登录方法

    /**
     * 获得第三方登录数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getOauthCount(Specification<OauthInfo> condition) {

        try {
            return oauths.getOauthCount(condition);
        } catch (Exception ex) {
            logs.write(ex, "获得第三方登录数量失败");
        }
        return 0;
    }

    /**
     * 创建一条第三方登录数据
     *
     * @param oauthInfo 第三方登录模型
     * @return 返回创建信息
     **/
    public OauthInfo createOauth(OauthInfo oauthInfo) {
        try {
            return oauths.createOauth(oauthInfo);
        } catch (Exception ex) {
            logs.write(ex, "创建一条第三方登录数据失败");
        }
        return null;
    }

    /**
     * 创建oauth
     *
     * @param server
     * @param openId
     * @param unionId
     * @param oauthAppId
     * @return
     */
    public OauthInfo createOauth(String uid, String openId, String unionId, String server, String oauthAppId) {

        try {

            return oauths.createOauth(new OauthInfo(0, uid, openId, unionId, server, oauthAppId));

        } catch (Exception ex) {
            logs.write(ex, "创建oauth");
        }
        return null;
    }

    /**
     * 更新一条第三方登录数据
     *
     * @param oauthInfo 第三方登录模型
     **/
    public OauthInfo updateOauth(OauthInfo oauthInfo) {
        try {
            return oauths.updateOauth(oauthInfo);
        } catch (Exception ex) {
            logs.write(ex, "更新一条第三方登录数据异常");
        }

        return null;
    }

    /**
     * 删除一条第三方登录数据
     *
     * @param id 第三方登录模型
     **/
    public void deleteOauthById(int id) {
        try {
            oauths.deleteOauthById(id);
        } catch (Exception ex) {
            logs.write(ex, "删除一条第三方登录数据异常");
        }
    }

    /**
     * 批量删除一批第三方登录数据
     **/
    public void deleteOauthByIdList(String idList) {
        try {
            oauths.deleteOauthByIdList(idList);
        } catch (Exception ex) {
            logs.write(ex, "批量删除一批第三方登录数据异常");
        }
    }

    /**
     * 获取一条第三方登录数据
     *
     * @param id 第三方登录模型
     **/
    public OauthInfo getOauthById(int id) {
        try {
            return oauths.getOauthById(id);
        } catch (Exception ex) {
            logs.write(ex, "获取一条第三方登录数据");
        }

        return null;
    }


    /**
     * 获得第三方登录数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回OauthInfo
     **/
    public List<OauthInfo> getOauthList(Specification<OauthInfo> condition, Sort sort) {

        List<OauthInfo> oauthList = new ArrayList<OauthInfo>();

        try {
            oauthList = oauths.getOauthList(condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得第三方登录数据列表异常");
        }

        return oauthList;
    }


    /**
     * 获得第三方登录数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回OauthInfo
     **/
    public Page<OauthInfo> getOauthList(Integer pageSize, Integer pageNumber, Specification<OauthInfo> condition, Sort sort) {

        Page<OauthInfo> oauthList = null;

        try {
            oauthList = oauths.getOauthList(pageSize, pageNumber, condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得第三方登录数据列表异常");
        }

        return oauthList;
    }

    /**
     * 检测OpendId是否重复
     *
     * @param openId opendId
     * @param server 服务
     * @return
     */
    public OauthInfo getOauthByOpenId(String server, String openId, String oauthAppId) {

        try {
            return oauths.getOauthByOpenId(server, openId, oauthAppId);
        } catch (Exception ex) {
            logs.write(ex, "检测OpendId是否重复");
        }

        return null;

    }


    /**
     * 通过unionId和openId获取授权
     *
     * @param server  服务
     * @param openId  开发id
     * @param unionId 联合id
     * @return
     * @throws IOException
     */
    public OauthInfo getOauthByOpenIdAndUnionId(String server, String openId, String unionId, String oauthAppId) {

        OauthInfo oauthInfo = null;

        try {

            if (StringHelper.isNotNullOrWhiteSpace(unionId)) {
                oauthInfo = oauths.getOauthByOpenIdAndUnionId(server, openId, unionId, oauthAppId);
            } else {
                oauthInfo = getOauthByOpenId(server, openId, oauthAppId);
            }
        } catch (Exception ex) {

            logs.write(ex, "通过unionId和openId获取授权");
        }

        return oauthInfo;
    }

    /**
     * 判断服务的OpenId是否存在
     *
     * @param server     服务
     * @param openId     openId
     * @param oauthAppId oauthAppId
     * @return
     */
    public boolean existsOauthByOAuthAppId(String server, String openId, String oauthAppId) {

        try {
            return oauths.existsOauthByOAuthAppId(server, openId, oauthAppId);
        } catch (IOException ex) {
            logs.write(ex, "检测OpendId是否重复");
        }

        return false;
    }

    /**
     * 查找用户是否存在
     *
     * @param server  服务
     * @param unionId unionId
     * @return
     */
    public boolean existsOauthByUnionId(String server, String unionId) {

        try {

            return oauths.existsOauthByUnionId(server, unionId);

        } catch (IOException ex) {
            logs.write(ex, "检测OpendId是否重复");
        }

        return false;
    }

    /**
     * 通过appid和uid查找用户信息
     *
     * @param oauthAppId oauthAppId
     * @param uid        uid
     * @return
     */
    public OauthInfo getOauthByOAuthAppIdAndUid(String oauthAppId, String uid) {

        try {

            return oauths.getOauthByOAuthAppIdAndUid(oauthAppId, uid);

        } catch (IOException ex) {
            logs.write(ex, "通过appid和uid查找用户信息");
        }
        return null;
    }


    //endregion

}
