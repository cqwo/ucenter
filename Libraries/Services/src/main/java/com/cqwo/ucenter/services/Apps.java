package com.cqwo.ucenter.services;

import com.cqwo.ucenter.core.domain.app.AppInfo;
import com.cqwo.ucenter.core.log.Logs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author cqnews
 * @date 2017/4/11
 */

//应用管理
@Service(value = "Apps")
public class Apps {

    @Resource(name = "AppDatas")
    com.cqwo.ucenter.data.Apps apps;

    @Autowired
    private Logs logs;


    //region  应用管理方法

    /**
     * 获得应用管理数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getAppCount(Specification<AppInfo> condition) {

        try {
            return apps.getAppCount(condition);
        } catch (IOException ex) {
            logs.write(ex, "获得应用管理数量失败");
        }
        return 0;
    }

    /**
     * 创建一条应用管理数据
     *
     * @param appInfo 应用管理模型
     * @return 返回创建信息
     **/
    public AppInfo createApp(AppInfo appInfo) {
        try {
            return apps.createApp(appInfo);
        } catch (IOException ex) {
            logs.write(ex, "创建一条应用管理数据失败");
        }
        return null;
    }

    /**
     * 更新一条应用管理数据
     *
     * @param appInfo 应用管理模型
     **/
    public AppInfo updateApp(AppInfo appInfo) {
        try {
            return apps.updateApp(appInfo);
        } catch (IOException ex) {
            logs.write(ex, "更新一条应用管理数据异常");
        }

        return null;
    }

    /**
     * 删除一条应用管理数据
     *
     * @param appId 应用管理模型
     **/
    public void deleteAppByAppId(int appId) {
        try {
            apps.deleteAppByAppId(appId);
        } catch (IOException ex) {
            logs.write(ex, "删除一条应用管理数据异常");
        }
    }

    /**
     * 批量删除一批应用管理数据
     **/
    public void deleteAppByAppIdList(String appIdList) {
        try {
            apps.deleteAppByAppIdList(appIdList);
        } catch (IOException ex) {
            logs.write(ex, "批量删除一批应用管理数据异常");
        }
    }

    /**
     * 获取一条应用管理数据
     *
     * @param appId 应用管理模型
     **/
    public AppInfo getAppById(Integer appId) {
        try {
            return apps.getAppById(appId);
        } catch (IOException ex) {
            logs.write(ex, "获取一条应用管理数据");
        }

        return null;
    }

    /**
     * 获取一条应用管理数据
     *
     * @param appId 应用管理模型
     **/
    public AppInfo getAppByAppId(String appId) {
        try {
            return apps.getAppByAppId(appId);
        } catch (IOException ex) {
            logs.write(ex, "获取一条应用管理数据");
        }

        return null;
    }

    /**
     * 获取用户apiKey
     *
     * @param apiKey    apiKey
     * @param apiSecret apiSecret
     */
    public AppInfo getAppByApiKeyAndApiSecret(String apiKey, String apiSecret) {

        AppInfo appInfo = null;

        try {

            appInfo = apps.getAppByApiKeyAndApiSecret(apiKey, apiSecret);

        } catch (Exception ex) {

            logs.write(ex, "用户校验token失败");
        }

        return appInfo;

    }


    /**
     * 获得应用管理数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回ApplicationInfo
     **/
    public List<AppInfo> getAppList(Specification<AppInfo> condition, Sort sort) {

        List<AppInfo> applicationList = new ArrayList<AppInfo>();

        try {
            applicationList = apps.getAppList(condition, sort);
        } catch (IOException ex) {
            logs.write(ex, "获得应用管理数据列表异常");
        }

        return applicationList;
    }


    /**
     * 获得应用管理数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回ApplicationInfo
     **/
    public Page<AppInfo> getAppList(Integer pageSize, Integer pageNumber, Specification<AppInfo> condition, Sort sort) {

        Page<AppInfo> applicationList = null;

        try {
            applicationList = apps.getAppList(pageSize, pageNumber, condition, sort);
        } catch (IOException ex) {
            logs.write(ex, "获得应用管理数据列表异常");
        }

        return applicationList;
    }

    /**
     * 列表条件组装
     *
     * @return
     */
    public Specification<AppInfo> getAppListCondition(String appName) {

        Specification<AppInfo> condition = null;

        try {

            condition = (Specification<AppInfo>) (root, query, cb) -> {

                List<Predicate> list = new ArrayList<>();

                list.add(cb.like(root.get("appName").as(String.class), "%" + appName + "%"));

                Predicate[] p = new Predicate[list.size()];

                query.where(cb.and(list.toArray(p)));


                return query.getGroupRestriction();
            };

        } catch (Exception ex) {
            logs.write(ex, "条件组装失败");
        }

        return condition;

    }


    //endregion

}
