package com.cqwo.ucenter.services;

import com.alibaba.fastjson.JSON;
import com.cqwo.ucenter.core.domain.users.*;
import com.cqwo.ucenter.core.exception.token.NoLoginException;
import com.cqwo.ucenter.core.exception.token.TokenDecryptException;
import com.cqwo.ucenter.core.exception.token.TokenException;
import com.cqwo.ucenter.core.exception.users.FailException;
import com.cqwo.ucenter.core.exception.users.RepeatException;
import com.cqwo.ucenter.core.helper.*;
import com.cqwo.ucenter.core.log.Logs;
import com.cqwo.ucenter.data.UserDetails;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.vdurmont.emoji.EmojiParser;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.cqwo.ucenter.core.errors.CWMConstants.TOKEN_EXPIRE;
import static com.cqwo.ucenter.core.helper.AESHelper.encode;
import static com.cqwo.ucenter.core.helper.MD5Util.md5;

//用户
@Service(value = "Users")
public class Users {

    @Autowired
    private com.cqwo.ucenter.data.Users users;

    @Autowired
    private Logs logs;

    @Autowired
    private UserRanks userRanks;

    @Autowired
    private Oauths oauths;

    @Autowired
    private UserDetails userDetails;

    @Autowired
    private com.cqwo.ucenter.data.UserTokens userTokens;

    private Lock loginLock = new ReentrantLock();

    //region  用户信息方法


    /**
     * 创建一条用户信息数据
     *
     * @param userInfo 用户信息模型
     * @return 返回创建信息
     **/
    public PartUserInfo createUser(PartUserInfo userInfo) {
        try {
            return users.createPartUser(userInfo);
        } catch (Exception ex) {
            logs.write(ex, "创建一条用户信息数据失败");
        }
        return null;
    }

    /**
     * 更新一条用户信息数据
     *
     * @param userInfo 用户信息模型
     **/
    public PartUserInfo updateUser(PartUserInfo userInfo) {
        try {
            return users.updateUser(userInfo);
        } catch (Exception ex) {
            logs.write(ex, "更新一条用户信息数据异常");
        }

        return null;
    }

    /**
     * 删除一条用户信息数据
     *
     * @param uid 用户信息模型
     **/
    public void deleteUserByUid(String uid) {
        try {
            users.deleteUserByUid(uid);
        } catch (Exception ex) {
            logs.write(ex, "删除一条用户信息数据异常");
        }
    }

    /**
     * 批量删除一批用户信息数据
     **/
    public void deleteUserByUidList(String uidList) {
        try {
            users.deleteUserByUidList(uidList);
        } catch (Exception ex) {
            logs.write(ex, "批量删除一批用户信息数据异常");
        }
    }

    /**
     * 获取一条用户信息数据
     *
     * @param uid 用户信息模型
     **/
    public PartUserInfo getUserByUid(String uid) {
        try {
            return users.getPartUserByUid(uid);
        } catch (Exception ex) {
            logs.write(ex, "获取一条用户信息数据");
        }

        return null;
    }


    /**
     * 获得用户信息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回UserInfo
     **/
    public List<PartUserInfo> getUserList(Specification<PartUserInfo> condition, Sort sort) {

        List<PartUserInfo> userList = new ArrayList<PartUserInfo>();

        try {
            userList = users.getPartUserList(condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得用户信息数据列表异常");
        }

        return userList;
    }


    /**
     * 获得用户信息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回UserInfo
     **/
    public Page<PartUserInfo> getUserList(Integer pageSize, Integer pageNumber, Specification<PartUserInfo> condition, Sort sort) {

        Page<PartUserInfo> userList = null;

        try {
            userList = users.getPartUserList(pageSize, pageNumber, condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得用户信息数据列表异常");
        }

        return userList;
    }


    //endregion

    /**
     * 获取一条用户数据
     *
     * @param uid 用户模型
     **/
    public PartUserInfo getPartUserByUid(String uid) {


        try {
            if (uid.isEmpty()) {
                return null;
            }

            return users.getPartUserByUid(uid);
        } catch (Exception ex) {
            logs.write(ex, "获取一条用户数据");
        }
        return null;
    }


    /**
     * 获得用户数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @return 返回UserInfo
     **/
    public Page<PartUserInfo> getPartUserList(Integer pageSize, Integer pageNumber, Specification<PartUserInfo> condition, Sort sort) {


        try {

            return users.getPartUserList(pageSize, pageNumber, condition, sort);

        } catch (Exception ex) {

            logs.write(ex, "获取用户数据列表");
        }

        return null;

    }

    //region 用户中心


    /**
     * 创建基本用户信息
     *
     * @param userInfo 用户信息
     * @return
     */
    public PartUserInfo createPartUser(PartUserInfo userInfo) {
        try {
            return users.createPartUser(userInfo);
        } catch (Exception ex) {
            ex.printStackTrace();
            logs.write(ex, "创建基本用户信息");
        }
        return null;
    }

    public long getUserCount(Specification<PartUserInfo> condition) {

        try {
            return users.getPartUserCount(condition);
        } catch (Exception ex) {

            logs.write(ex, "数据处理失败");
        }

        return 0;
    }


    /**
     * 获得用户数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @return 返回UserInfo
     **/
    public Page<PartUserInfo> getPartUserList(int pageSize, int pageNumber, Specification<PartUserInfo> condition, Sort sort) {

        Page<PartUserInfo> infoPage = null;

        try {

            infoPage = users.getPartUserList(pageSize, pageNumber, condition, sort);

        } catch (Exception ex) {

            logs.write(ex, "获取用户数据列表");
        }

        return infoPage;

    }


    /**
     * 通过用户名获取用户信息
     *
     * @param userName
     * @return
     */
    public PartUserInfo getPartUserByUserName(String userName) {

        try {

            return users.getPartUserByUserName(userName);

        } catch (Exception ex) {
            logs.write(ex, "通过用户名获取用户信息");
        }
        return null;
    }

    /**
     * 通过用户名获取用户信息
     *
     * @param email
     * @return
     */
    public PartUserInfo getPartUserByEmail(String email) {
        try {

            return users.getPartUserByEmail(email);

        } catch (Exception ex) {
            logs.write(ex, "通过用户名获取用户信息");
        }
        return null;
    }

    /**
     * 通过用户手机获取用户信息
     *
     * @param mobile 手机
     * @return
     */
    public PartUserInfo getPartUserByMobile(String mobile) {
        try {

            return users.getPartUserByMobile(mobile);

        } catch (Exception ex) {
            logs.write(ex, "通过用户名获取用户信息");
        }
        return null;
    }

    /**
     * 获取账号的用户信息
     *
     * @param account 账号,可为邮箱,手机和用户名
     * @return
     */
    public PartUserInfo getPartUserByAccount(String account) {

        //检测账号类型
        PartUserInfo userInfo = null;

        if (ValidateHelper.isValidEmail(account)) {
            userInfo = getPartUserByEmail(account);
            logs.write("邮箱登录");
        } else if (ValidateHelper.isValidMobile(account)) {
            userInfo = getPartUserByMobile(account);
            logs.write("手机登录");
        } else {
            userInfo = getPartUserByUserName(account);
            logs.write("账号登录");
        }
        return userInfo;
    }


    /**
     * 生成盐值
     *
     * @return
     */
    public String createSalt() {
        return RandomHelper.generateString(6);
    }


    /**
     * 创建用户密码
     *
     * @param password 真实密码
     * @param salt     散列盐值
     * @return
     */
    public String createUserPassword(String password, String salt) {

        System.out.println("password:" + password);
        System.out.println("salt:" + salt);
        System.out.println("md5:" + SecureHelper.md5(password + salt));
        return SecureHelper.md5(password + salt);
    }


    /**
     * 更新用户组
     *
     * @param uid     用户uid
     * @param userRid 用户分组
     */
    public void updateUserRankByUid(String uid, Integer userRid) {

        try {
            users.updateUserRankByUid(uid, userRid);
        } catch (Exception ex) {
            logs.write(ex, "更新用户组失败");
        }
    }


    /**
     * 创过token用户
     *
     * @param uid       用户uid
     * @param appId     appid
     * @param salt      盐值
     * @param limitTime 过期时间
     * @return
     */
    private String createUserToken(String uid, String appId, String salt, Integer limitTime) {

        String token = null;

        try {

            UserToken userToken = new UserToken(uid, appId, salt, limitTime);
            String content = JSON.toJSONString(userToken);
            System.out.println(content);
            token = encode(content);

        } catch (Exception ex) {

            //logs.write("用户创建token失败");

        }

        return token;
    }

    /**
     * 创建刷新密角球
     *
     * @param token token
     * @return 刷新密钥
     */
    private String createRefreshToken(String token) {

        String refreshToken = null;

        try {
            refreshToken = md5(token + "iloverliuyufei");
        } catch (Exception ex) {
            logs.write("用户创建token失败");
        }

        return refreshToken;
    }

    /**
     * 用户解密
     *
     * @param token token
     * @return
     */
    public UserToken decryptUserToken(String token) {

        UserToken tokenInfo = null;

        if (StringHelper.isNullOrWhiteSpace(token)) {
            return null;
        }

        try {
            String postStr = AESHelper.decode(token);
            tokenInfo = JSON.parseObject(postStr, UserToken.class);

        } catch (Exception ex) {

            //logs.write(ex, "用户解密失败");
        }

        return tokenInfo;

    }

    /**
     * 安全处理用户信息
     *
     * @param userInfo 用户信息
     * @return
     */
    public static PartUserInfo securityUserInfo(PartUserInfo userInfo) {

        if (userInfo == null) {
            return null;
        }


        userInfo.setPassword("*****");
        // userInfo.setMobile(StringHelper.replaceWithSpecialChar(userInfo.getMobile()));
        userInfo.setSalt("******");

        return userInfo;
    }

    /**
     * 通过用户OpendId获取用用户信息
     *
     * @param server     server
     * @param openId     openId
     * @param oauthAppId oauthAppId
     * @return
     */
    public PartUserInfo getPartUserByOpenIdAndOAuthAppId(String server, String openId, String oauthAppId) {

        try {

            return users.getPartUserByOpenIdAndOAuthAppId(server, openId, oauthAppId);

        } catch (Exception ex) {
            logs.write(ex, "通过用户OpendId获取用用户信息");
        }
        return null;
    }

    /**
     * /**
     * 用户第三方登录
     *
     * @param nickName   用户昵称
     * @param avatar     头像
     * @param gender     性别
     * @param oauthAppId appid
     * @param regionId   行政区域
     * @return
     */
    public PartUserInfo oauthLogin(String server,
                                   String openId,
                                   String unionId,
                                   String nickName,
                                   String avatar,
                                   Integer gender,
                                   Integer regionId,
                                   String oauthAppId) throws RepeatException, FailException {


        loginLock.lock();
        try {

            if (oauths.existsOauthByOAuthAppId(server, openId, oauthAppId)) {

                PartUserInfo userInfo = getPartUserByOpenIdAndOAuthAppId(server, openId, oauthAppId);

                throw new RepeatException("用户已注册,登录成功", userInfo);
            }

            if (oauths.existsOauthByUnionId(server, unionId)) {

                PartUserInfo userInfo = users.getPartUserByUnionId(server, unionId);

                if (userInfo != null && StringHelper.isNotNullOrWhiteSpace(userInfo.getUid())) {
                    //String uid, String openId, String unionId, String server, String oauthAppId
                    oauths.createOauth(userInfo.getUid(), openId, unionId, server, oauthAppId);
                    throw new RepeatException("用户已注册,登录成功", userInfo);
                }

            }


            PartUserInfo userInfo = new PartUserInfo();

            Integer nowts = DateHelper.getUnixTimeStamp();

            userInfo.setSalt(createSalt());
            userInfo.setPassword(createUserPassword(userInfo.getSalt(), userInfo.getSalt()));
            userInfo.setUserRid(userRanks.getLowestUserRank().getUserRid());

            nickName = EmojiParser.removeAllEmojis(nickName);

            userInfo.setNickName(nickName);
            userInfo.setAvatar(avatar);

            userInfo.setRegionId(regionId);
            userInfo.setUserName("");
            userInfo.setEmail("");
            userInfo.setPayCredits(0);
            userInfo.setRankCredits(0);
            userInfo.setVerifyEmail(0);
            userInfo.setVerifyMobile(0);
            userInfo.setLiftBanTime(0);
            userInfo.setGender(gender);
            userInfo.setBirthday(nowts);
            userInfo.setIdCard("");
            userInfo.setCreateTime(nowts);

            logs.write("用户注册");

            userInfo = createPartUser(userInfo);
            logs.write("uid:" + userInfo.getUid());

            if (userInfo == null || StringHelper.isNullOrWhiteSpace(userInfo.getUid())) {

                throw new FailException("账号注册失败");

            }


            OauthInfo oauthInfo = new OauthInfo(0, userInfo.getUid(), openId, unionId, server, oauthAppId);

            oauths.createOauth(oauthInfo);


            UserDetailInfo userDetailInfo = new UserDetailInfo();

            userDetailInfo.setUid(userInfo.getUid());
            userDetailInfo.setLastVisitTime(nowts);
            userDetailInfo.setLastVisitRgid(regionId);
            userDetailInfo.setRegisterTime(nowts);
            userDetailInfo.setRegisterRgid(regionId);
            userDetailInfo.setBio("");


            logs.write("用户第三方登录注册:" + userDetailInfo.toString());
            createUserDetail(userDetailInfo);


            logs.write("用户第三方登录注册成功");

            return userInfo;


        } catch (RepeatException ex) {

            if (ex.getUserInfo() != null) {
                return ex.getUserInfo();
            }

            throw ex;

        } catch (FailException ex) {
            logs.write(ex, "账号注册失败");
            throw ex;
        } catch (Exception ex) {
            logs.write(ex, "用户第三方登录注册失败");
        } finally {
            loginLock.unlock();
        }

        return null;
    }

    /**
     * /**
     * 用户简单注册
     *
     * @param account  用户账号
     * @param password 用户密码
     * @param nickName 用户昵称
     * @param avatar   头像
     * @param gender   性别
     * @param regionId 行政区域
     * @return
     */
    public PartUserInfo createSimpleUserInfo(String account,
                                             String password,
                                             String nickName,
                                             String avatar,
                                             Integer gender,
                                             Integer regionId) throws RepeatException, FailException {

        PartUserInfo userInfo = new PartUserInfo();

        Integer nowts = DateHelper.getUnixTimeStamp();

        if (ValidateHelper.isValidEmail(account)) {

            if (checkedUserByEmail(account)) {
                throw new RepeatException("重复的邮箱地址");
            }
            userInfo.setEmail(account);
            logs.write("邮箱注册");

        } else if (ValidateHelper.isValidMobile(account)) {

            if (checkedUserByMobile(account)) {
                throw new RepeatException("重复的手机");
            }

            userInfo.setMobile(account);
            logs.write("手机注册");

        } else {

            if (checkedUserByUserName(account)) {
                throw new RepeatException("重复的用户名");
            }

            userInfo.setUserName(account);

            logs.write("账号注册");
        }

        userInfo.setSalt(createSalt());
        userInfo.setPassword(createUserPassword(password, userInfo.getSalt()));
        userInfo.setUserRid(userRanks.getLowestUserRank().getUserRid());


        userInfo.setNickName(nickName);
        userInfo.setAvatar(avatar);

        userInfo.setRegionId(regionId);
        userInfo.setPayCredits(0);
        userInfo.setRankCredits(0);
        userInfo.setVerifyEmail(0);
        userInfo.setVerifyMobile(0);
        userInfo.setLiftBanTime(0);
        userInfo.setGender(gender);
        userInfo.setBirthday(nowts);
        userInfo.setIdCard("");

        userInfo.setCreateTime(nowts);

        logs.write("用户注册");

        try {
            userInfo = createPartUser(userInfo);
            logs.write("uid:" + userInfo.getUid());

            if (StringHelper.isNotNullOrWhiteSpace(userInfo.getUid())) {

                UserDetailInfo userDetailInfo = new UserDetailInfo();

                userDetailInfo.setUid(userInfo.getUid());
                userDetailInfo.setLastVisitTime(nowts);
                userDetailInfo.setLastVisitRgid(regionId);
                userDetailInfo.setRegisterTime(nowts);
                userDetailInfo.setRegisterRgid(regionId);
                userDetailInfo.setBio("");


                logs.write("用户简单注册:" + userDetailInfo.toString());
                createUserDetail(userDetailInfo);


                logs.write("用户简单注册成功");

                return userInfo;
            }

            throw new FailException("用户简单注册失败");

        } catch (FailException ex) {

            ex.printStackTrace();
            throw ex;

        } catch (Exception ex) {

            logs.write(ex, "用户简单注册失败");
            throw new FailException("用户简单注册失败");
        }

    }


    public OauthInfo bindUserOAuthor(PartUserInfo userInfo, String server, String openId, String unionId, String nickName,
                                     String avatar, Integer sex, int regionId, String oauthAppId) throws FailException {

        if (StringHelper.isNotNullOrWhiteSpace(userInfo.getUid())) {


            OauthInfo oauthInfo = new OauthInfo(0, userInfo.getUid(), openId, unionId, server, oauthAppId);


            oauthInfo = oauths.createOauth(oauthInfo);

            if (oauthInfo == null || oauthInfo.getId() <= 0) {
                throw new FailException("用户绑定失败");
            }


            if (StringHelper.isNullOrWhiteSpace(userInfo.getNickName())) {
                userInfo.setNickName(nickName);
            }
            if (StringHelper.isNullOrWhiteSpace(userInfo.getAvatar())) {
                userInfo.setAvatar(avatar);
            }

            if (userInfo.getGender().equals(0)) {
                userInfo.setGender(sex);
            }

            if (userInfo.getRegionId() <= 0) {
                userInfo.setRegionId(regionId);
            }

            updateUser(userInfo);

            logs.write("用户绑定成功");

            return oauthInfo;
        }

        throw new FailException("用户绑定失败");
    }

    /**
     * 检测用户邮箱是否存在
     *
     * @param email 邮箱地址
     * @return
     */
    private boolean checkedUserByEmail(String email) {

        try {

            return users.checkedUserByEmail(email);

        } catch (Exception ex) {

            logs.write(ex, "检测用户邮箱是否存在");
        }

        return false;
    }

    /**
     * 检测用户账号是否已经存在
     *
     * @param account 用户账号
     * @return
     */
    public boolean checkedUserByAccount(String account) {

        try {

            return users.checkedUserByAccount(account);

        } catch (Exception ex) {
            logs.write(ex, "检测用户账号是否已经存在");
        }

        return true;
    }

    /**
     * 检测用户用户名是否存在
     *
     * @param userName 用户名
     * @return
     */
    public boolean checkedUserByUserName(String userName) {

        try {

            return users.checkedUserByUserName(userName);

        } catch (Exception ex) {

            logs.write(ex, "检测用户用户名是否存在");
        }

        return true;
    }


    /**
     * 检测用户邮箱是否存在
     *
     * @param mobile 邮箱地址
     * @return
     */
    public boolean checkedUserByMobile(String mobile) {

        try {

            return users.checkedUserByMobile(mobile);

        } catch (Exception ex) {

            logs.write(ex, "检测用户邮箱是否存在");
        }

        return true;
    }

    //endregion


    //region  用户详情方法

    /**
     * 获得用户详情数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getUserDetailCount(Specification<UserDetailInfo> condition) {

        try {
            return userDetails.getUserDetailCount(condition);
        } catch (Exception ex) {
            logs.write(ex, "获得用户详情数量失败");
        }
        return 0;
    }

    /**
     * 创建一条用户详情数据
     *
     * @param userdetailInfo 用户详情模型
     * @return 返回创建信息
     **/
    public UserDetailInfo createUserDetail(UserDetailInfo userdetailInfo) {
        try {
            return userDetails.createUserDetail(userdetailInfo);
        } catch (Exception ex) {
            logs.write(ex, "创建一条用户详情数据失败");
        }
        return null;
    }

    /**
     * 更新一条用户详情数据
     *
     * @param userdetailInfo 用户详情模型
     **/
    public UserDetailInfo updateUserDetail(UserDetailInfo userdetailInfo) {
        try {
            return userDetails.updateUserDetail(userdetailInfo);
        } catch (Exception ex) {
            logs.write(ex, "更新一条用户详情数据异常");
        }

        return null;
    }

    /**
     * 删除一条用户详情数据
     *
     * @param id 用户详情模型
     **/
    public void deleteUserDetailById(int id) {
        try {
            userDetails.deleteUserDetailById(id);
        } catch (Exception ex) {
            logs.write(ex, "删除一条用户详情数据异常");
        }
    }

    /**
     * 批量删除一批用户详情数据
     **/
    public void deleteUserDetailByIdList(String idList) {
        try {
            userDetails.deleteUserDetailByIdList(idList);
        } catch (Exception ex) {
            logs.write(ex, "批量删除一批用户详情数据异常");
        }
    }

    /**
     * 获取一条用户详情数据
     *
     * @param id 用户详情模型
     **/
    public UserDetailInfo getUserDetailById(int id) {
        try {
            return userDetails.getUserDetailById(id);
        } catch (Exception ex) {
            logs.write(ex, "获取一条用户详情数据");
        }

        return null;
    }


    /**
     * 获得用户详情数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回UserDetailInfo
     **/
    public List<UserDetailInfo> getUserDetailList(Specification<UserDetailInfo> condition, Sort sort) {

        List<UserDetailInfo> userDetailList = new ArrayList<UserDetailInfo>();

        try {
            userDetailList = userDetails.getUserDetailList(condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得用户详情数据列表异常");
        }

        return userDetailList;
    }


    /**
     * 获得用户详情数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回UserDetailInfo
     **/
    public Page<UserDetailInfo> getUserDetailList(Integer pageSize, Integer pageNumber, Specification<UserDetailInfo> condition, Sort sort) {

        Page<UserDetailInfo> userDetailList = null;

        try {
            userDetailList = userDetails.getUserDetailList(pageSize, pageNumber, condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得用户详情数据列表异常");
        }

        return userDetailList;
    }

    /**
     * 获取用户列表条件
     *
     * @param uid      uid
     * @param nickName 昵称
     * @param mobile   手机号码
     * @return
     */
    public Specification<PartUserInfo> getPartUserListCondition(String uid, String nickName, String mobile) {

        return (Specification<PartUserInfo>) (root, query, cb) -> {

            List<Predicate> list = Lists.newArrayList();


            if (StringHelper.isNotNullOrWhiteSpace(uid)) {
                list.add(cb.equal(root.get("uid").as(String.class), uid));
            }
            if (StringHelper.isNotNullOrWhiteSpace(nickName)) {
                list.add(cb.like(root.get("nickName").as(String.class), "%" + nickName + "%"));
            }

            if (StringHelper.isNotNullOrWhiteSpace(mobile)) {
                list.add(cb.like(root.get("mobile").as(String.class), "%" + mobile + "%"));
            }

            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)));

            return query.getGroupRestriction();
        };
    }


    //endregion


    //region token管理


    /**
     * 更新token
     *
     * @param uid uid
     * @return 返回用户token
     * @throws NoLoginException NoLoginException
     */
    public UserTokenInfo updateUserToken(String uid) throws TokenException, NoLoginException {
        return updateUserToken(uid, 2);
    }

    /**
     * 更新token
     *
     * @param uid   uid
     * @param hours 时间
     * @return 返回用户token
     * @throws NoLoginException NoLoginException
     */
    public UserTokenInfo updateUserToken(String uid, Integer hours) throws TokenException, NoLoginException {
        return updateUserToken(uid, "", hours);
    }

    /**
     * 更新token
     *
     * @param uid   uid
     * @param appId appid
     * @param hours 时间
     * @return 返回用户token
     * @throws TokenException TokenException
     */
    public UserTokenInfo updateUserToken(String uid, String appId, Integer hours) throws TokenException, NoLoginException {

        try {

            if (Strings.isNullOrEmpty(uid)) {
                throw new NoLoginException("用户uid不能为空");
            }

            Integer limitTime = DateHelper.getUnixTimeStamp() + 3600 * hours;

            String salt = createSalt();

            String token = createUserToken(uid, appId, salt, limitTime);

            String refreshToken = createRefreshToken(token);

            UserTokenInfo userTokenInfo = null;

            userTokenInfo = new UserTokenInfo(uid, appId);

            userTokenInfo.setLimitTime(limitTime);
            userTokenInfo.setSalt(salt);
            userTokenInfo.setToken(token);
            userTokenInfo.setRefreshToken(refreshToken);

            userTokenInfo = userTokens.updateUserToken(userTokenInfo);

            if (userTokenInfo == null || userTokenInfo.getId() <= 0) {
                throw new TokenException("token更新异常");
            }

            return userTokenInfo;


        } catch (NoLoginException | TokenException ex) {
            throw ex;
        } catch (Exception ex) {
            ex.printStackTrace();
            logs.write(ex, "token更新失败");
        }

        return null;
    }

    /**
     * 更新用户的token信息
     *
     * @param userTokenInfo 用户模型
     * @return 返回创建信息
     **/
    public UserTokenInfo updateUserToken(UserTokenInfo userTokenInfo) {

        try {
            return userTokens.updateUserToken(userTokenInfo);
        } catch (Exception ex) {
            logs.write(ex, "token更新失败");
        }
        return null;
    }


    /**
     * 删除过期token
     */
    public void deleteLitmitToken() throws IOException {
        try {
            userTokens.deleteLitmitToken();
        } catch (Exception ex) {
            logs.write(ex, "token删除失败");
        }
    }


    /**
     * 获取token
     *
     * @param token token
     * @return
     */
    public UserTokenInfo findUserToken(String token) throws TokenException {

        try {

            UserToken userToken = decryptUserToken(token);

            if (userToken == null || userToken.getUid().isEmpty()) {
                throw new TokenDecryptException("Token异常");
            }

            Integer nowTime = DateHelper.getUnixTimeStamp();

            if (userToken.getLimitTime() < nowTime) {
                throw new TokenDecryptException("Token过期");
            }

            return userTokens.findUserToken(userToken.getUid(), token);
        } catch (TokenException ex) {
            throw ex;
        } catch (Exception ex) {
            logs.write(ex, "获取TOKEN失败");
            throw new TokenException("获取TOKEN失败");
        }
    }

    /**
     * 获取token
     *
     * @param uid   uid
     * @param appId oauthAppId
     * @return UserTokenInfo
     */
    public UserTokenInfo findLastUserTokenByUid(String uid, String appId) throws TokenException {

        UserTokenInfo userTokenInfo = null;

        try {

            userTokenInfo = userTokens.findLastUserTokenByUid(uid, appId);

            Integer nowTime = DateHelper.getUnixTimeStamp();

            if (userTokenInfo.getLimitTime() < nowTime) {
                throw new TokenException("Token过期");
            }


        } catch (TokenException ex) {
            throw ex;
        } catch (Exception ex) {
            logs.write(ex, "获取TOKEN失败2");
        }


        if (userTokenInfo == null) {
            userTokenInfo = new UserTokenInfo(uid, appId);
        }

        return userTokenInfo;
    }

    /**
     * 获取token
     *
     * @param uid   uid
     * @param appId oauthAppId
     * @return UserTokenInfo
     */
    public UserTokenInfo findUserTokenByUidAndRefreshToken(String uid, String appId, String refreshToken) throws TokenException {

        UserTokenInfo userTokenInfo = null;

        try {

            userTokenInfo = userTokens.findUserTokenByUidAndRefreshToken(uid, appId, refreshToken);

            Integer nowTime = DateHelper.getUnixTimeStamp();

            if (userTokenInfo.getLimitTime() < nowTime + 2 * 60 * 60) {
                throw new TokenException("Token过期太久");
            }


        } catch (TokenException ex) {
            throw ex;
        } catch (Exception ex) {
            logs.write(ex, "获取TOKEN失败2");
        }


        if (userTokenInfo == null) {
            userTokenInfo = new UserTokenInfo(uid, appId);
        }

        return userTokenInfo;
    }

//    /**
//     * 获取token
//     *
//     * @param refreshToken 刷新token
//     * @param appId        oauthAppId
//     * @return UserTokenInfo
//     */
//    public UserTokenInfo findUserTokenByRefreshToken(String refreshToken, String appId) {
//        return null;
//    }


    @Test
    public void test() {

        UserToken token = new UserToken();

        System.out.println(JSON.toJSONString(token));

    }

    public PartUserInfo getGuestPartUserInfo() {
        return new PartUserInfo();
    }


    //endregion


}