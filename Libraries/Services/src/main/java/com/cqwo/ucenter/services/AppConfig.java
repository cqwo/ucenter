package com.cqwo.ucenter.services;


import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@ConfigurationProperties(prefix = "spring.application")
@Component(value = "AppConfig")
public class AppConfig {

    /**
     * 项目名称
     */
    private String name = "";

    /**
     * 项目域名地址
     */
    private String domain = "";


    /**
     * 管理员账号
     */
    private String[] admin = new String[]{"cqnews"};

}
