package com.cqwo.ucenter.services;/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */


import com.cqwo.ucenter.core.log.Logs;
import com.cqwo.ucenter.core.upload.CWMUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by cqnews on 2017/12/25.
 */
@Service(value = "Uploads")
public class Uploads {

    @Resource
    private CWMUpload cwmUpload;

    @Autowired
    private Logs logs;

    //region 文件上传

    /**
     * 文件上传
     *
     * @param s
     * @return
     */
    public List<String> fileUpload(String s, Map<String, MultipartFile> requestFileMap) {

        List<String> uploadFileList = new ArrayList<String>();

        try {
            uploadFileList = cwmUpload.getIUploadStrategy().fileUpload(s, requestFileMap);
        } catch (Exception ex) {
            logs.write(ex, "文件上传失败");
        }

        return uploadFileList;
    }


    /**
     * 获取客户端token
     *
     * @return
     */
    public String getUpToken() {

        String token = null;
        try {
            token = cwmUpload.getIUploadStrategy().getUpToken();
        } catch (Exception ex) {

            logs.write(ex, "获取客户端token");
        }
        return token;
    }

    /**
     * NB图片上传
     *
     * @param imgName
     * @param b
     */
    public String imgUpload(String imgName, byte[] b) {
        String path = null;
        try {
            path = cwmUpload.getIUploadStrategy().imgUpload(imgName, b);
        } catch (Exception ex) {
            logs.write(ex, "NB图片上传异常，请检查");
        }
        return path;
    }


    //endregion


    //endregion


}
