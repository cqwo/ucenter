package com.cqwo.ucenter.services;


import com.cqwo.ucenter.core.exception.CWMException;
import com.cqwo.ucenter.core.log.Logs;
import com.cqwo.ucenter.core.wechat.CWMWechat;
import com.cqwo.wechat.open.web.domain.WxOpenOAuth2AccessToken;
import com.cqwo.wechat.open.web.domain.WxOpenOAuth2User;
import com.cqwo.wechat.open.web.exption.WxOpenErrorException;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "WxOpenUtils")
public class WxOpenUtils {

    @Autowired
    private CWMWechat cwmWechat;

    @Autowired
    private Logs logs;


    /**
     * 获取开发接口
     *
     * @return
     * @throws CWMException
     * @throws WxErrorException
     */
    public String getOauth2CodeUrl(String redirectUrl) throws WxOpenErrorException {

        try {

            return cwmWechat.getIWxOpenStrategy().oauth2GetCodeUrl(redirectUrl);

        } catch (Exception ex) {

            logs.write(ex, "");
            throw new WxOpenErrorException("获取code异常");
        }

    }

    /**
     * 获取授权accesstoken
     * <p>
     * https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code
     *
     * @param code code code
     */
    public WxOpenOAuth2AccessToken oauth2GetAccessToken(String code) throws WxOpenErrorException {

        try {
            return cwmWechat.getIWxOpenStrategy().oauth2GetAccessToken(code);

        } catch (Exception ex) {

            logs.write(ex, "");
            throw new WxOpenErrorException("获取code异常");
        }
    }

    /**
     * 刷新或续期access_token使用
     * https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=%s&grant_type=refresh_token&refresh_token=%s
     *
     * @param refreshToken refresh_token
     */
    public WxOpenOAuth2AccessToken oauth2RefreshAccessToken(String refreshToken) throws WxOpenErrorException {
        try {
            return cwmWechat.getIWxOpenStrategy().oauth2RefreshAccessToken(refreshToken);

        } catch (Exception ex) {

            logs.write(ex, "");
            throw new WxOpenErrorException("获取code异常");
        }
    }


    /**
     * 检验授权凭证（access_token）是否有效
     * https://api.weixin.qq.com/sns/auth?access_token=%s&openid=%s
     *
     * @param appId accessToken
     */
    public boolean oauth2CheckAccessToken(String appId, String openId) throws WxOpenErrorException {

        try {
            return cwmWechat.getIWxOpenStrategy().oauth2CheckAccessToken(appId, openId);

        } catch (Exception ex) {

            logs.write(ex, "");
            throw new WxOpenErrorException("检验授权凭证（access_token）是否有效异常");
        }

    }

    /**
     * 获取用户信息
     *
     * @param openId openid
     */
    public WxOpenOAuth2User oauth2GetUserinfo(String openId) throws WxOpenErrorException {
        try {
            return cwmWechat.getIWxOpenStrategy().oauth2GetUserinfo(openId);

        } catch (Exception ex) {

            logs.write(ex, "");
            throw new WxOpenErrorException("获取用户信息异常");
        }
    }

    /**
     * 获取用户信息
     *
     * @param appId  accessToken
     * @param openId openid
     */
    public WxOpenOAuth2User oauth2GetUserinfo(String appId, String openId) throws WxOpenErrorException {
        try {
            return cwmWechat.getIWxOpenStrategy().oauth2GetUserinfo(appId, openId);

        } catch (Exception ex) {

            logs.write(ex, "");
            throw new WxOpenErrorException("获取用户信息异常");
        }
    }

    /**
     * 获取appId
     *
     * @return
     */
    public String getAppId() throws WxOpenErrorException {
        try {
            return cwmWechat.getIWxOpenStrategy().getAppId();

        } catch (Exception ex) {

            logs.write(ex, "");
            throw new WxOpenErrorException("获取appId异常");
        }
    }
}
