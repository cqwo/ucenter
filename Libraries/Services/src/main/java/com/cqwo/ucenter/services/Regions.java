package com.cqwo.ucenter.services;

import com.cqwo.ucenter.core.domain.base.RegionInfo;
import com.cqwo.ucenter.core.log.Logs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cqnews on 2017/4/11.
 */

//区域信息
@Service(value = "Regions")
public class Regions {
    @Resource(name = "RegionsData")
    com.cqwo.ucenter.data.Regions regions;

    @Autowired
    private Logs logs;


    //region  区域信息方法

    /**
     * 获得区域信息数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getRegionCount(Specification<RegionInfo> condition) {

        try {
            return regions.getRegionCount(condition);
        } catch (Exception ex) {
            logs.write(ex, "获得区域信息数量失败");
        }
        return 0;
    }

    /**
     * 创建一条区域信息数据
     *
     * @param regionInfo 区域信息模型
     * @return 返回创建信息
     **/
    public RegionInfo createRegion(RegionInfo regionInfo) {
        try {
            return regions.createRegion(regionInfo);
        } catch (Exception ex) {
            logs.write(ex, "创建一条区域信息数据失败");
        }
        return null;
    }

    /**
     * 更新一条区域信息数据
     *
     * @param regionInfo 区域信息模型
     **/
    public RegionInfo updateRegion(RegionInfo regionInfo) {
        try {
            return regions.updateRegion(regionInfo);
        } catch (Exception ex) {
            logs.write(ex, "更新一条区域信息数据异常");
        }

        return null;
    }

    /**
     * 删除一条区域信息数据
     *
     * @param regionId 区域信息模型
     **/
    public void deleteRegionByRegionid(Integer regionId) {
        try {
            regions.deleteRegionByRegionid(regionId);
        } catch (Exception ex) {
            logs.write(ex, "删除一条区域信息数据异常");
        }
    }

    /**
     * 批量删除一批区域信息数据
     **/
    public void deleteRegionByRegionidList(String regionIdList) {
        try {
            regions.deleteRegionByRegionidList(regionIdList);
        } catch (Exception ex) {
            logs.write(ex, "批量删除一批区域信息数据异常");
        }
    }

    /**
     * 获取一条区域信息数据
     *
     * @param regionId 区域信息模型
     **/
    public RegionInfo getRegionByRegionid(Integer regionId) {
        try {

            if (regionId <= 0) {
                return new RegionInfo();
            }

            return regions.getRegionByRegionid(regionId);
        } catch (Exception ex) {
            logs.write(ex, "获取一条区域信息数据");
        }

        return new RegionInfo();
    }


    /**
     * 获得区域信息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回RegionInfo
     **/
    public List<RegionInfo> getRegionList(Specification<RegionInfo> condition, Sort sort) {

        List<RegionInfo> regionList = new ArrayList<RegionInfo>();

        try {
            regionList = regions.getRegionList(condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得区域信息数据列表异常");
        }

        return regionList;
    }


    /**
     * 获得区域信息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回RegionInfo
     **/
    public Page<RegionInfo> getRegionList(Integer pageSize, Integer pageNumber, Specification<RegionInfo> condition, Sort sort) {

        Page<RegionInfo> regionList = null;

        try {
            regionList = regions.getRegionList(pageSize, pageNumber, condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得区域信息数据列表异常");
        }

        return regionList;
    }


    /**
     * 获取行政省列表
     *
     * @return
     */
    public List<RegionInfo> getRegionProvinceList() {

        List<RegionInfo> regionList = new ArrayList<RegionInfo>();

        try {
            regionList = regions.getRegionProvinceList();
        } catch (Exception ex) {
            logs.write(ex, "获取行政省列表");
        }

        return regionList;

    }

    /**
     * 获取行政城市(市)列表
     *
     * @return
     */
    public List<RegionInfo> getRegionCityList(Integer provinceId) {

        List<RegionInfo> regionList = new ArrayList<RegionInfo>();

        try {
            regionList = regions.getRegionCityList(provinceId);

        } catch (Exception ex) {

            logs.write(ex, "获取行政城市(市)列表");
        }

        return regionList;

    }

    /**
     * 获取行政城市(县)列表
     *
     * @return
     */
    public List<RegionInfo> getRegionCountyList(Integer cityId) {

        List<RegionInfo> regionList = new ArrayList<RegionInfo>();

        try {
            regionList = regions.getRegionCountyList(cityId);

        } catch (Exception ex) {

            logs.write(ex, "获取行政城市(县)列表");
        }

        return regionList;

    }


    //endregion

}
