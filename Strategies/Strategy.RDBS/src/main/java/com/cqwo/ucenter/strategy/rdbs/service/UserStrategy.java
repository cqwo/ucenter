package com.cqwo.ucenter.strategy.rdbs.service;

import com.cqwo.ucenter.core.data.rdbs.IUserStrategy;
import com.cqwo.ucenter.core.data.rdbs.repository.*;
import com.cqwo.ucenter.core.domain.users.*;
import com.cqwo.ucenter.core.exception.token.TokenException;
import com.cqwo.ucenter.core.helper.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component(value = "UserStrategy")
public class UserStrategy extends RDBSService implements IUserStrategy {

    @Autowired
    UserRepository userRepository;


    //region 用户

    /**
     * 获得用户数量
     *
     * @param condition 条件
     * @return 返回数量
     * @throws IOException
     **/
    @Override

    public long getPartUserCount(Specification<PartUserInfo> condition) throws IOException {
        return userRepository.count(condition);
    }


    /**
     * 创建一条用户数据
     *
     * @param userInfo 用户模型
     * @return 返回创建信息
     * @throws IOException
     **/

    @Override
    public PartUserInfo createPartUser(PartUserInfo userInfo) throws IOException {

        return userRepository.save(userInfo);
    }


    @Override
    public PartUserInfo updatePartUser(PartUserInfo userInfo) throws IOException {

        if (!userInfo.getUid().isEmpty()) {
            userRepository.save(userInfo);
        }
        return userInfo;
    }


    /**
     * 不实现用户的删除.太重要了
     *
     * @param uid 用户模型
     * @throws IOException
     */
    @Override
    public void deleteUserByUid(String uid) throws IOException {

        //userRepository.deleteAllByUid(uid);
    }


    @Override
    public void deleteUserByUidList(String uidList) throws IOException {

    }


    @Override
    public PartUserInfo getPartUserByUid(String uid) throws IOException {
        return userRepository.findFirstByUid(uid);
    }


    @Override
    public PartUserInfo getPartUserByMobile(String mobile) throws IOException {

        return userRepository.findFirstByMobile(mobile);


    }


    @Override
    public PartUserInfo getPartUserByUserName(String userName) throws IOException {

        return userRepository.findFirstByUserName(userName);

    }

    /**
     * 通过OpenId获取用户信息
     *
     * @param server     服务
     * @param openId     openid
     * @param oauthAppId 开放appid
     */
    @Override
    public PartUserInfo getPartUserByOpenIdAndOAuthAppId(String server, String openId, String oauthAppId) throws IOException {

        List<PartUserInfo> userInfoList = userRepository.findByOpenIdAndOAuthAppId(server, openId, oauthAppId);

        if (userInfoList == null || userInfoList.size() <= 0) {
            return null;
        }

        return userInfoList.get(0);
    }

    /**
     * 通过UnionId获取用户信息
     *
     * @param server  服务
     * @param unionId unionId
     */
    @Override
    public PartUserInfo getPartUserByUnionId(String server, String unionId) throws IOException {

        List<PartUserInfo> userInfoList = userRepository.findByUnionId(server, unionId);

        if (userInfoList == null || userInfoList.size() <= 0) {
            return null;
        }

        return userInfoList.get(0);
    }

    @Override
    public PartUserInfo getPartUserByEmail(String email) throws IOException {

        return userRepository.findFirstByEmail(email);

    }


    @Override
    public List<PartUserInfo> getPartUserList(Specification<PartUserInfo> specification, Sort sort) throws IOException {

        return userRepository.findAll(specification, sort);

    }


    @Override
    public Page<PartUserInfo> getPartUserList(int pageSize, int pageNumber, Specification<PartUserInfo> condition, Sort sort) throws IOException {

        if (pageNumber >= 1) {
            pageNumber--;
        }

        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        return userRepository.findAll(condition, pageable);
    }

    @Override
    public boolean isExitsInvitCode(String invitCode) throws IOException {
        return userRepository.existsByInvitCode(invitCode);
    }

    /**
     * 更新用户组
     *
     * @param uid     用户uid
     * @param userRid 用户分组
     */
    @Override
    public void updateUserRankByUid(String uid, Integer userRid) throws IOException {
        userRepository.updateUserRankByUid(uid, userRid);
    }

    /**
     * 检测用户账号是否已经存在
     *
     * @param account 用户账号
     * @return
     */
    @Override
    public boolean checkedUserByAccount(String account) throws IOException {
        return userRepository.existsByMobileOrEmailOrUserName(account, account, account);
    }


    /**
     * 检测邮箱是否存在
     *
     * @param email 邮箱
     * @return
     * @throws IOException
     */
    @Override
    public boolean checkedUserByEmail(String email) throws IOException {
        return userRepository.existsByEmail(email);
    }

    /**
     * 检测用户名是否存在
     *
     * @param userName 用户名
     * @return
     * @throws IOException
     */
    @Override
    public boolean checkedUserByUserName(String userName) throws IOException {
        return userRepository.existsByUserName(userName);
    }


    /**
     * 检测用户手机是否存在
     *
     * @param mobile mobile
     * @return
     * @throws IOException
     */
    @Override
    public boolean checkedUserByMobile(String mobile) throws IOException {
        return userRepository.existsByMobile(mobile);
    }


    //endregion


    @Autowired
    UserDetailRepository userDetailRepository;

    //region 用户详情

    /**
     * 获得用户详情数量
     *
     * @param condition 条件
     * @return 返回数量
     * @throws IOException
     **/
    @Override

    public long getUserDetailCount(Specification<UserDetailInfo> condition) throws IOException {

        return userDetailRepository.count();
    }


    /**
     * 创建一条用户详情数据
     *
     * @param userdetailInfo 用户详情模型
     * @return 返回创建信息
     * @throws IOException
     **/
    @Override

    public UserDetailInfo createUserDetail(UserDetailInfo userdetailInfo) throws IOException {

        return userDetailRepository.save(userdetailInfo);
    }


    /**
     * 更新一条用户详情数据
     *
     * @param userdetailInfo 用户详情模型
     **/
    @Override

    public UserDetailInfo updateUserDetail(UserDetailInfo userdetailInfo) throws IOException {

        if (userdetailInfo.getId() >= 1) {
            return userDetailRepository.save(userdetailInfo);
        }

        return userdetailInfo;

    }


    /**
     * 删除一条用户详情数据
     *
     * @param id 用户详情模型
     **/
    @Override

    public void deleteUserDetailById(int id) throws IOException {

        //userDetailRepository.deleteById(id);
    }

    /**
     * 批量删除一批用户详情数据
     **/
    @Override
    public void deleteUserDetailByIdList(String idlist) throws IOException {


    }

    /**
     * 获得用户详情一条记录
     *
     * @param id id
     * @return 返回一条UserDetailInfo
     **/
    @Override

    public UserDetailInfo getUserDetailById(int id) throws IOException {
        return userDetailRepository.findById(id).get();
    }


    /**
     * 获得用户详情数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回UserDetailInfo
     **/
    @Override

    public List<UserDetailInfo> getUserDetailList(Specification<UserDetailInfo> condition, Sort sort) throws IOException {

        return userDetailRepository.findAll(condition, sort);

    }


    /**
     * 获得用户详情数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回UserDetailInfo
     **/
    @Override

    public Page<UserDetailInfo> getUserDetailList(Integer pageSize, Integer pageNumber, Specification<UserDetailInfo> condition, Sort sort) throws IOException {

        if (pageNumber >= 1) {
            pageNumber--;
        }

        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        return userDetailRepository.findAll(condition, pageable);


    }


    //endregion

    @Autowired
    UserRankRepository userRankRepository;

    //region 用户等级

    /**
     * 获得用户等级数量
     *
     * @param condition 条件
     * @return 返回数量
     * @throws IOException
     **/
    @Override

    public long getUserRankCount(Specification<UserRankInfo> condition) throws IOException {

        return userRankRepository.count();
    }


    /**
     * 创建一条用户等级数据
     *
     * @param userrankInfo 用户等级模型
     * @return 返回创建信息
     * @throws IOException
     **/
    @Override

    public UserRankInfo createUserRank(UserRankInfo userrankInfo) throws IOException {

        return userRankRepository.save(userrankInfo);
    }


    /**
     * 更新一条用户等级数据
     *
     * @param userrankInfo 用户等级模型
     **/
    @Override

    public UserRankInfo updateUserRank(UserRankInfo userrankInfo) throws IOException {

        if (userrankInfo.getUserRid() >= 1) {
            return userRankRepository.save(userrankInfo);
        }

        return userrankInfo;

    }


    /**
     * 删除一条用户等级数据
     *
     * @param userrid 用户等级模型
     **/
    @Override

    public void deleteUserRankByUserrid(int userrid) throws IOException {

        userRankRepository.deleteById(userrid);
    }

    /**
     * 批量删除一批用户等级数据
     **/
    @Override
    public void deleteUserRankByUserridList(String userridlist) throws IOException {


    }

    /**
     * 获得用户等级一条记录
     *
     * @param userrid userrid
     * @return 返回一条UserRankInfo
     **/
    @Override

    public UserRankInfo getUserRankByUserrid(int userrid) throws IOException {
        return userRankRepository.findById(userrid).get();
    }


    /**
     * 获得用户等级数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回UserRankInfo
     **/
    @Override

    public List<UserRankInfo> getUserRankList(Specification<UserRankInfo> condition, Sort sort) throws IOException {

        return userRankRepository.findAll(condition, sort);

    }


    /**
     * 获得用户等级数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回UserRankInfo
     **/
    @Override

    public Page<UserRankInfo> getUserRankList(Integer pageSize, Integer pageNumber, Specification<UserRankInfo> condition, Sort sort) throws IOException {

        if (pageNumber >= 1) {
            pageNumber--;
        }

        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        return userRankRepository.findAll(condition, pageable);


    }


    /**
     * 获取全部用户组
     *
     * @return
     */
    @Override

    public List<UserRankInfo> getAllUserRankList() throws IOException {
        return (List<UserRankInfo>) userRankRepository.findAll();
    }


    //endregion

    @Autowired
    private OauthRepository oauthRepository;

    //region 第三方登录

    /**
     * 获得第三方登录数量
     *
     * @param condition 条件
     * @return 返回数量
     * @throws IOException
     **/
    @Override

    public long getOauthCount(Specification<OauthInfo> condition) throws IOException {

        return oauthRepository.count();
    }


    /**
     * 创建一条第三方登录数据
     *
     * @param oauthInfo 第三方登录模型
     * @return 返回创建信息
     * @throws IOException
     **/
    @Override

    public OauthInfo createOauth(OauthInfo oauthInfo) throws IOException {

        return oauthRepository.save(oauthInfo);
    }


    /**
     * 更新一条第三方登录数据
     *
     * @param oauthInfo 第三方登录模型
     **/
    @Override

    public OauthInfo updateOauth(OauthInfo oauthInfo) throws IOException {

        if (oauthInfo.getId() >= 1) {
            return oauthRepository.save(oauthInfo);
        }

        return oauthInfo;

    }


    /**
     * 删除一条第三方登录数据
     *
     * @param id 第三方登录模型
     **/
    @Override

    public void deleteOauthById(int id) throws IOException {

        //oauthRepository.deleteById(id);
    }

    /**
     * 批量删除一批第三方登录数据
     **/
    @Override
    public void deleteOauthByIdList(String idlist) throws IOException {


    }

    /**
     * 获得第三方登录一条记录
     *
     * @param id id
     * @return 返回一条OauthInfo
     **/
    @Override

    public OauthInfo getOauthById(int id) throws IOException {
        return oauthRepository.findById(id).get();
    }


    /**
     * 获得第三方登录数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回OauthInfo
     **/
    @Override

    public List<OauthInfo> getOauthList(Specification<OauthInfo> condition, Sort sort) throws IOException {

        return oauthRepository.findAll(condition, sort);

    }


    /**
     * 获得第三方登录数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回OauthInfo
     **/
    @Override

    public Page<OauthInfo> getOauthList(Integer pageSize, Integer pageNumber, Specification<OauthInfo> condition, Sort sort) throws IOException {

        if (pageNumber >= 1) {
            pageNumber--;
        }

        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        return oauthRepository.findAll(condition, pageable);


    }

    /**
     * 检测OpendId是否重复
     *
     * @param server     服务
     * @param openId     opendId
     * @param oauthAppId
     * @return
     */
    @Override
    public OauthInfo getOauthByOpenId(String server, String openId, String oauthAppId) throws IOException {

        return oauthRepository.findFirstByServerAndOpenIdAndOauthAppId(server, openId, oauthAppId);

    }

    /**
     * @param server
     * @param openId
     * @param unionId
     * @param oauthAppId
     * @return
     * @throws IOException
     */
    @Override
    public OauthInfo getOauthByOpenIdAndUnionId(String server, String openId, String unionId, String oauthAppId) throws IOException {
        return oauthRepository.findFirstByServerAndOpenIdAndUnionIdAndOauthAppId(server, openId, unionId, oauthAppId);
    }

    /**
     * 判断服务的OpenId是否存在
     *
     * @param server     服务
     * @param openId     openId
     * @param oauthAppId
     * @return
     */
    @Override
    public boolean existsOauthByOAuthAppId(String server, String openId, String oauthAppId) throws IOException {

        return oauthRepository.existsByServerAndOpenIdAndOauthAppId(server, openId, oauthAppId);

    }


    /**
     * 查找用户是否存在
     *
     * @param server  服务
     * @param unionId unionId
     * @return
     */
    @Override
    public boolean existsOauthByUnionId(String server, String unionId) throws IOException {
        return oauthRepository.existsByServerAndUnionId(server, unionId);
    }


    /**
     * 通过appid和uid查找用户信息
     *
     * @param oauthAppId oauthAppId
     * @param uid        uid
     * @return
     */
    @Override
    public OauthInfo getOauthByOAuthAppIdAndUid(String oauthAppId, String uid) throws IOException {
        return oauthRepository.findFirstByOauthAppIdAndUid(oauthAppId, uid);
    }
    //endregion

    @Autowired
    OnlineTimeRepository onlineTimeRepository;

    //region 在线时间统计

    /**
     * 获得在线时间统计数量
     *
     * @param condition 条件
     * @return 返回数量
     * @throws IOException
     **/
    @Override

    public long getOnlineTimeCount(Specification<OnlineTimeInfo> condition) throws IOException {

        return onlineTimeRepository.count();
    }


    /**
     * 创建一条在线时间统计数据
     *
     * @param onlinetimeInfo 在线时间统计模型
     * @return 返回创建信息
     * @throws IOException
     **/
    @Override

    public OnlineTimeInfo createOnlineTime(OnlineTimeInfo onlinetimeInfo) throws IOException {

        return onlineTimeRepository.save(onlinetimeInfo);
    }


    /**
     * 更新一条在线时间统计数据
     *
     * @param onlinetimeInfo 在线时间统计模型
     **/
    @Override

    public OnlineTimeInfo updateOnlineTime(OnlineTimeInfo onlinetimeInfo) throws IOException {

        if (onlinetimeInfo.getId() >= 1) {
            return onlineTimeRepository.save(onlinetimeInfo);
        }

        return onlinetimeInfo;

    }


    /**
     * 删除一条在线时间统计数据
     *
     * @param id 在线时间统计模型
     **/
    @Override

    public void deleteOnlineTimeById(int id) throws IOException {

        onlineTimeRepository.deleteById(id);
    }

    /**
     * 批量删除一批在线时间统计数据
     **/
    @Override
    public void deleteOnlineTimeByIdList(String idlist) throws IOException {


    }

    /**
     * 获得在线时间统计一条记录
     *
     * @param id id
     * @return 返回一条OnlineTimeInfo
     **/
    @Override

    public OnlineTimeInfo getOnlineTimeById(int id) throws IOException {
        return onlineTimeRepository.findById(id).get();
    }


    /**
     * 获得在线时间统计数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回OnlineTimeInfo
     **/
    @Override

    public List<OnlineTimeInfo> getOnlineTimeList(Specification<OnlineTimeInfo> condition, Sort sort) throws IOException {

        return onlineTimeRepository.findAll(condition, sort);

    }


    /**
     * 获得在线时间统计数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回OnlineTimeInfo
     **/
    @Override

    public Page<OnlineTimeInfo> getOnlineTimeList(Integer pageSize, Integer pageNumber, Specification<OnlineTimeInfo> condition, Sort sort) throws IOException {

        if (pageNumber >= 1) {
            pageNumber--;
        }

        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        return onlineTimeRepository.findAll(condition, pageable);


    }


    //endregion


    @Autowired
    OnlineUserRepository onlineUserRepository;


    //region 在线用户

    /**
     * 获得在线用户数量
     *
     * @param condition 条件
     * @return 返回数量
     * @throws IOException
     **/
    @Override

    public long getOnlineUserCount(Specification<OnlineUserInfo> condition) throws IOException {

        return onlineUserRepository.count();
    }


    /**
     * 创建一条在线用户数据
     *
     * @param onlineuserInfo 在线用户模型
     * @return 返回创建信息
     * @throws IOException
     **/
    @Override

    public OnlineUserInfo createOnlineUser(OnlineUserInfo onlineuserInfo) throws IOException {

        return onlineUserRepository.save(onlineuserInfo);
    }


    /**
     * 更新一条在线用户数据
     *
     * @param onlineuserInfo 在线用户模型
     **/
    @Override

    public OnlineUserInfo updateOnlineUser(OnlineUserInfo onlineuserInfo) throws IOException {

        if (onlineuserInfo.getOlId() >= 1) {
            return onlineUserRepository.save(onlineuserInfo);
        }

        return onlineuserInfo;

    }


    /**
     * 删除一条在线用户数据
     *
     * @param olid 在线用户模型
     **/
    @Override

    public void deleteOnlineUserByOlid(int olid) throws IOException {

        onlineUserRepository.deleteById(olid);
    }

    /**
     * 批量删除一批在线用户数据
     **/
    @Override
    public void deleteOnlineUserByOlidList(String olidlist) throws IOException {


    }

    /**
     * 获得在线用户一条记录
     *
     * @param olid olid
     * @return 返回一条OnlineUserInfo
     **/
    @Override

    public OnlineUserInfo getOnlineUserByOlid(int olid) throws IOException {
        return onlineUserRepository.findById(olid).get();
    }


    /**
     * 获得在线用户数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回OnlineUserInfo
     **/
    @Override

    public List<OnlineUserInfo> getOnlineUserList(Specification<OnlineUserInfo> condition, Sort sort) throws IOException {

        return onlineUserRepository.findAll(condition, sort);

    }


    /**
     * 获得在线用户数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回OnlineUserInfo
     **/
    @Override

    public Page<OnlineUserInfo> getOnlineUserList(Integer pageSize, Integer pageNumber, Specification<OnlineUserInfo> condition, Sort sort) throws IOException {

        if (pageNumber >= 1) {
            pageNumber--;
        }

        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        return onlineUserRepository.findAll(condition, pageable);


    }


    //endregion

    @Autowired
    UserTokenRepository userTokenRepository;

    //region token管理

    /**
     * 更新用户的token信息
     *
     * @param userTokenInfo 用户模型
     * @return 返回创建信息
     **/
    @Override
    public UserTokenInfo updateUserToken(UserTokenInfo userTokenInfo) throws IOException {

        return userTokenRepository.saveAndFlush(userTokenInfo);
    }


    /**
     * 删除过期token
     */
    @Override
    public void deleteLitmitToken() throws IOException {
        Integer timestamp = DateHelper.getUnixTimeStamp() - 30 * 60;
        userTokenRepository.deleteLitmitToken(timestamp);
    }


    /**
     * 获取token
     *
     * @param uid uid
     * @param token token
     * @return
     */
    @Override
    public UserTokenInfo findUserToken(String uid, String token) throws IOException {
        return userTokenRepository.findFirstByUidAndToken(uid, token);
    }

    /**
     * 获取token
     *
     * @param uid   uid
     * @param appId
     * @return
     */
    @Override
    public UserTokenInfo findLastUserTokenByUid(String uid, String appId) throws IOException {
        return userTokenRepository.findFirstByUidAndAppId(uid, appId);
    }


    /**
     * 获取token
     *
     * @param uid   uid
     * @param appId oauthAppId
     * @return UserTokenInfo
     */
    @Override
    public UserTokenInfo findUserTokenByUidAndRefreshToken(String uid, String appId, String refreshToken) throws TokenException {
        return userTokenRepository.findFirstByUidAndRefreshToken(uid, appId, refreshToken);
    }
    //endregion
}
