package com.cqwo.ucenter.strategy.redis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cqwo.ucenter.core.cache.ICacheStrategy;
import com.cqwo.ucenter.core.helper.StringHelper;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component(value = "RedisStrategy")
public class RedisStrategy implements ICacheStrategy {

    /**
     * 日志记录
     */
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    protected RedisTemplate redisTemplate;

    protected int timeout = 60;

    protected TimeUnit timeUnit = TimeUnit.MINUTES;


    @Autowired(required = false)
    public void setRedisTemplate(RedisTemplate redisTemplate) {
        try {
            RedisSerializer stringSerializer = new StringRedisSerializer();
            redisTemplate.setKeySerializer(stringSerializer);
            redisTemplate.setValueSerializer(stringSerializer);
            redisTemplate.setHashKeySerializer(stringSerializer);
            redisTemplate.setHashValueSerializer(stringSerializer);
            this.redisTemplate = redisTemplate;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 测试打印
     */

    @Override
    public String print() {

        ValueOperations<String, Object> valueops = redisTemplate.opsForValue();
        valueops.set("222", "测试打印");


        System.out.print("测试打印");
        return (String) valueops.get("222");
    }

    /**
     * 获取
     *
     * @param key 关键值
     * @return 值
     */
    @Override
    public Object getValue(String key) {

        ValueOperations<String, String> valueops = redisTemplate.opsForValue();

        return valueops.get(key);
    }

    /**
     * 获取
     *
     * @param key 关键值
     * @return 值
     */
    @Override
    public <T> T getValue(String key, Class<T> clz) {

        try {

            ValueOperations<String, String> valueops = redisTemplate.opsForValue();

            String s = valueops.get(key);

            if (s.isEmpty()) {
                return null;
            }

            return JSON.parseObject(key, clz);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }

        return null;
    }


    /**
     * 设置
     *
     * @param key    关键值
     * @param object 值
     */
    @Override
    public void setValue(String key, Object object) {
        setValue(key, object, 60, TimeUnit.MINUTES);
    }


    /**
     * 设置
     *
     * @param key    关键值
     * @param object 值
     */
    @Override
    public void setValue(String key, Object object, int timeout, TimeUnit timeUnit) {

        object = object == null ? new Object() : object;


        ValueOperations<String, Object> valueops = redisTemplate.opsForValue();

        String s = JSON.toJSONString(object);

        valueops.set(key, s, timeout, timeUnit);
    }

    /**
     * 获取
     *
     * @param key 关键值
     * @return 值
     */
    @Override
    public <T> List<T> getListValue(String key, Class<T> clz) {


        ValueOperations<String, String> valueops = redisTemplate.opsForValue();

        String s = valueops.get(key);

        if (StringHelper.isEmpty(s)) {
            return null;
        }


        return (List<T>) JSONArray.parseArray(s, clz);
    }

    /**
     * 设置
     *
     * @param key  关键值
     * @param list 值
     */
    @Override
    public void setListValue(String key, List<?> list) {
        setListValue(key, list, 60, TimeUnit.MINUTES);
    }


    /**
     * 设置
     *
     * @param key  关键值
     * @param list 值
     */
    @Override
    public void setListValue(String key, List<?> list, int timeout, TimeUnit timeUnit) {

        list = list == null ? new ArrayList<>() : list;

        String s = JSONObject.toJSONString(list);

        ValueOperations<String, Object> valueops = redisTemplate.opsForValue();

        valueops.set(key, s, timeout, timeUnit);
    }


    @Test
    public void test2() {

        String s = "{id:2}";

        System.out.println(JSON.parse(s));

    }

}
