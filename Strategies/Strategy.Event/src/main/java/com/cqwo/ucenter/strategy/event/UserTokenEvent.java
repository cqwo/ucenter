package com.cqwo.ucenter.strategy.event;

import com.cqwo.ucenter.core.data.CWMData;
import com.cqwo.ucenter.core.envent.IEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * usertoken定时器
 *
 * @author cqnews
 */
@Component(value = "UserTokenEvent")
public class UserTokenEvent implements IEvent {

    /**
     * rdbs策略
     */
    @Autowired
    private CWMData cwmData;

    /**
     * 名称
     *
     * @return 名称
     */
    @Override
    public String name() {
        return "usertoken定时器";
    }

    @Override
    @Scheduled(cron = "0 0/5 * * * ?")
    public void execute() {

        try {
            cwmData.getIUserStrategy().deleteLitmitToken();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }
}
