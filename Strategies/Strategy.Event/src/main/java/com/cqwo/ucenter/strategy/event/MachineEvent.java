/*
 *
 *  *
 *  *  * Copyright (C) 2018.
 *  *  * 用于JAVA项目开发
 *  *  * 重庆青沃科技有限公司 版权所有
 *  *  * Copyright (C)  2018.  CqingWo Systems Incorporated. All rights reserved.
 *  *
 *
 */

package com.cqwo.ucenter.strategy.event;


import com.cqwo.ucenter.core.envent.IEvent;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Calendar;
import java.util.Date;

/**
 * @author cqnews
 */
public class MachineEvent implements IEvent {


    @Scheduled()
    @Override
    public void execute() {

        Calendar calendar = Calendar.getInstance();

        long time = calendar.get(Calendar.HOUR);

        if (time >= 9 && time <= 22) {

        }

    }


}
