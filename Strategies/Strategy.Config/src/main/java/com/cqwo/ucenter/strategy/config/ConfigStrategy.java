package com.cqwo.ucenter.strategy.config;

import com.cqwo.ucenter.core.config.IConfigStrategy;
import com.cqwo.ucenter.core.config.info.*;
import org.springframework.stereotype.Component;

@Component(value = "ConfigStrategy")
public class ConfigStrategy implements IConfigStrategy {
    @Override
    public boolean saveRDBSConfig(RDBSConfigInfo configInfo) {
        return false;
    }

    @Override
    public RDBSConfigInfo getRDBSConfig() {
        return null;
    }

    @Override
    public boolean saveBaseConfig(BaseConfigInfo configInfo) {
        return false;
    }

    @Override
    public BaseConfigInfo getBaseConfig() {
        return null;
    }

    @Override
    public boolean saveEmailConfig(EmailConfigInfo configInfo) {
        return false;
    }

    @Override
    public EmailConfigInfo getEmailConfig() {
        return null;
    }

    @Override
    public boolean saveSMSConfig(SMSConfigInfo configInfo) {
        return false;
    }

    @Override
    public SMSConfigInfo getSMSConfig() {
        return null;
    }

    @Override
    public boolean saveWechatConfig(WechatConfigInfo configInfo) {
        return false;
    }

    @Override
    public WechatConfigInfo getWechatConfig() {
        return null;
    }
}
