package com.cqwo.ucenter.strategy.wechat.config;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@ConfigurationProperties(prefix = "wechat.config")
@PropertySource("classpath:wxopenconfig.properties")
@Component(value = "WxOpenConfig")
public class WxOpenConfig {

    /**
     * AppId
     */
    private String appId = "";

    /**
     * App密钥
     */
    private String secret = "";


    /**
     * token
     */
    private String token = "";


}
