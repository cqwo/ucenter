package com.cqwo.ucenter.strategy.wechat;

import com.cqwo.ucenter.core.cache.CWMCache;
import com.cqwo.ucenter.core.wechat.IWxOpenStrategy;
import com.cqwo.ucenter.strategy.wechat.config.WxOpenConfig;
import com.cqwo.wechat.open.web.config.WxOpenConfigStorage;
import com.cqwo.wechat.open.web.config.WxOpenInMemoryConfigStorage;
import com.cqwo.wechat.open.web.exption.WxOpenErrorException;
import com.cqwo.wechat.open.web.impl.WxOpenServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component(value = "WxOpenStrategy")
public class WxOpenStrategy extends WxOpenServiceImpl implements IWxOpenStrategy {


    @Autowired
    private CWMCache cwmCache;

    private Logger logger = LoggerFactory.getLogger(WxOpenStrategy.class);

    @Autowired
    private WxOpenConfig wxOpenConfig;

    @PostConstruct
    public void init() {

        WxOpenConfigStorage config = new WxOpenInMemoryConfigStorage();

        config.setAppId(wxOpenConfig.getAppId());
        config.setSecret(wxOpenConfig.getSecret());
        config.setToken(wxOpenConfig.getToken());
        try {
            super.putWxOpenConfigStorage(config);
        } catch (WxOpenErrorException ex) {
            ex.printStackTrace();
            logger.error("参数初始化失败", ex);
        }

    }

}
